package jarawak;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import jarawak.db.DBLin;
import jarawak.db.DBParVal;

public class JarawakModel implements Serializable, Cloneable {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	private static final long serialVersionUID = 1L;
	private JarawakConnector connector = null;
	private String encoding = null;
	public String totalrows = null;
	public String f_page = null;
	public String f_limit = null;
	private String table_name = null;

	public JarawakModel(JarawakConnector connector, String encoding) {
		this.connector = connector;
		this.encoding = encoding;
		if (encoding == null)
			this.encoding = "UTF-8";
	}

	/*
	 * #########################################################################
	 * ##### GETTERS SETTERS
	 * #########################################################################
	 * #####
	 */

	public void setMetainfodb(JarawakParam[] metainfodb) {
		if (connector != null)
			connector.setMetainfotype(metainfodb);
	}

	public JarawakParam[] getMetainfodb() {
		if (connector != null)
			return connector.getMetainfotype();
		else
			return null;
	}

	public void setConnectorEndpoint(String endpoint) {
		if (connector != null)
			connector.setEndpoint(endpoint);
	}

	public void setConnector(JarawakConnector connector) {
		this.connector = connector;
	}

	public JarawakConnector getConnector() {
		return connector;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
		if (encoding == null)
			this.encoding = "UTF-8";
	}

	public String getTableName() {
		return table_name;
	}

	public void setTableName(String table_name) {
		this.table_name = table_name;
	}

	/*
	 * #########################################################################
	 * ##### CREATE TABLE
	 * #########################################################################
	 */

	public void createTable(boolean force) throws JarawakException {

		ArrayList<JarawakParam> fields = new ArrayList<JarawakParam>();
		JarawakParam[] meta = getMetainfodb();

		if (connector == null)
			throw new JarawakException("E", "MODEL", "model.e.noconnector");
		if (meta == null || meta.length <= 0)
			throw new JarawakException("E", "MODEL", "model.e.noconnector");

		for (JarawakParam p : meta) {
			if (p.name != null)
				p.name = p.name.toLowerCase();
			else
				throw new JarawakException("E", "MODEL", "model.e.informfieldname");
			fields.add(p);
		}

		connector.createTable(connector.getEndpoint(), fields, force);
	}

	/*
	 * #########################################################################
	 * ##### LIST
	 * #########################################################################
	 * ##### keys = obligatory composed by linParam.name (name of key)
	 * linParam.value(value of key)
	 */

	public Object[] listOfMe(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit, boolean bringtotal)
			throws JarawakException {
		return listOfMe(keys, ord, page, limit, bringtotal, false, false);
	}

	public Object[] listOfMe(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit, boolean bringtotal,
			boolean intercepterror, boolean useconfig) throws JarawakException {

		ArrayList<DBLin> al = connector.list(keys, ord, page, limit, bringtotal, intercepterror, useconfig);
		Object[] out = null;
		if (al != null && al.size() > 0)
			out = JarawakWS.convDBL2O(this.getClass(), keys, al, connector, null, bringtotal);

		return out;
	}

	/*
	 * #########################################################################
	 * ##### GET
	 * #########################################################################
	 * #####
	 */

	public void getMe(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		getMe(keys, uid, false, false);
	}

	public void getMe(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, boolean useconfig)
			throws JarawakException {
		DBLin lin = connector.get(keys, uid, intercepterror, useconfig);

		if (lin != null && lin.cols != null && lin.cols.length > 0) {
			for (DBParVal p : lin.cols) {
				String param = p.param;
				if (param != null && param.indexOf('.') >= 0)
					try {
						param = param.replaceAll("\\.", "_");
					} catch (Exception e) {

					}
				try {
					if (p.value != null) {
						JarawakParam mi = connector.getMetaInfoTypeP(param);
						if (mi != null && mi.value != null) {
							param = mi.name;
							if (mi.value.trim().equals(JarawakParam.TYPE_EXCLUDED))
								continue;
						}
						Field f = null;
						try {
							f = this.getClass().getDeclaredField(param);
						} catch (Exception e) {
							continue;
						}
						if (f == null)
							continue;
						String classname = f.getType().getName();
						if (classname.trim().indexOf("[") == 0)
							classname = classname.substring(2, classname.length() - 1);
						if (f.getType().getName().equals("java.lang.String")) {
							if (mi != null && mi.value != null
									&& mi.value.trim().equals(JarawakParam.TYPE_ENCODED_STRING))
								try {
									p.value = URLDecoder.decode(p.value, encoding);
								} catch (Exception e) {
									p.value = p.value;
								}

							this.getClass().getDeclaredField(param).set(this, p.value);
						} else {
							Class<?> clz = Class.forName(classname);
							Constructor<?> constr = clz.getConstructors()[0];
							Object o = null;
							try {
								o = constr.newInstance(new Object[] { null, null });
							} catch (Exception w) {
								try {
									o = constr.newInstance();
								} catch (Exception y) {

								}
							}
							if (mi != null && mi.value != null && mi.value.trim().equals(JarawakParam.TYPE_OBJECT)) {
								try {
									JSONObject jso = new JSONObject(p.value);
									JarawakWS ws = new JarawakWS();
									if (o != null)
										o = ws.json2Object(o, jso, true);
									this.getClass().getDeclaredField(param).set(this, o);
								} catch (Exception e) {

								}
							} else if (mi != null && mi.value != null
									&& mi.value.trim().equals(JarawakParam.TYPE_ARRAY_OBJECT)) {
								try {
									JSONArray jsa = new JSONArray(p.value);
									JarawakWS ws = new JarawakWS();
									Object[] a = (Object[]) Array.newInstance(clz, jsa.length());
									for (int ci = 0; ci < a.length; ci++)
										if (constr.getParameterTypes().length == 0)
											a[ci] = constr.newInstance();
										else if (constr.getParameterTypes().length == 2)
											a[ci] = constr.newInstance(new Object[] { null, null });
									if (a != null)
										a = ws.json2ObjectA(a, jsa, true);
									this.getClass().getDeclaredField(param).set(this, a);
								} catch (Exception e) {

								}
							} else {
								if (mi != null && mi.value != null
										&& mi.value.trim().equals(JarawakParam.TYPE_ENCODED_STRING))
									p.value = URLDecoder.decode(p.value, encoding);
								try {
									this.getClass().getDeclaredField(param).set(this, p.value);
								} catch (Exception cex) {

								}
							}
						}
					}
				} catch (Exception e) {
					throw new JarawakException("E", "MODEL", "model.e.errorparsingdata");
				}
			}
		} else {
			throw new JarawakException("E", "MODEL", "model.e.noresult");
		}
		JSONObject jso = prepareThisJSON(this, null, false, null, false, false);

		if (jso != null && jso.names().length() > 0) {
			for (int i = 0; i < jso.names().length(); i++) {

				// ON METHODS
				try {
					String name = jso.names().getString(i);
					Method[] methods = this.getClass().getDeclaredMethods();
					for (int j = 0; j < methods.length; j++)
						// ON SELECT
						if (methods[j].getName().equals("_onselect_" + name)) {
							try {
								methods[j].invoke(this, keys, name);
							} catch (Exception ee) {

							}
							break;
						}
				} catch (Exception e) {

				}

			}
		}
	}

	/*
	 * #########################################################################
	 * ##### CREATE
	 * #########################################################################
	 * #####
	 */

	public String createMe(String ai) throws JarawakException {
		return createMe(ai, null);
	}

	public String createMe(String ai, ArrayList<JarawakParam> connpars) throws JarawakException {
		return createMe(ai, connpars, false);
	}

	public String createMe(String ai, ArrayList<JarawakParam> connpars, boolean intercepterror)
			throws JarawakException {
		return createMe(ai, connpars, intercepterror, false);
	}

	public String createMe(String ai, ArrayList<JarawakParam> connpars, boolean intercepterror, boolean encode)
			throws JarawakException {
		return createMe(ai, connpars, intercepterror, false, false, false);
	}

	public String createMe(String ai, ArrayList<JarawakParam> connpars, boolean intercepterror, boolean encode,
			boolean processobjtypes, boolean useconfig) throws JarawakException {

		JSONObject jso = prepareThisJSON(this, ai, true, connpars, encode, true, processobjtypes);

		return connector.create(ai, jso, intercepterror, useconfig);
	}

	/*
	 * #########################################################################
	 * ##### SAVE
	 * #########################################################################
	 * #####
	 */

	public String saveMe(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return saveMe(keys, uid, false);
	}

	public String saveMe(ArrayList<JarawakParam> keys, String uid, boolean intercepterror) throws JarawakException {
		return saveMe(keys, uid, intercepterror, false);
	}

	public String saveMe(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, boolean encode)
			throws JarawakException {
		return saveMe(keys, uid, intercepterror, encode, false);
	}

	public String saveMe(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, boolean encode,
			boolean ignorenull) throws JarawakException {
		return saveMe(keys, uid, intercepterror, encode, ignorenull, false, false);
	}

	public String saveMe(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, boolean encode,
			boolean ignorenull, boolean processobjtypes, boolean useconfig) throws JarawakException {

		JSONObject jso = prepareThisJSON(this, null, true, null, encode, ignorenull, processobjtypes);
		return connector.save(jso, keys, uid, intercepterror, useconfig);
	}

	/*
	 * #########################################################################
	 * ##### DELETE
	 * #########################################################################
	 * #####
	 */

	public boolean deleteMe(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return deleteMe(keys, uid, false);
	}

	public boolean deleteMe(ArrayList<JarawakParam> keys, String uid, boolean intercepterror) throws JarawakException {
		return deleteMe(keys, uid, intercepterror, null, false);
	}

	public boolean deleteMe(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, String sessionuser,
			boolean useconfig) throws JarawakException {
		try {
			connector.delete(keys, uid, intercepterror, sessionuser, useconfig);
		} catch (JarawakException e) {
			throw e;
		}
		return true;
	}

	/*
	 * #########################################################################
	 * ##### PREPARE THIS JSON
	 * #########################################################################
	 * #####
	 */
	private JSONObject prepareThisJSON(Object obj2J, String ai, boolean exclude, ArrayList<JarawakParam> addpars,
			boolean encode, boolean ignorenull) {
		return prepareThisJSON(obj2J, ai, exclude, addpars, encode, ignorenull, false);
	}

	private JSONObject prepareThisJSON(Object obj2J, String ai, boolean exclude, ArrayList<JarawakParam> addpars,
			boolean encode, boolean ignorenull, boolean processobjtypes) {
		if (obj2J == null)
			return null;

		JarawakWS ws = new JarawakWS();
		JSONObject jso = null;
		try {
			jso = ws.lin2json(obj2J, encode, ignorenull);
			if (addpars != null && addpars.size() > 0) {
				for (JarawakParam p : addpars) {
					if (p.mode != null) {
						if (p.mode.equals(JarawakParam.TYPE_OBJECT))
							jso.put(p.name, new JSONObject(p.value));
						else if (p.mode.equals(JarawakParam.TYPE_ARRAY_OBJECT))
							jso.put(p.name, new JSONArray(p.value));
						else if (p.mode.equals(JarawakParam.TYPE_ARRAY_STRING))
							jso.put(p.name, p.value);
						else if (p.mode.equals(JarawakParam.TYPE_BIGINTEGER) || p.mode.equals(JarawakParam.TYPE_INTEGER)
								|| p.mode.equals(JarawakParam.TYPE_NUMERIC)) {
							if (p.value != null) {
								if (p.value.indexOf('.') >= 0) {
									try {
										double d = Double.valueOf(p.value).doubleValue();
										jso.put(p.name, d);
									} catch (Exception e) {
									}
								} else {
									try {
										long i = Long.valueOf(p.value).longValue();
										jso.put(p.name, i);
									} catch (Exception e) {
									}
								}
							}
						} else
							jso.put(p.name, p.value);
					} else {
						jso.put(p.name, p.value);
					}
				}
			}
		} catch (Exception e) {

		}
		// Remove (additional) not String fields
		if (jso != null && jso.length() > 0) {
			JSONObject jsout = new JSONObject();
			int ttl = jso.names().length();
			for (int i = 0; i < ttl; i++) {
				try {
					String n = jso.names().getString(i);
					if (ai != null && n.equalsIgnoreCase(ai))
						continue;
					if (n.equals("table_name") || n.equals("f_page") || n.equals("f_limit") || n.equals("totalrows"))
						continue;
					if (exclude) {
						JarawakParam mi = connector.getMetaInfoTypeP(n);
						if (mi != null) {
							if (mi.value.equals(JarawakParam.TYPE_EXCLUDED))
								continue;
							if (mi.value.equals(JarawakParam.TYPE_ARRAY_OBJECT)
									|| mi.value.equals(JarawakParam.TYPE_OBJECT)) {
								if (!processobjtypes)
									continue;
							}
						}
					}
					jsout.put(n, jso.get(n));
				} catch (Exception e) {

				}
			}
			return jsout;
		}
		return null;
	}

}
