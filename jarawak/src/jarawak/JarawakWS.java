package jarawak;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jarawak.config.WSConfig;
import jarawak.connectors.WSConnector;
import jarawak.db.DBLin;
import jarawak.db.DBParVal;

public class JarawakWS implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */
	private String encoding = "UTF-8";
	public final static int METHOD_GET = 1;
	public final static int METHOD_POST = 2;
	public final static int METHOD_PUT = 3;
	public final static int METHOD_DELETE = 4;
	public final static int METHOD_PATCH = 5;;
	private WSConfig conf = null;

	/*
	 * #########################################################################
	 * ##### CONSTRUCTORS
	 * #########################################################################
	 * #####
	 */

	public JarawakWS() {
		super();
	}

	public JarawakWS(String encoding) {
		super();
		if (encoding != null)
			this.encoding = encoding;
		else
			this.encoding = "UTF-8";
	}

	public JarawakWS(WSConfig conf) {
		super();
		this.conf = conf;
		this.encoding = conf.encoding;
	}

	/*
	 * #########################################################################
	 * ##### CALL WS
	 * #########################################################################
	 * #####
	 */

	public String callWSAS(String host, String endpoint, String[][] params, JSONObject jso, boolean encoded, int method,
			String[][] headerparams, boolean useconfig) throws JarawakException {

		if (!useconfig)
			endpoint = host + endpoint;

		switch (method) {
		case METHOD_GET:
			return new JarawakHttp(conf).doGET(endpoint, params, encoded, headerparams, useconfig);
		case METHOD_POST:
			return new JarawakHttp(conf).doPOST(endpoint, params, jso, encoded, headerparams, useconfig);
		case METHOD_PUT:
			return new JarawakHttp(conf).doPUT(endpoint, params, jso, encoded, headerparams, useconfig);
		case METHOD_DELETE:
			return new JarawakHttp(conf).doDELETE(endpoint, null, null, encoded, headerparams, useconfig);
		case METHOD_PATCH:
			return new JarawakHttp(conf).doPATCH(endpoint, params, jso, encoded, headerparams, useconfig);
		}
		return null;
	}

	/*
	 * #########################################################################
	 * ##### CALL WS Return Array of Object
	 * #########################################################################
	 * #####
	 */

	public Object[] callArray(String classname, String arrayparam, String wshost, String wsname, String[][] params,
			JSONObject jso, boolean encoded, int method, String[][] headerparams) throws JarawakException {
		return callArray(classname, arrayparam, wshost, wsname, params, jso, encoded, method, headerparams, false);
	}

	public Object[] callArray(String classname, String arrayparam, String wshost, String wsname, String[][] params,
			JSONObject jso, boolean encoded, int method, String[][] headerparams, boolean useconfig)
			throws JarawakException {
		JarawakException e = new JarawakException();

		if (classname == null || classname.trim().length() <= 0) {
			e.setType("E");
			e.setCode("WEBSERVICE");
			e.setMessage("You have to inform a classname.");
			throw e;
		}

		String callres = callWSAS(wshost, wsname, params, jso, encoded, method, headerparams, useconfig);
		if (callres != null && callres.trim().indexOf("[") >= 0) {
			try {
				JSONArray jsa = null;
				if (arrayparam != null && arrayparam.trim().length() > 0) {
					JSONObject jsoret = new JSONObject(callres);
					if (jso.has(arrayparam))
						jsa = jsoret.getJSONArray(arrayparam);
				} else {
					jsa = new JSONArray(callres);
				}
				if (jsa != null) {
					Object uout = Array.newInstance(Class.forName(classname), jsa.length());
					Object[] rout = (Object[]) uout;
					rout = json2ObjectA(rout, jsa, true);
					return rout;
				}
				return null;
			} catch (Exception ex) {
				e.setType("E");
				e.setCode("WEBSERVICE");
				e.setMessage("Error parsing Data");
				throw e;
			}
		} else {
			e.setType("E");
			e.setCode("WEBSERVICE");
			e.setMessage("Web Service not executed");
			throw e;
		}
	}

	/*
	 * #########################################################################
	 * ##### CALL WS Return Simple Object
	 * #########################################################################
	 * #####
	 */
	public Object call(String classname, String wshost, String wsname, String[][] params, JSONObject jso,
			boolean encoded, int method, String[][] headerparams) throws JarawakException {
		return call(classname, wshost, wsname, params, jso, encoded, method, headerparams, false);
	}

	public Object call(String classname, String wshost, String wsname, String[][] params, JSONObject jso,
			boolean encoded, int method, String[][] headerparams, boolean useconfig) throws JarawakException {
		JarawakException e = new JarawakException();

		if (classname == null || classname.trim().length() <= 0) {
			e.setType("E");
			e.setCode("WEBSERVICE");
			e.setMessage("You have to inform a classname.");
			throw e;
		}

		String callres = callWSAS(wshost, wsname, params, jso, encoded, method, headerparams, useconfig);
		if (callres != null) {
			try {
				Object ref = Class.forName(classname).getConstructors()[0].newInstance();
				JSONObject jsoret = new JSONObject(callres);
				return json2Object(ref, jsoret, true);
			} catch (Exception ex) {
				e.setType("E");
				e.setCode("WEBSERVICE");
				e.setMessage("Error parsing Data");
				throw e;
			}
		} else {
			e.setType("E");
			e.setCode("WEBSERVICE");
			e.setMessage("Web Service not executed");
			throw e;
		}
	}

	/*
	 * #########################################################################
	 * ##### CALL WS Return String JSON
	 * #########################################################################
	 * #####
	 */

	public String callSimple(String wshost, String wsname, String[][] params, JSONObject jso, boolean encoded,
			int method, String[][] headerparams) throws JarawakException {

		return callSimple(wshost, wsname, params, jso, encoded, method, headerparams, false);
	}

	public String callSimple(String wshost, String wsname, String[][] params, JSONObject jso, boolean encoded,
			int method, String[][] headerparams, boolean useconfig) throws JarawakException {
		JarawakException e = new JarawakException();
		String callres = callWSAS(wshost, wsname, params, jso, encoded, method, headerparams, useconfig);
		if (callres != null) {
			return callres;
		} else if (method != METHOD_PATCH) {
			e.setType("E");
			e.setCode("WEBSERVICE");
			e.setMessage("Web Service not executed");
			throw e;
		}
		return null;
	}

	/*
	 * #########################################################################
	 * ##### Convert Object to JSONObject
	 * #########################################################################
	 * #####
	 */

	public JSONObject lin2json(Object o, boolean encoded) {
		return lin2json(o, encoded, false);
	}

	public JSONObject lin2json(Object o, boolean encoded, boolean ignorenull) {
		if (o == null)
			return null;
		JSONObject jout = new JSONObject();
		JarawakModel mod = null;
		JarawakParam[] metas = null;
		JarawakParam meta = null;
		// Check if object is model
		if (o.getClass().getSuperclass().getName().indexOf("JarawakModel") >= 0) {
			mod = (JarawakModel) o;
			metas = mod.getMetainfodb();
		}

		// Get current class and superclass fields
		Field[] ofields = null;
		Field[] supfields = null;
		Field[] allfields = null;
		ofields = o.getClass().getDeclaredFields();
		if (o.getClass().getSuperclass() != null) {
			supfields = o.getClass().getSuperclass().getDeclaredFields();
			allfields = new Field[ofields.length + supfields.length];
			int c = 0;
			for (Field f : ofields) {
				allfields[c] = f;
				c++;
			}
			for (Field f : supfields) {
				allfields[c] = f;
				c++;
			}
		} else {
			allfields = ofields;
		}

		for (Field f : allfields) {

			if (f == null)
				continue;

			String val = null;

			// Get metainfo
			meta = null;
			if (metas != null && metas.length > 0)
				for (JarawakParam p : metas)
					if (p.name.equals(f.getName())) {
						meta = p;
						break;
					}

			// Only public fields
			if (Modifier.isPublic(f.getModifiers())) {
				try {
					if (f.get(o) != null) {
						if (meta != null && (meta.value.equals(JarawakParam.TYPE_BIGINTEGER)
								|| meta.value.equals(JarawakParam.TYPE_INTEGER)
								|| meta.value.equals(JarawakParam.TYPE_NUMERIC))) {
							val = (String) f.get(o);
							if (val != null) {
								if (val.indexOf('.') >= 0) {
									try {
										jout.put(f.getName(), Double.valueOf(val).doubleValue());
									} catch (Exception e2) {

									}
								} else {
									try {
										jout.put(f.getName(), Long.valueOf(val));
									} catch (Exception e) {
										try {
											jout.put(f.getName(), Double.valueOf(val));
										} catch (Exception e2) {

										}
									}
								}
							}

						} else if (f.get(o).getClass().isArray()) {
							if (f.get(o).getClass().getName().equals("[L" + String.class.getName() + ";")) {
								jout.put(f.getName(), (String[]) f.get(o));
							} else {
								JSONArray ao = lin2json((Object[]) f.get(o), encoded, ignorenull);
								jout.put(f.getName(), ao);
							}
						} else {

							if (f.get(o).getClass().getName().toUpperCase().indexOf("STRING") < 0
									&& f.get(o).getClass().getName().toUpperCase().indexOf("INTEGER") < 0
									&& f.get(o).getClass().getName().toUpperCase().indexOf("LONG") < 0
									&& f.get(o).getClass().getName().toUpperCase().indexOf("FLOAT") < 0) {
								JSONObject t_jo = lin2json(f.get(o), encoded, ignorenull);
								if (t_jo != null)
									jout.put(f.getName(), t_jo);
								else
									jout.put(f.getName(), "");
							} else {
								if (!f.get(o).getClass().getName().equals(String.class.getName())) {
									JSONObject jo = lin2json(f.get(o), encoded);
									jout.put(f.getName(), jo);
								} else {
									val = (String) f.get(o);
									// Check if is Array object
									if (val != null && val.trim().startsWith("[")) {
										try {
											JSONArray ja = new JSONArray(val);
											jout.put(f.getName(), ja);
											continue;
										} catch (Exception e) {

										}
									}
									// Check if is object
									if (val != null && val.trim().startsWith("{")) {
										try {
											JSONObject jo = new JSONObject(val);
											jout.put(f.getName(), jo);
											continue;
										} catch (Exception e) {

										}
									}
									if (val != null) {
										if (encoded)
											jout.put(f.getName(), URLEncoder.encode(val, encoding));
										else
											jout.put(f.getName(), val);
									} else if (!ignorenull)
										jout.put(f.getName(), "");
								}
							}
						}
					} else {
						if (!ignorenull)
							jout.put(f.getName(), "");
					}
				} catch (Exception e) {

				}
			}
		}
		return jout;
	}

	/*
	 * #########################################################################
	 * ##### Convert Object to JSONArray
	 * #########################################################################
	 * #####
	 */
	public JSONArray lin2json(Object[] ao, boolean encoded) {
		return lin2json(ao, encoded, false);
	}

	public JSONArray lin2json(Object[] ao, boolean encoded, boolean ignorenull) {
		if (ao == null)
			return null;
		JSONArray jout = new JSONArray();
		for (Object o : ao) {

			if (o == null)
				continue;

			JarawakModel mod = null;
			JarawakParam[] metas = null;
			JarawakParam meta = null;

			// Check if object is model
			if (o.getClass().getSuperclass().getName().indexOf("JarawakModel") >= 0) {
				mod = (JarawakModel) o;
				metas = mod.getMetainfodb();
			}

			JSONObject jso = new JSONObject();

			// Get current class and superclass fields
			Field[] ofields = null;
			Field[] supfields = null;
			Field[] allfields = null;
			ofields = o.getClass().getDeclaredFields();
			if (o.getClass().getSuperclass() != null) {
				supfields = o.getClass().getSuperclass().getDeclaredFields();
				allfields = new Field[ofields.length + supfields.length];
				int c = 0;
				for (Field f : ofields) {
					allfields[c] = f;
					c++;
				}
				for (Field f : supfields) {
					allfields[c] = f;
					c++;
				}
			} else {
				allfields = ofields;
			}
			for (Field f : allfields) {
				// Get metainfo
				meta = null;
				if (metas != null && metas.length > 0)
					for (JarawakParam p : metas)
						if (p.name.equals(f.getName())) {
							meta = p;
							break;
						}

				// Only public fields
				try {
					if (f.get(o) != null) {
						String val = null;
						if (Modifier.isPublic(f.getModifiers()) && f.get(o) != null) {
							if (meta != null && (meta.value.equals(JarawakParam.TYPE_BIGINTEGER)
									|| meta.value.equals(JarawakParam.TYPE_INTEGER)
									|| meta.value.equals(JarawakParam.TYPE_NUMERIC))) {
								val = (String) f.get(o);
								if (val != null) {
									try {
										jso.put(f.getName(), Long.valueOf(val));
									} catch (Exception e) {
										try {
											jso.put(f.getName(), Double.valueOf(val));
										} catch (Exception e2) {

										}
									}
								}
							} else if (f.get(o).getClass().isArray()) {
								if (f.get(o).getClass().getName().equals("[L" + String.class.getName() + ";")) {
									jso.put(f.getName(), (String[]) f.get(o));
								} else {
									JSONArray aao = lin2json((Object[]) f.get(o), encoded, ignorenull);
									jso.put(f.getName(), aao);
								}
							} else {

								if (f.get(o).getClass().getName().toUpperCase().indexOf("STRING") < 0
										&& f.get(o).getClass().getName().toUpperCase().indexOf("INTEGER") < 0
										&& f.get(o).getClass().getName().toUpperCase().indexOf("LONG") < 0
										&& f.get(o).getClass().getName().toUpperCase().indexOf("FLOAT") < 0) {
									JSONObject t_jo = lin2json(f.get(o), encoded);
									if (t_jo != null)
										jso.put(f.getName(), t_jo);
									else
										jso.put(f.getName(), "");
								} else {
									if (!f.get(o).getClass().getName().equals(String.class.getName())) {
										JSONObject jo = lin2json(f.get(o), encoded);
										jso.put(f.getName(), jo.toString());
									} else {
										val = (String) f.get(o);
										if (val != null) {
											if (encoded)
												jso.put(f.getName(), URLEncoder.encode(val, encoding));
											else
												jso.put(f.getName(), val);
										} else if (!ignorenull)
											jso.put(f.getName(), "");
									}
								}
							}
						}
					} // public fields
				} catch (Exception e) {
					if (!ignorenull)
						try {
							jso.put(f.getName(), "");
						} catch (JSONException e1) {
						}
				}

			}
			jout.put(jso);
		}
		return jout;
	}

	/*
	 * #########################################################################
	 * ##### Convert JSONObject to Object
	 * #########################################################################
	 * #####
	 */

	public Object json2Object(Object o, JSONObject jso, boolean decode) {
		if (jso == null)
			return null;
		if (o == null)
			return null;
		if (jso.length() <= 0)
			return null;

		JSONArray jsa = new JSONArray();
		Iterator<String> iter = jso.keys();
		while (iter.hasNext()) {
			String key = iter.next();
			try {
				if (jso.get(key).getClass().equals(jsa.getClass())) {
					// IS JSON ARRAY?
					String clname = o.getClass().getDeclaredField(key.replaceAll("-", "_")).getType().getName();
					if (clname.trim().indexOf("[") == 0)
						clname = clname.substring(2, clname.length() - 1);

					Class<?> clazz = Class.forName(clname);
					Object[] ao = (Object[]) Array.newInstance(clazz, ((JSONArray) jso.get(key)).length());

					for (int cnob = 0; cnob < ao.length; cnob++) {
						for (Constructor<?> constr : clazz.getConstructors()) {
							if (constr.getParameterTypes().length == 0) {
								ao[cnob] = constr.newInstance();
								break;
							}
							if (constr.getParameterTypes().length == 2) {
								ao[cnob] = constr.newInstance(null, null);
								break;
							}
						}
					}
					ao = json2ObjectA(ao, (JSONArray) jso.get(key), decode);
					if (ao != null) {
						o.getClass().getDeclaredField(key.replaceAll("-", "_")).set(o, ao);
						if (o.getClass().getSuperclass() != null)
							o.getClass().getSuperclass().getDeclaredField(key.replaceAll("-", "_")).set(o, ao);
					}
				} else if (jso.get(key).getClass().equals(jso.getClass())) {
					// IS JSON OBJECT?
					String clname = o.getClass().getDeclaredField(key.replaceAll("-", "_")).getType().getName();
					if (clname.trim().indexOf("[") == 0)
						clname = clname.substring(2, clname.length() - 1);

					Object no = null;

					if (clname.contentEquals(String.class.getName())) {
						no = jso.getJSONObject(key).toString();
					} else {
						Class<?> clazz = Class.forName(clname);
						for (Constructor<?> constr : clazz.getConstructors()) {
							if (constr.getParameterTypes().length == 0) {
								no = constr.newInstance();
								break;
							}
							if (constr.getParameterTypes().length == 2) {
								no = constr.newInstance(null, null);
								break;
							}
						}
						no = json2Object(no, jso.getJSONObject(key), decode);
					}
					Field df = null;
					try {
						df = o.getClass().getDeclaredField(key.replaceAll("-", "_"));
					} catch (Exception ex) {
						df = o.getClass().getSuperclass().getDeclaredField(key.replaceAll("-", "_"));
					}
					if (df != null) {
						df.set(o, no);
					}
				} else {
					// IS OTHER OBJECT (STRING)?
					Field df = null;
					try {
						df = o.getClass().getDeclaredField(key.replaceAll("-", "_"));
					} catch (Exception ex) {
						df = o.getClass().getSuperclass().getDeclaredField(key.replaceAll("-", "_"));
					}
					if (df != null) {
						if (decode)
							df.set(o, URLDecoder.decode((String) jso.get(key).toString(), encoding));
						else
							df.set(o, (String) jso.get(key).toString());
					}
				}
			} catch (Exception e) {
			}
		}

		return o;
	}

	/*
	 * #########################################################################
	 * ##### Convert JSON Array to Object Array
	 * #########################################################################
	 * #####
	 */
	public Object[] json2ObjectA(Object[] o, JSONArray jsa, boolean decode) {
		if (jsa == null)
			return null;
		if (o == null)
			return null;
		if (jsa.length() <= 0)
			return null;
		for (int i = 0; i < jsa.length(); i++) {
			try {
				String clname = o.getClass().getName();
				String clnameredux = clname.substring(2, clname.length() - 1);
				Class<?> clazz = Class.forName(clnameredux);
				if (o[i] == null) {
					for (Constructor<?> constr : clazz.getConstructors()) {
						if (constr.getParameterTypes().length == 0) {
							o[i] = constr.newInstance();
							break;
						}
					}
				}
				if (jsa.get(i).getClass().equals(jsa.getClass())) {
					// is array
					Object[] ao = json2ObjectA(o, jsa.getJSONArray(i), decode);
					o[i] = ao;
				} else {
					// Is object
					Object oo = json2Object(o[i], jsa.getJSONObject(i), decode);
					o[i] = oo;
				}
			} catch (Exception e) {

			}
		}
		return o;
	}

	/*
	 * #########################################################################
	 * ##### Convert DBLIN to Object Array
	 * #########################################################################
	 * #####
	 */
	public static Object[] convDBL2O(Class<?> cls, ArrayList<JarawakParam> keys, ArrayList<DBLin> al,
			JarawakConnector con, String encoding, boolean bringtotal) throws JarawakException {

		if (encoding == null || encoding.trim().length() <= 0)
			encoding = "UTF-8";

		Object[] out = (Object[]) Array.newInstance(cls, al.size());
		int c = 0;

		if (al != null && al.size() > 0) {
			String param = null;
			for (DBLin lin : al) {
				try {
					Class<?> clazz = null;
					ClassLoader cl = cls.getClassLoader();
					try {
						clazz = cl.loadClass(cls.getName());
					} catch (Exception e) {
						clazz = Class.forName(cls.getName());
					}
					Constructor<?> ctor = clazz.getConstructor(JarawakConnector.class, String.class);
					out[c] = ctor.newInstance(new Object[] { con, encoding });
					for (DBParVal p : lin.cols) {
						param = p.param;
						if (param.indexOf('.') >= 0)
							param = param.replace('.', '_');
						try {
							if (p.value != null) {
								p.value = p.value.replaceAll("\n", "\\n");
								JarawakParam mi = con.getMetaInfoTypeP(param);
								if (mi != null && mi.value != null)
									param = mi.name;
								if (mi != null && mi.value != null) {
									if (mi.value.trim().equals(JarawakParam.TYPE_ENCODED_STRING)) {
										p.value = URLDecoder.decode(p.value, encoding);
									}
									if (mi.value.trim().equals(JarawakParam.TYPE_EXCLUDED))
										continue;
								}
								Field f = null;
								try {
									if (param.trim().equals("totalrows"))
										f = out[c].getClass().getSuperclass().getDeclaredField(param);
									else
										f = out[c].getClass().getDeclaredField(param);
								} catch (Exception e) {
									continue;
								}
								if (f == null)
									continue;
								String classname = f.getType().getName();
								if (classname.trim().indexOf("[") == 0)
									classname = classname.substring(2, classname.length() - 1);
								if (f.getType().getName().equals("java.lang.String"))
									if (param.trim().equals("totalrows"))
										out[c].getClass().getSuperclass().getDeclaredField(param).set(out[c], p.value);
									else
										out[c].getClass().getDeclaredField(param).set(out[c], p.value);
								else {
									Class<?> clz = null;
									try {
										clz = cl.loadClass(classname);
									} catch (Exception e) {
										clz = Class.forName(classname);
									}
									Constructor<?> constr = clz.getConstructors()[0];
									Object o = null;
									if (constr.getParameterTypes().length == 0)
										o = constr.newInstance();
									else if (constr.getParameterTypes().length == 2)
										o = constr.newInstance(
												new Object[] { new WSConnector(new WSConfig(), null, null), null });

									if (mi != null && mi.value != null
											&& mi.value.trim().equals(JarawakParam.TYPE_OBJECT)) {
										try {
											JSONObject jso = new JSONObject(p.value);
											JarawakWS ws = new JarawakWS();
											o = ws.json2Object(o, jso, true);
											if (o != null)
												out[c].getClass().getDeclaredField(param).set(out[c], o);
										} catch (Exception e) {

										}
									} else if (mi != null && mi.value != null
											&& mi.value.trim().equals(JarawakParam.TYPE_ARRAY_OBJECT)) {
										try {
											JSONArray jsa = new JSONArray(p.value);
											JarawakWS ws = new JarawakWS();
											Object[] a = (Object[]) Array.newInstance(clz, jsa.length());
											for (int ci = 0; ci < a.length; ci++)
												if (constr.getParameterTypes().length == 0)
													a[ci] = constr.newInstance();
												else if (constr.getParameterTypes().length == 2)
													a[ci] = constr.newInstance(new Object[] { null, null });
											if (a != null)
												a = ws.json2ObjectA(a, jsa, false);
											out[c].getClass().getDeclaredField(param).set(out[c], a);
										} catch (Exception e) {

										}
									} else {
										try {
											out[c].getClass().getDeclaredField(param).set(out[c], p.value);
										} catch (Exception cex) {

										}
									}
								}
							}
						} catch (Exception e) {

						}
					}
				} catch (Exception e) {
					throw new JarawakException("E", "MODEL", "model.e.moderrcreateinst");
				}
				if (out != null && out[c] != null && bringtotal && con.totalrows != null) {
					try {
						out[c].getClass().getSuperclass().getDeclaredField("totalrows").set(out[c], con.totalrows);
					} catch (Exception e) {
					}
				}

				JSONObject jso = prepareJSON(out[c], null, false, null, false, false, con);

				if (jso != null && jso.names().length() > 0) {
					for (int i = 0; i < jso.names().length(); i++) {
						String name = null;
						// ON METHODS
						try {
							name = jso.names().getString(i);
							Method[] methods = out[c].getClass().getDeclaredMethods();
							for (int j = 0; j < methods.length; j++)
								// ON SELECT
								if (methods[j].getName().equals("_onselect_" + name)) {
									try {
										methods[j].invoke(out[c], keys, name);
									} catch (Exception ee) {

									}
									break;
								}
						} catch (Exception e) {

						}

					}
				}

				c++;
			}
		} else {
			throw new JarawakException("E", "MODEL", "model.e.emptyresult");
		}

		return out;
	}

	/*
	 * #########################################################################
	 * ##### PREPARE THIS JSON
	 * #########################################################################
	 * #####
	 */
	public static JSONObject prepareJSON(Object obj2J, String ai, boolean exclude, ArrayList<JarawakParam> addpars,
			boolean encode, boolean ignorenull, JarawakConnector con) {
		if (obj2J == null)
			return null;

		JarawakWS ws = new JarawakWS();
		JSONObject jso = null;
		try {
			jso = ws.lin2json(obj2J, encode, ignorenull);
			if (addpars != null && addpars.size() > 0) {
				for (JarawakParam p : addpars) {
					jso.put(p.name, p.value);
				}
			}
		} catch (Exception e) {

		}
		// Remove (additional) not String fields
		if (jso != null && jso.length() > 0) {
			JSONObject jsout = new JSONObject();
			int ttl = jso.names().length();
			for (int i = 0; i < ttl; i++) {
				try {
					String n = jso.names().getString(i);
					if (ai != null && n.equalsIgnoreCase(ai))
						continue;
					if (n.equals("table_name") || n.equals("f_page") || n.equals("f_limit") || n.equals("totalrows"))
						continue;
					if (exclude) {
						JarawakParam mi = con.getMetaInfoTypeP(n);
						if (mi != null) {
							if (mi.value.equals(JarawakParam.TYPE_ARRAY_OBJECT)
									|| mi.value.equals(JarawakParam.TYPE_OBJECT)
									|| mi.value.equals(JarawakParam.TYPE_EXCLUDED)) {
								continue;
							}
						}
					}
					jsout.put(n, jso.get(n));
				} catch (Exception e) {

				}
			}
			return jsout;
		}
		return null;
	}
}
