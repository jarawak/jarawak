package jarawak.connectors;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakParam;
import jarawak.JarawakWS;
import jarawak.JarawakWSAndroid;
import jarawak.config.WSConfig;
import jarawak.db.DBLin;
import jarawak.db.DBParVal;

public class WSConnector extends JarawakConnector {
	private static final long serialVersionUID = 1L;
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */
	public WSConfig conf = null;
	public JarawakWS webservice = null;

	/*
	 * #########################################################################
	 * ##### CONSTRUCTOR
	 * #########################################################################
	 * #####
	 */
	public WSConnector(Object config, String endpoint, JarawakParam[] metainfodb) {
		super(config, endpoint, metainfodb);

		if (config.getClass().getName().trim().equals("jarawak.config.WSConfig"))
			conf = (WSConfig) config;
		webservice = new JarawakWS(conf);
	}

	public WSConnector(Object config, Object webservice, String endpoint, JarawakParam[] metainfodb) {
		super(config, endpoint, metainfodb);

		if (config.getClass().getName().trim().equals("jarawak.config.WSConfig"))
			conf = (WSConfig) config;

		if (webservice.getClass().getName().equals("jarawak.JarawakWS"))
			this.webservice = new JarawakWS(conf);
		else if (webservice.getClass().getName().equals("jarawak.JarawakWSAndroid"))
			this.webservice = new JarawakWSAndroid();
	}

	/*
	 * #########################################################################
	 * ##### LIST
	 * #########################################################################
	 * #####
	 */

	@Override
	public ArrayList<DBLin> list(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal) throws JarawakException {
		return list(keys, ord, page, limit, bringtotal, false);
	}

	@Override
	public ArrayList<DBLin> list(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal, boolean intercepterror) throws JarawakException {
		return list(keys, ord, page, limit, bringtotal, intercepterror, false);
	}

	@Override
	public ArrayList<DBLin> list(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal, boolean intercepterror, boolean useconfig) throws JarawakException {

		String params = JarawakParam.PARAM_ON_REQUEST;
		String[][] parameters = null;
		JSONObject jso = null;
		String endpoint = super.getEndpoint();
		ArrayList<DBLin> out = null;
		int method = JarawakWS.METHOD_GET;

		if (conf.method_list != null && conf.method_list.trim().length() > 0)
			method = getMethod(conf.method_list);

		// Prepare to send
		if (conf.params_list != null && conf.params_list.trim().length() > 0)
			params = getParamsMode(conf.params_list);
		switch (params) {
		case JarawakParam.PARAM_ON_REQUEST:
			if (keys != null && keys.size() > 0) {
				for (JarawakParam p : keys) {
					if (p != null && p.name != null && p.value != null) {
						String val = p.value;
						if (endpoint.indexOf("?") < 0)
							endpoint = endpoint + "?";
						else
							endpoint = endpoint + "&";
						if (conf.encoded)
							try {
								val = URLEncoder.encode(val, conf.encoding);
							} catch (Exception e) {

							}
						endpoint = endpoint + p.name + "=" + val;
						if (p.mode != null && p.mode.trim().length() > 0)
							endpoint = endpoint + "&" + p.name + "_mode=" + p.mode;
					}
				}
			}
			break;
		case JarawakParam.PARAM_AS_PARAMETER:
			if (keys != null && keys.size() > 0) {
				parameters = new String[keys.size()][2];
				int i = 0;
				for (JarawakParam p : keys) {
					if (p.value != null) {
						parameters[i][0] = p.name;
						if (conf.encoded)
							try {
								parameters[i][1] = URLEncoder.encode(p.value, conf.encoding);
							} catch (Exception e) {
								parameters[i][1] = p.value;
							}
						else
							parameters[i][1] = p.value;
						if (p.mode != null && p.mode.trim().length() > 0) {
							parameters[i][0] = p.name + "_mode";
							parameters[i][1] = p.mode;
							i++;
						}
					}
					i++;
				}
			}
			break;
		case JarawakParam.PARAM_AS_JSON:
			if (keys != null && keys.size() > 0) {
				jso = new JSONObject();
				for (JarawakParam p : keys) {
					try {
						if (p.value != null)
							if (conf.encoded)
								try {
									jso.put(p.name, (String) URLEncoder.encode(p.value, conf.encoding));
								} catch (Exception e) {
									jso.put(p.name, (String) p.value);
								}
							else
								jso.put(p.name, (String) p.value);
						if (p.mode != null && p.mode.trim().length() > 0) {
							jso.put(p.name + "_mode", (String) p.mode);
						}
					} catch (Exception e) {

					}
				}
			}
			break;
		}

		try {
			String ret = webservice.callSimple(conf.host, endpoint, parameters, jso, conf.encoded, method, getHeaders(),
					useconfig);
			if (ret != null) {
				if (intercepterror) {
					try {
						JSONObject exjo = new JSONObject(ret);
						if (exjo.has("errortype") && exjo.has("errormessage")) {
							throw new JarawakException(exjo.getString("errortype"), exjo.getString("errorcode"),
									exjo.getString("errormessage"));
						}
					} catch (Exception e) {
					}
				}
				try {
					JSONArray jaret = new JSONArray(ret);
					if (jaret != null && jaret.length() > 0) {
						out = new ArrayList<DBLin>();
						for (int a = 0; a < jaret.length(); a++) {
							JSONObject jsret = (JSONObject) jaret.get(a);
							out.add(conv2Lin(jsret.toString()));
						}
						return out;
					}
				} catch (Exception e) {
				}
			} else {

			}
		} catch (JarawakException e) {
			throw e;
		}
		throw new JarawakException("E", "CONNECTOR", "connector.e.notfound");

	}

	/*
	 * #########################################################################
	 * ##### DELETE
	 * #########################################################################
	 * #####
	 */

	@Override
	public String delete(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return delete(keys, uid, false);
	}

	@Override
	public String delete(ArrayList<JarawakParam> keys, String uid, boolean intercepterror) throws JarawakException {
		return delete(keys, uid, false, null);
	}

	public String delete(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, String sessionuser)
			throws JarawakException {

		return delete(keys, uid, false, sessionuser, false);
	}

	@Override
	public String delete(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, String sessionuser,
			boolean useconfig) throws JarawakException {
		String params = JarawakParam.PARAM_ON_REQUEST;
		String[][] parameters = null;
		JSONObject jso = null;
		String endpoint = super.getEndpoint();

		int method = JarawakWS.METHOD_DELETE;

		if (conf.method_delete != null && conf.method_delete.trim().length() > 0)
			method = getMethod(conf.method_delete);

		// SETUID
		if (uid != null && uid.trim().length() > 0)
			endpoint = endpoint + "/" + uid;

		// Prepare to send
		if (conf.params_delete != null && conf.params_delete.trim().length() > 0)
			params = getParamsMode(conf.params_delete);
		switch (params) {
		case JarawakParam.PARAM_ON_REQUEST:
			if (keys != null && keys.size() > 0) {
				for (JarawakParam p : keys) {
					if (p != null && p.name != null && p.value != null) {
						String val = p.value;
						if (endpoint.indexOf("?") < 0)
							endpoint = endpoint + "?";
						else
							endpoint = endpoint + "&";
						if (conf.encoded)
							try {
								val = URLEncoder.encode(val, conf.encoding);
							} catch (Exception e) {

							}
						endpoint = endpoint + p.name + "=" + val;
						if (p.mode != null && p.mode.trim().length() > 0)
							endpoint = endpoint + "&" + p.name + "_mode=" + p.mode;
					}
				}
			}
			break;
		case JarawakParam.PARAM_AS_PARAMETER:
			if (keys != null && keys.size() > 0) {
				parameters = new String[keys.size()][2];
				int i = 0;
				for (JarawakParam p : keys) {
					if (p.value != null) {
						parameters[i][0] = p.name;
						if (conf.encoded)
							try {
								parameters[i][1] = URLEncoder.encode(p.value, conf.encoding);
							} catch (Exception e) {
								parameters[i][1] = p.value;
							}
						else
							parameters[i][1] = p.value;
						if (p.mode != null && p.mode.trim().length() > 0) {
							parameters[i][0] = p.name + "_mode";
							parameters[i][1] = p.mode;
							i++;
						}
					}
					i++;
				}
			}
			break;
		case JarawakParam.PARAM_AS_JSON:
			if (keys != null && keys.size() > 0) {
				jso = new JSONObject();
				for (JarawakParam p : keys) {
					try {
						if (p.value != null)
							if (conf.encoded)
								try {
									jso.put(p.name, (String) URLEncoder.encode(p.value, conf.encoding));
								} catch (Exception e) {
									jso.put(p.name, (String) p.value);
								}
							else
								jso.put(p.name, (String) p.value);
						if (p.mode != null && p.mode.trim().length() > 0) {
							jso.put(p.name + "_mode", (String) p.mode);
						}

					} catch (Exception e) {

					}
				}
			}
			break;
		}

		try {
			String ret = webservice.callSimple(conf.host, endpoint, parameters, jso, conf.encoded, method, getHeaders(),
					useconfig);
			if (ret != null) {
				if (intercepterror) {
					try {
						JSONObject exjo = new JSONObject(ret);
						if (exjo.has("errortype") && exjo.has("errormessage")) {
							throw new JarawakException(exjo.getString("errortype"), exjo.getString("errorcode"),
									exjo.getString("errormessage"));
						}
					} catch (Exception e) {
					}
				}
				return ret;
			} else {

			}
		} catch (JarawakException e) {
			throw e;
		}
		throw new JarawakException("E", "CONNECTOR", "connector.e.notdeleted");
	}

	/*
	 * #########################################################################
	 * ##### CREATE
	 * #########################################################################
	 * #####
	 */

	@Override
	public String create(String pk, JSONObject fields) throws JarawakException {
		return create(pk, fields, false);
	}

	@Override
	public String create(String pk, JSONObject fields, boolean intercepterror) throws JarawakException {
		return create(pk, fields, intercepterror, false);

	}

	@Override
	public String create(String pk, JSONObject fields, boolean intercepterror, boolean useconfig)
			throws JarawakException {
		String params = JarawakParam.PARAM_ON_REQUEST;
		String[][] parameters = null;
		JSONObject jso = null;
		String endpoint = super.getEndpoint();
		int method = JarawakWS.METHOD_POST;

		if (conf.method_create != null && conf.method_create.trim().length() > 0)
			method = getMethod(conf.method_create);

		// Prepare to send
		if (conf.params_create != null && conf.params_create.trim().length() > 0)
			params = getParamsMode(conf.params_create);
		switch (params) {
		case JarawakParam.PARAM_ON_REQUEST:
			if (fields != null && fields.names().length() > 0) {
				for (int i = 0; i < fields.names().length(); i++) {
					String p = null;
					String v = null;
					try {
						p = fields.names().getString(i);
						v = fields.getString(p);
					} catch (Exception e) {

					}
					if (p != null && p != null && v != null) {
						String val = v;
						if (endpoint.indexOf("?") < 0)
							endpoint = endpoint + "?";
						else
							endpoint = endpoint + "&";
						if (conf.encoded)
							try {
								val = URLEncoder.encode(val, conf.encoding);
							} catch (Exception e) {

							}
						endpoint = endpoint + p + "=" + val;
					}
				}
			}
			break;
		case JarawakParam.PARAM_AS_PARAMETER:
			if (fields != null && fields.names().length() > 0) {
				parameters = new String[fields.names().length()][2];
				int i = 0;
				for (int j = 0; j < fields.names().length(); j++) {
					String p = null;
					String v = null;
					try {
						p = fields.names().getString(j);
						v = fields.getString(p);
					} catch (Exception e) {

					}
					if (v != null) {
						parameters[i][0] = p;
						if (conf.encoded)
							try {
								parameters[i][1] = URLEncoder.encode(v, conf.encoding);
							} catch (Exception e) {
								parameters[i][1] = v;
							}
						else
							parameters[i][1] = v;
					}
					i++;
				}
			}
			break;
		case JarawakParam.PARAM_AS_JSON:
			if (fields != null && fields.names().length() > 0) {
				jso = fields;
			}
			break;
		}

		// SEND
		String ret = null;
		try {
			ret = webservice.callSimple(conf.host, endpoint, parameters, jso, conf.encoded, method, getHeaders(),
					useconfig);
		} catch (JarawakException e) {
			throw e;
		}
		if (ret != null && ret.trim().length() > 0) {
			if (intercepterror) {
				JSONObject exjo = null;
				String ecode = null;
				String etype = null;
				String emsg = null;
				try {
					exjo = new JSONObject(ret);
					if (exjo != null && exjo.has("errortype") && exjo.has("errormessage")) {
						ecode = exjo.getString("errorcode");
						etype = exjo.getString("errortype");
						emsg = exjo.get("errormessage").toString();
					}
				} catch (Exception e) {
				}

				if (exjo != null && etype != null) {
					throw new JarawakException(etype, ecode, emsg);
				}
			}
			return ret;
		}
		return null;
	}

	/*
	 * #########################################################################
	 * ##### SAVE
	 * #########################################################################
	 * #####
	 */

	@Override
	public String save(JSONObject fields, ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return save(fields, keys, uid, false);
	}

	@Override
	public String save(JSONObject fields, ArrayList<JarawakParam> keys, String uid, boolean intercepterror)
			throws JarawakException {
		return save(fields, keys, uid, intercepterror, false);
	}

	@Override
	public String save(JSONObject fields, ArrayList<JarawakParam> keys, String uid, boolean intercepterror,
			boolean useconfig) throws JarawakException {
		String params = JarawakParam.PARAM_ON_REQUEST;
		String[][] parameters = null;
		JSONObject jso = null;
		String endpoint = super.getEndpoint();
		int method = JarawakWS.METHOD_PUT;

		if (conf.method_save != null && conf.method_save.trim().length() > 0)
			method = getMethod(conf.method_save);

		// SETUID
		if (uid != null && uid.trim().length() > 0)
			endpoint = endpoint + "/" + uid;

		// Prepare to send
		if (conf.params_save != null && conf.params_save.trim().length() > 0)
			params = getParamsMode(conf.params_save);
		switch (params) {
		case JarawakParam.PARAM_ON_REQUEST:
			if (keys != null && keys.size() > 0) {
				for (JarawakParam p : keys) {
					if (p != null && p.name != null && p.value != null) {
						String val = p.value;
						if (endpoint.indexOf("?") < 0)
							endpoint = endpoint + "?";
						else
							endpoint = endpoint + "&";
						if (conf.encoded)
							try {
								val = URLEncoder.encode(val, conf.encoding);
							} catch (Exception e) {

							}
						endpoint = endpoint + p.name + "=" + val;
						if (p.mode != null && p.mode.trim().length() > 0)
							endpoint = endpoint + "&" + p.name + "_mode=" + p.mode;
					}
				}
			}
			if (fields != null) {
				for (int i = 0; i < fields.names().length(); i++) {
					String p = null;
					String v = null;
					try {
						p = fields.names().getString(i);
						v = fields.getString(p);
					} catch (Exception e) {

					}
					if (p != null && p != null && v != null) {
						String val = v;
						if (endpoint.indexOf("?") < 0)
							endpoint = endpoint + "?";
						else
							endpoint = endpoint + "&";
						if (conf.encoded)
							try {
								val = URLEncoder.encode(val, conf.encoding);
							} catch (Exception e) {

							}
						endpoint = endpoint + p + "=" + val;
					}
				}
			}
			break;
		case JarawakParam.PARAM_AS_PARAMETER:
			int ksize = 0, fsize = 0, pcount = 0;
			if (keys != null && keys.size() > 0)
				ksize = keys.size();
			if (fields != null)
				fsize = fields.names().length();
			parameters = new String[ksize + fsize][2];

			if (keys != null && keys.size() > 0) {
				for (JarawakParam p : keys) {
					if (p.name != null && p.value != null) {
						parameters[pcount][0] = p.name;
						if (conf.encoded)
							try {
								parameters[pcount][1] = URLEncoder.encode(p.value, conf.encoding);
							} catch (Exception e) {
								parameters[pcount][1] = p.value;
							}
						else
							parameters[pcount][1] = p.value;
						if (p.mode != null && p.mode.trim().length() > 0) {
							parameters[pcount][0] = p.name + "_mode";
							parameters[pcount][1] = p.mode;
							pcount++;
						}

					}
					pcount++;
				}
			}
			if (fields != null) {
				for (int i = 0; i < fields.names().length(); i++) {
					String p = null;
					String v = null;
					try {
						p = fields.names().getString(i);
						v = fields.getString(p);
					} catch (Exception e) {

					}
					if (p != null && v != null) {
						parameters[pcount][0] = p;
						if (conf.encoded)
							try {
								parameters[pcount][1] = URLEncoder.encode(v, conf.encoding);
							} catch (Exception e) {
								parameters[pcount][1] = v;
							}
						else
							parameters[pcount][1] = v;
					}
					pcount++;
				}
			}
			break;
		case JarawakParam.PARAM_AS_JSON:
			jso = new JSONObject();
			if (fields != null) {
				jso = fields;
			}
			if (keys != null && keys.size() > 0) {
				for (JarawakParam p : keys) {
					try {
						if (p.name != null && p.value != null)
							if (conf.encoded)
								try {
									jso.put(p.name, (String) URLEncoder.encode(p.value, conf.encoding));
								} catch (Exception e) {
									jso.put(p.name, (String) p.value);
								}
							else
								jso.put(p.name, (String) p.value);
						if (p.mode != null && p.mode.trim().length() > 0) {
							jso.put(p.name + "_mode", (String) p.mode);
						}
					} catch (Exception e) {

					}
				}
			}
			break;
		}

		// SEND
		try {
			String ret = webservice.callSimple(conf.host, endpoint, parameters, jso, conf.encoded, method, getHeaders(),
					useconfig);
			if (ret != null && ret.trim().length() > 0) {
				try {
					if (intercepterror) {
						try {
							JSONObject exjo = new JSONObject(ret);
							if (exjo.has("errortype") && exjo.has("errormessage")) {
								throw new JarawakException(exjo.getString("errortype"), exjo.getString("errorcode"),
										exjo.getString("errormessage"));
							}
						} catch (Exception e) {
						}
					}
					return ret;
				} catch (Exception e) {

				}
			} else {

			}
		} catch (JarawakException e) {
			throw new JarawakException(e.getType(), e.getCode(), e.getMessage());
		}
		if (method != JarawakWS.METHOD_PATCH)
			throw new JarawakException("E", "CONNECTOR", "connector.e.notsaved");
		else
			return null;
	}

	/*
	 * #########################################################################
	 * ##### GET
	 * #########################################################################
	 * #####
	 */

	@Override
	public DBLin get(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return get(keys, uid, false);
	}

	@Override
	public DBLin get(ArrayList<JarawakParam> keys, String uid, boolean intercepterror) throws JarawakException {
		return get(keys, uid, intercepterror, false);
	}

	@Override
	public DBLin get(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, boolean useconfig)
			throws JarawakException {
		String params = JarawakParam.PARAM_ON_REQUEST;
		String[][] parameters = null;
		JSONObject jso = null;
		String endpoint = super.getEndpoint();
		int method = JarawakWS.METHOD_GET;

		if (conf.method_get != null && conf.method_get.trim().length() > 0)
			method = getMethod(conf.method_get);

		// SETUID
		if (uid != null && uid.trim().length() > 0)
			endpoint = endpoint + "/" + uid;

		// Prepare to send
		if (conf.params_get != null && conf.params_get.trim().length() > 0)
			params = getParamsMode(conf.params_get);
		switch (params) {
		case JarawakParam.PARAM_ON_REQUEST:
			if (keys != null && keys.size() > 0) {
				for (JarawakParam p : keys) {
					if (p != null && p.name != null && p.value != null) {
						String val = p.value;
						if (endpoint.indexOf("?") < 0)
							endpoint = endpoint + "?";
						else
							endpoint = endpoint + "&";
						if (conf.encoded)
							try {
								val = URLEncoder.encode(val, conf.encoding);
							} catch (Exception e) {

							}
						endpoint = endpoint + p.name + "=" + val;
						if (p.mode != null && p.mode.trim().length() > 0)
							endpoint = endpoint + "&" + p.name + "_mode=" + p.mode;
					}
				}
			}
			break;
		case JarawakParam.PARAM_AS_PARAMETER:
			if (keys != null && keys.size() > 0) {
				parameters = new String[keys.size()][2];
				int i = 0;
				for (JarawakParam p : keys) {
					parameters[i][0] = p.name;
					if (conf.encoded)
						try {
							parameters[i][1] = URLEncoder.encode(p.value, conf.encoding);
						} catch (Exception e) {
							parameters[i][1] = p.value;
						}
					else
						parameters[i][1] = p.value;
					if (p.mode != null && p.mode.trim().length() > 0) {
						parameters[i][0] = p.name + "_mode";
						parameters[i][1] = p.mode;
						i++;
					}
					i++;
				}
			}
			break;
		case JarawakParam.PARAM_AS_JSON:
			if (keys != null && keys.size() > 0) {
				jso = new JSONObject();
				for (JarawakParam p : keys) {
					try {
						if (conf.encoded)
							try {
								jso.put(p.name, (String) URLEncoder.encode(p.value, conf.encoding));
							} catch (Exception e) {
								jso.put(p.name, (String) p.value);
							}
						else
							jso.put(p.name, (String) p.value);
						if (p.mode != null && p.mode.trim().length() > 0) {
							jso.put(p.name + "_mode", (String) p.mode);
						}
					} catch (Exception e) {

					}
				}
			}
			break;
		}

		// SEND
		try {
			String ret = webservice.callSimple(conf.host, endpoint, parameters, jso, conf.encoded, method, getHeaders(),
					useconfig);
			if (ret != null && ret.trim().length() > 0) {
				if (intercepterror) {
					try {
						JSONObject exjo = new JSONObject(ret);
						if (exjo.has("errortype") && exjo.has("errormessage")) {
							throw new JarawakException(exjo.getString("errortype"), exjo.getString("errorcode"),
									exjo.getString("errormessage"));
						}
					} catch (Exception e) {
					}
				}
				return conv2Lin(ret);
			} else {

			}
		} catch (JarawakException e) {
			throw e;
		}
		throw new JarawakException("E", "CONNECTOR", "connector.e.notfound");
	}

	/*
	 * #########################################################################
	 * ##### Convert Result String to DBLin
	 * #########################################################################
	 * #####
	 */

	private DBLin conv2Lin(String result) {
		DBLin out = null;
		try {
			JSONObject jsret = new JSONObject(result);
			if (jsret != null) {
				DBParVal[] pret = new DBParVal[jsret.length()];
				out = new DBLin();
				for (int i = 0; i < jsret.length(); i++) {
					/*
					 * if(getMetaInfoType(jsret.names().getString(i)) != null &&
					 * getMetaInfoType(jsret.names().getString(i)).equals(JarawakParam.TYPE_EXCLUDED
					 * )) continue;
					 */
					pret[i] = new DBParVal();
					pret[i].param = jsret.names().getString(i);
					String val = null;
					String compval = null;
					try {
						compval = JSONObject.numberToString((Number) jsret.get(pret[i].param));
					} catch (Exception e) {

					}
					if (val == null)
						try {
							val = jsret.getString(pret[i].param);
						} catch (Exception e) {

						}

					if (val == null && compval != null && compval.contains(".")) {
						try {
							val = String.valueOf(jsret.getDouble(pret[i].param));
						} catch (Exception e) {

						}
					}

					if (val == null)
						try {
							val = String.valueOf(jsret.getLong(pret[i].param));
						} catch (Exception e) {

						}

					if (val == null)
						try {
							val = String.valueOf(jsret.getDouble(pret[i].param));
						} catch (Exception e) {

						}

					if (val == null)
						try {
							val = String.valueOf(jsret.getInt(pret[i].param));
						} catch (Exception e) {

						}

					if (val == null)
						try {
							if (jsret.getBoolean(pret[i].param))
								val = "T";
							else
								val = "F";
						} catch (Exception e) {

						}

					if (val == null) {
						try {
							pret[i].value = jsret.getJSONObject(pret[i].param).toString();
						} catch (Exception e2) {
							try {
								pret[i].value = jsret.getJSONArray(pret[i].param).toString();
							} catch (Exception e3) {
								continue;
							}
						}
					} else {
						if (conf.encoded)
							try {
								pret[i].value = URLDecoder.decode(val, conf.encoding);
							} catch (Exception e) {
								pret[i].value = val;
							}
						else {
							pret[i].value = val;
						}
					}
				}
				out.cols = pret;
				return out;
			}
		} catch (Exception e) {

		}
		return out;
	}
}
