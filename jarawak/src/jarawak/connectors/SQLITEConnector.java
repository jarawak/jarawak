package jarawak.connectors;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONObject;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakParam;
import jarawak.config.DBConfig;
import jarawak.db.ConBase;
import jarawak.db.DBLin;

public class SQLITEConnector extends JarawakConnector {
	/*
	 * Copyright 2018 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	/**
	 * 
	 */
	private static final long serialVersionUID = -757763730152344509L;
	private DBConfig conf = null;
	private ConBase con = null;

	public SQLITEConnector(Object config, String endpoint, JarawakParam[] metainfotype) {
		super(config, endpoint, metainfotype);
		if (config.getClass().getName().equals("jarawak.config.DBConfig")) {
			conf = (DBConfig) config;
			con = new ConBase(conf, ConBase.DBD_SQLITE);
		}
	}

	/*
	 * #########################################################################
	 * ##### CREATE TABLE
	 * #########################################################################
	 * #####
	 */
	@Override
	public boolean createTable(String name, ArrayList<JarawakParam> fields, boolean force) throws JarawakException {
		boolean tableexist = false;

		if (con == null)
			throw new JarawakException("E", "CONNECTOR", "connector.e.noconnection");

		if (fields == null || fields.size() <= 0)
			throw new JarawakException("E", "CONNECTOR", "connector.e.nofields");

		// CHECK IF EXIST
		ArrayList<DBLin> al = con
				.readDb("SELECT name FROM sqlite_master WHERE type = 'table' AND name = '" + name + "' ");
		if (al != null && al.size() > 0)
			tableexist = true;

		// IF FORCE - DROP
		if (force && tableexist) {
			try {
				if (!con.updateDB("DROP TABLE " + name + "")) {
					throw new JarawakException("E", "CONNECTOR", "connector.e.tablenotdroped");
				}
			} catch (Exception ex) {
				throw new JarawakException("E", "CONNECTOR", "connector.e.tablenotdroped");
			}
		} else if (tableexist) {
			throw new JarawakException("E", "CONNECTOR", "connector.e.tableexist");
		}

		String createscript = "CREATE TABLE \"" + name + "\" ( ";
		boolean primaryset = false;
		int cntfields = 0;
		// FIELDS DEFINITION
		for (JarawakParam f : fields) {
			if (!f.value.equals(JarawakParam.TYPE_EXCLUDED) && !f.value.equals(JarawakParam.TYPE_ARRAY_OBJECT)
					&& !f.value.equals(JarawakParam.TYPE_OBJECT)) {
				// CHECK PRIMARY KEY
				if (!primaryset && f.primfore != null && f.primfore.equals(JarawakParam.KEY_PRIMARY)) {
					primaryset = true;
				}

				if (cntfields > 0)
					createscript = createscript + ", ";

				switch (f.value) {
				case JarawakParam.TYPE_BIGINTEGER:
					createscript = createscript + " " + f.name + " INTEGER ";
					if (f.mode != null && f.mode.equals(JarawakParam.DB_SERIAL))
						createscript = createscript + " PRIMARY KEY AUTOINCREMENT ";
					else {
						createscript = createscript + " NOT NULL ";
						if (!primaryset && f.primfore != null && f.primfore.equals(JarawakParam.KEY_PRIMARY))
							createscript = createscript + " PRIMARY KEY ";
						createscript = createscript + " DEFAULT 0 ";
					}
					break;

				case JarawakParam.TYPE_INTEGER:
					createscript = createscript + " " + f.name + " INTEGER ";
					if (f.mode != null && f.mode.equals(JarawakParam.DB_SERIAL))
						createscript = createscript + " PRIMARY KEY AUTOINCREMENT ";
					else {
						createscript = createscript + " NOT NULL ";
						if (!primaryset && f.primfore != null && f.primfore.equals(JarawakParam.KEY_PRIMARY))
							createscript = createscript + " PRIMARY KEY ";
						createscript = createscript + " DEFAULT 0 ";
					}
					break;
				case JarawakParam.TYPE_NUMERIC:
					createscript = createscript + " " + f.name + " REAL NOT NULL";
					break;
				case JarawakParam.TYPE_STRING:
				case JarawakParam.TYPE_ENCODED_STRING:
					if (f.mode != null) {
						createscript = createscript + " " + f.name + " TEXT";
					} else {
						createscript = createscript + " " + f.name + " TEXT";
					}
					if (!primaryset && f.primfore != null && f.primfore.equals(JarawakParam.KEY_PRIMARY))
						createscript = createscript + " PRIMARY KEY ";

					break;
				default:
					throw new JarawakException("E", "CONNECTOR", "connector.e.invalidtype");
				}
			}
			cntfields++;
		}

		createscript = createscript + " ) ";

		if (con.updateDB(createscript))
			return true;

		return false;
	}

	/*
	 * #########################################################################
	 * ##### LIST
	 * #########################################################################
	 * #####
	 */
	@Override
	public ArrayList<DBLin> list(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal) throws JarawakException {
		return list(keys, ord, page, limit, bringtotal, false);
	}

	@Override
	public ArrayList<DBLin> list(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal, boolean errorinterceptor) throws JarawakException {
		return list(keys, ord, page, limit, bringtotal, errorinterceptor, false);
	}

	@Override
	public ArrayList<DBLin> list(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal, boolean errorinterceptor, boolean useconfig) throws JarawakException {

		if (con == null)
			throw new JarawakException("E", "CONNECTOR", "connector.e.noconnection");

		String query = "SELECT * FROM " + getEndpoint() + " ";
		String querywhere = " WHERE ";
		String queryend = "";
		String querycount = "SELECT COUNT(*) AS rowstotal FROM " + getEndpoint() + " ";

		// ++++++++++++++++++++++++++++++++++++++
		// PREPARE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		if (keys != null && keys.size() > 0) {
			boolean qlink = false;
			for (JarawakParam lin : keys) {
				String mityp = getMetaInfoType(lin.name.trim());
				String mimode = getMetaInfoTypeMode(lin.name.trim());
				if (!checkFieldExcluded(lin.name.trim()) && lin.name != null
						&& lin.name.trim().toLowerCase().equals(lin.name.trim().toLowerCase())) {
					if (qlink)
						querywhere = querywhere + " AND ";
					querywhere = querywhere + " " + defineOper(lin, mityp, mimode);
					qlink = true;
				}
			}
			query = query + querywhere;
			querycount = querycount + querywhere;
		}

		if (ord != null && ord.length > 0) {
			queryend = queryend + " ORDER BY ";

			boolean ordq = false;

			for (JarawakParam lin : ord) {
				if (ordq)
					queryend = queryend + ", ";
				queryend = queryend + lin.name.trim().toLowerCase() + " " + lin.value;
				ordq = true;
			}
		}

		if (limit > 0)
			queryend = queryend + " LIMIT " + limit + " OFFSET " + (limit * page);

		// ++++++++++++++++++++++++++++++++++++++
		// EXECUTE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		if (bringtotal) {
			ArrayList<DBLin> altt = con.readDb(querycount);
			if (altt != null && altt.size() > 0) {
				totalrows = altt.get(0).getVal("rowstotal").toString();
			}
		}
		ArrayList<DBLin> al = con.readDb(query + queryend);

		if (al != null && al.size() > 0) {
			return al;
		} else {
			throw new JarawakException("E", "CONNECTOR", "connector.e.notfound");
		}
	}

	/*
	 * #########################################################################
	 * ##### GET
	 * #########################################################################
	 * #####
	 */

	@Override
	public DBLin get(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return get(keys, uid, false);
	}

	@Override
	public DBLin get(ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor) throws JarawakException {
		return get(keys, uid, errorinterceptor, false);
	}

	@Override
	public DBLin get(ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor, boolean useconfig)
			throws JarawakException {
		if (con == null)
			throw new JarawakException("E", "CONNECTOR", "connector.e.noconnection");

		if (keys == null || keys.size() <= 0)
			throw new JarawakException("E", "CONNECTOR", "connector.e.modinformkeys");

		String query = "SELECT * FROM " + getEndpoint() + " WHERE ";

		// ++++++++++++++++++++++++++++++++++++++
		// PREPARE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		boolean qlink = false;
		for (JarawakParam lin : keys) {
			String mityp = getMetaInfoType(lin.name);
			String mimode = getMetaInfoTypeMode(lin.name.trim());
			if (!checkFieldExcluded(lin.name)) {
				if (lin.value != null && lin.value.trim().length() > 0) {
					if (qlink)
						query = query + " AND ";
					query = query + " " + defineOper(lin, mityp, mimode);
					qlink = true;
				} else {
					throw new JarawakException("E", "CONNECTOR", "connector.e.modinvalidkey");
				}
			}
		}
		// ++++++++++++++++++++++++++++++++++++++
		// EXECUTE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		query = query + " LIMIT 1";
		ArrayList<DBLin> al = con.readDb(query);

		if (al != null && al.size() > 0) {
			return al.get(0);
		} else {
			throw new JarawakException("E", "CONNECTOR", "connector.e.notfound");
		}

	}

	/*
	 * #########################################################################
	 * ##### DELETE
	 * #########################################################################
	 * #####
	 */
	@Override
	public String delete(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return delete(keys, uid, false);
	}

	@Override
	public String delete(ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor) throws JarawakException {
		return delete(keys, uid, errorinterceptor, null, false);
	}

	@Override
	public String delete(ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor, String sessionuser,
			boolean useconfig) throws JarawakException {
		if (con == null)
			throw new JarawakException("E", "CONNECTOR", "connector.e.noconnection");
		if (keys == null || keys.size() <= 0)
			throw new JarawakException("E", "CONNECTOR", "connector.e.informkeys");

		String query = "DELETE FROM " + getEndpoint() + " WHERE ";

		// ++++++++++++++++++++++++++++++++++++++
		// PREPARE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		boolean qlink = false;
		for (JarawakParam lin : keys) {
			String mityp = getMetaInfoType(lin.name);
			String mimode = getMetaInfoTypeMode(lin.name.trim());
			if (!checkFieldExcluded(lin.name)) {
				if (lin.value != null && lin.value.trim().length() > 0) {
					if (qlink)
						query = query + " AND ";
					query = query + " " + defineOper(lin, mityp, mimode);
					qlink = true;
				} else {
					throw new JarawakException("E", "CONNECTOR", "connector.e.modinvalidkey");
				}
			}
		}
		// ++++++++++++++++++++++++++++++++++++++
		// EXECUTE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		boolean b = con.updateDB(query);

		// ++++++++++++++++++++++++++++++++++++++
		// UPDATE OBJECT
		// ++++++++++++++++++++++++++++++++++++++
		if (!b) {
			throw new JarawakException("E", "CONNECTOR", "connector.e.notdeleted");
		}
		return null;
	}

	/*
	 * #########################################################################
	 * ##### CREATE
	 * #########################################################################
	 * #####
	 */

	@Override
	public String create(String pk, JSONObject params) throws JarawakException {
		return create(pk, params, false);
	}

	@Override
	public String create(String pk, JSONObject params, boolean errorinterceptor) throws JarawakException {
		return create(pk, params, errorinterceptor, false);
	}

	@Override
	public String create(String pk, JSONObject params, boolean errorinterceptor, boolean useconfig)
			throws JarawakException {
		if (con == null)
			throw new JarawakException("E", "CONNECTOR", "connector.e.noconnection");
		if (params == null) {
			throw new JarawakException("E", "CONNECTOR", "connector.e.informparams");
		}

		String query = "INSERT INTO " + getEndpoint() + " ";
		String qfields = "";
		String qvalues = "";
		boolean qset = false;

		for (int i = 0; i < params.names().length(); i++) {
			String p = null;
			String v = null;
			try {
				p = params.names().getString(i);
				v = String.valueOf(params.get(p));
			} catch (Exception e) {

			}
			if (!checkFieldExcluded(p)) {
				if (getMetainfotype() != null && getMetainfotype().length > 0) {
					boolean metafound = false;
					for (JarawakParam lm : getMetainfotype()) {
						if (lm.name.trim().equals(p.trim())) {
							switch (lm.value) {
							case JarawakParam.TYPE_STRING:
								if (v != null) {
									if (qset) {
										qfields = qfields + ", ";
										qvalues = qvalues + ", ";
									}
									qfields = qfields + p.trim().toLowerCase();
									qvalues = qvalues + "'" + v + "'";
									qset = true;
								} else {
									if (qset) {
										qfields = qfields + ", ";
										qvalues = qvalues + ", ";
									}
									qfields = qfields + p.trim().toLowerCase();
									qvalues = qvalues + "NULL";
									qset = true;
								}
								break;
							case JarawakParam.TYPE_ENCODED_STRING:
								if (v != null)
									try {
										if (qset) {
											qfields = qfields + ", ";
											qvalues = qvalues + ", ";
										}
										qfields = qfields + p.trim().toLowerCase();
										qvalues = qvalues + "'" + URLEncoder.encode(v, conf.encoding) + "'";
										qset = true;
									} catch (Exception e) {

									}
								else {
									if (qset) {
										qfields = qfields + ", ";
										qvalues = qvalues + ", ";
									}
									qfields = qfields + p.trim().toLowerCase();
									qvalues = qvalues + "NULL";
									qset = true;
								}
								break;
							case JarawakParam.TYPE_EXCLUDED:
								break;
							default:
								if (qset) {
									qfields = qfields + ", ";
									qvalues = qvalues + ", ";
								}
								qfields = qfields + p.trim().toLowerCase();
								qvalues = qvalues + v;
								qset = true;
								break;

							}
							metafound = true;
						}
					}
					if (!metafound) {
						if (qset) {
							qfields = qfields + ", ";
							qvalues = qvalues + ", ";
						}
						qfields = qfields + p.trim().toLowerCase();
						qvalues = qvalues + v;
						qset = true;
					}
				} else {
					if (qset) {
						qfields = qfields + ", ";
						qvalues = qvalues + ", ";
					}
					qfields = qfields + p.trim().toLowerCase();
					qvalues = qvalues + v;
					qset = true;
				}

			}
		}
		query = query + "(" + qfields + ") VALUES (" + qvalues + ")";

		// ++++++++++++++++++++++++++++++++++++++
		// EXECUTE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		if (pk != null)
			query = query + "; SELECT last_insert_rowid() AS " + pk;
		boolean res = con.updateDB(query);

		// ++++++++++++++++++++++++++++++++++++++
		// UPDATE OBJECT
		// ++++++++++++++++++++++++++++++++++++++
		if (res) {
			if (pk != null)
				if (con.lastval != null)
					return con.lastval;
				else
					throw new JarawakException("E", "CONNECTOR", "connector.e.modnotcreated");
			else
				return null;

		} else
			throw new JarawakException("E", "CONNECTOR", "connector.e.modnotcreated");
	}

	/*
	 * #########################################################################
	 * ##### SAVE
	 * #########################################################################
	 * #####
	 */

	@Override
	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return save(params, keys, uid, false);
	}

	@Override
	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor)
			throws JarawakException {
		return save(params, keys, uid, errorinterceptor, false, null, false);
	}

	@Override
	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor,
			boolean encode, String sessionuser, boolean useconfig) throws JarawakException {
		if (con == null)
			throw new JarawakException("E", "CONNECTOR", "connector.e.noconnection");
		if (keys == null || keys.size() <= 0)
			throw new JarawakException("E", "CONNECTOR", "connector.e.informkeys");

		String query = "UPDATE " + getEndpoint() + " SET ";
		String condition = " WHERE ";
		boolean qset = false;

		for (int i = 0; i < params.names().length(); i++) {
			String p = null;
			String v = null;
			try {
				p = params.names().getString(i);
				v = params.getString(p);
			} catch (Exception e) {

			}
			boolean iskey = false;
			for (JarawakParam k : keys)
				if (k.name.trim().toUpperCase().equals(p.trim().toUpperCase()))
					iskey = true;

			if (!iskey && !checkFieldExcluded(p)) {
				if (qset)
					query = query + ", ";
				qset = true;

				if (getMetainfotype() != null && getMetainfotype().length > 0) {
					boolean metafound = false;
					for (JarawakParam lm : getMetainfotype()) {
						if (lm.name.trim().toLowerCase().equals(p.trim().toLowerCase())) {
							switch (lm.value) {
							case JarawakParam.TYPE_STRING:
								if (v != null)
									query = query + p.trim().toLowerCase() + " = " + "'" + v + "'";
								else
									query = query + p.trim().toLowerCase() + " = " + "NULL";
								break;
							case JarawakParam.TYPE_ENCODED_STRING:
								if (v != null)
									try {
										query = query + p.trim().toLowerCase() + " = " + "'"
												+ URLEncoder.encode(v, conf.encoding) + "'";
									} catch (Exception e) {

									}
								else
									query = query + p.trim().toLowerCase() + " = " + "NULL";
								break;
							default:
								query = query + p.trim().toLowerCase() + " = " + v;
								break;

							}
							metafound = true;
						}
					}
					if (!metafound)
						query = query + p.trim().toLowerCase() + " = " + v;
				} else {
					query = query + p.trim().toLowerCase() + " = " + v;
				}
			}
		}

		boolean qlink = false;
		for (JarawakParam lin : keys) {
			if (qlink)
				condition = condition + " AND ";
			String mityp = getMetaInfoType(lin.name);
			String mimode = getMetaInfoTypeMode(lin.name.trim());
			condition = condition + " " + defineOper(lin, mityp, mimode);
			qlink = true;
		}

		// ++++++++++++++++++++++++++++++++++++++
		// EXECUTE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		boolean res = con.updateDB(query + condition);

		// ++++++++++++++++++++++++++++++++++++++
		// UPDATE OBJECT
		// ++++++++++++++++++++++++++++++++++++++
		if (!res) {
			throw new JarawakException("E", "CONNECTOR", "connector.e.modnotupdated");
		}

		return "S";
	}

	/*
	 * #########################################################################
	 * ##### GET OPERATOR CLAUSE
	 * #########################################################################
	 * #####
	 */
	public String defineOper(JarawakParam p, String metatype) throws JarawakException {
		return defineOper(p, metatype, null);
	}

	public String defineOper(JarawakParam p, String metatype, String metamode) throws JarawakException {
		String oper = null;
		String tps = "";
		String val = null;

		if (p == null)
			throw new JarawakException("E", "CONNECTOR", "connector.e.informkeys");

		val = p.value;
		if (val == null)
			val = "NULL";

		String field = p.name.trim().toLowerCase();

		if (metatype != null) {
			if (metatype.trim().equals(JarawakParam.TYPE_STRING)
					|| metatype.trim().equals(JarawakParam.TYPE_ENCODED_STRING)) {
				tps = "'";
			}
		}

		if (metamode != null) {
			if (metamode.trim().equals(JarawakParam.DB_DATETIME))
				field = field + "::timestamp";
		}

		if (p.mode != null && p.mode.trim().length() > 0) {
			switch (p.mode.trim()) {
			case JarawakParam.MODE_WSQUERY_EQUAL_U:
				oper = " UPPER(" + field + ")" + " <> " + tps + encodetrans(val.toUpperCase(), metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_NOTEQUAL:
				oper = field + " <> " + tps + encodetrans(val, metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_NOTEQUAL_U:
				oper = " UPPER(" + field + ")" + " <> " + tps + encodetrans(val.toUpperCase(), metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_GREATERTHAN:
				oper = field + " > " + tps + encodetrans(val, metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_GREATEROREQUAL:
				oper = field + " >= " + tps + encodetrans(val, metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_LOWERTHAN:
				oper = field + " < " + tps + encodetrans(val, metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_LOWEROREQUAL:
				oper = field + " <= " + tps + encodetrans(val, metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_ISNULL:
				oper = field + " IS NULL ";
				break;
			case JarawakParam.MODE_WSQUERY_NOTNULL:
				oper = field + " NOT IS NULL ";
				break;
			case JarawakParam.MODE_WSQUERY_IN:
				oper = field + " IN (" + encodetrans(val, metatype) + " ) ";
				break;
			case JarawakParam.MODE_WSQUERY_IN_U:
				oper = " UPPER(" + field + ") IN (" + encodetrans(val.toUpperCase(), metatype) + " ) ";
				break;
			case JarawakParam.MODE_WSQUERY_NOTIN:
				oper = field + " NOT IN (" + encodetrans(val, metatype) + " ) ";
				break;
			case JarawakParam.MODE_WSQUERY_NOTIN_U:
				oper = " UPPER(" + field + ") NOT IN (" + encodetrans(val.toUpperCase(), metatype) + " ) ";
				break;
			case JarawakParam.MODE_WSQUERY_STARTINGWITH:
				oper = field + " LIKE '" + encodetrans(val, metatype) + "%' ";
				break;
			case JarawakParam.MODE_WSQUERY_ENDINGWITH:
				oper = field + " LIKE '%" + encodetrans(val, metatype) + "' ";
				break;
			case JarawakParam.MODE_WSQUERY_HAVING:
				oper = field + " LIKE '%" + encodetrans(val, metatype) + "%' ";
				break;
			case JarawakParam.MODE_WSQUERY_STARTINGWITH_U:
				oper = " UPPER(" + field + ") LIKE '" + encodetrans(val.toUpperCase(), metatype) + "%' ";
				break;
			case JarawakParam.MODE_WSQUERY_ENDINGWITH_U:
				oper = " UPPER(" + field + ") LIKE '%" + encodetrans(val.toUpperCase(), metatype) + "' ";
				break;
			case JarawakParam.MODE_WSQUERY_HAVING_U:
				oper = " UPPER(" + field + ") LIKE '%" + encodetrans(val.toUpperCase(), metatype) + "%' ";
				break;
			default:
				oper = field + " = " + tps + encodetrans(val, metatype) + tps;
				break;
			}
		} else {
			if (p.value != null)
				oper = field + " = " + tps + encodetrans(val, metatype) + tps;
			else
				oper = field + " IS NULL ";
		}
		return oper;
	}

	private String encodetrans(String val, String metatype) {
		if (metatype == null)
			return val;
		if (val == null)
			return null;

		if (metatype.trim().equals(JarawakParam.TYPE_ENCODED_STRING)) {
			try {
				val = URLEncoder.encode(val, conf.encoding);
			} catch (Exception e) {

			}
		}
		return val;
	}

}
