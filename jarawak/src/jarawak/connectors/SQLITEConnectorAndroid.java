package jarawak.connectors;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONObject;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakParam;
import jarawak.db.DBLin;
import jarawak.db.DBParVal;

public class SQLITEConnectorAndroid extends JarawakConnector {

	private static final long serialVersionUID = 3572483364077958132L;
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	private SQLiteOpenHelper dbHelper = null;
	private String encoding = "UTF-8";

	public SQLITEConnectorAndroid(SQLiteOpenHelper dbHelper, String encoding, String endpoint,
			JarawakParam[] metainfotype) {
		super(null, endpoint, metainfotype);
		this.dbHelper = dbHelper;
		if (encoding != null)
			this.encoding = encoding;
	}

	/*
	 * #########################################################################
	 * ##### LIST
	 * #########################################################################
	 * #####
	 */

	@Override
	public ArrayList<DBLin> list(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal) throws JarawakException {
		return list(keys, ord, page, limit, bringtotal, false);
	}

	@Override
	public ArrayList<DBLin> list(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal, boolean errorinterceptor) throws JarawakException {
		return list(keys, ord, page, limit, bringtotal, errorinterceptor, false);
	}

	@Override
	public ArrayList<DBLin> list(ArrayList<JarawakParam> keys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal, boolean errorinterceptor, boolean useconfig) throws JarawakException {

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		ArrayList<DBLin> output = null;
		String query = "SELECT * FROM " + getEndpoint() + " ";
		String querywhere = " WHERE ";
		String querycount = "SELECT COUNT(*) AS rowstotal FROM " + getEndpoint() + " ";

		// ++++++++++++++++++++++++++++++++++++++
		// PREPARE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		if (keys != null && keys.size() > 0) {
			boolean qlink = false;
			for (JarawakParam lin : keys) {
				String mityp = getMetaInfoType(lin.name.trim());
				if (!checkFieldExcluded(lin.name.trim()) && lin.name != null
						&& lin.name.trim().toUpperCase().equals(lin.name.trim().toUpperCase())) {
					if (qlink)
						querywhere = querywhere + " AND ";
					querywhere = querywhere + " " + defineOper(lin, mityp);
					qlink = true;
				}
			}
			query = query + querywhere;
			querycount = querycount + querywhere;
		}

		if (ord != null && ord.length > 0) {
			query = query + " ORDER BY ";

			boolean ordq = false;

			for (JarawakParam lin : ord) {
				if (ordq)
					query = query + ", ";
				query = query + lin.name + " " + lin.value;
				ordq = true;
			}
		}

		if (limit > 0)
			query = query + " LIMIT " + limit + " OFFSET " + (limit * page);

		// ++++++++++++++++++++++++++++++++++++++
		// EXECUTE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		if (bringtotal) {
			try {
				Cursor cur = db.rawQuery(querycount, null);
				if (cur != null && cur.getCount() > 0) {
					cur.moveToFirst();
					totalrows = cur.getString(cur.getColumnIndex("rowstotal"));
				}
			} catch (Exception e) {

			}
		}

		boolean ok = false;
		int cntlin = 0;
		try {
			Cursor cur = db.rawQuery(query, null);
			if (cur != null && cur.getCount() > 0) {
				cur.moveToFirst();
				output = new ArrayList<DBLin>();
				do {
					DBLin lin = new DBLin();
					lin.lin = cntlin;
					DBParVal[] cols = new DBParVal[cur.getColumnCount()];
					for (int j = 0; j < cur.getColumnCount(); j++) {
						DBParVal par = new DBParVal();
						par.param = cur.getColumnName(j);
						switch (cur.getType(j)) {
						case Cursor.FIELD_TYPE_STRING:
							par.value = cur.getString(j);
							break;
						case Cursor.FIELD_TYPE_INTEGER:
							par.value = String.valueOf(cur.getInt(j));
							break;
						case Cursor.FIELD_TYPE_FLOAT:
							par.value = String.valueOf(cur.getFloat(j));
							break;
						default:
							try {
								par.value = cur.getString(j);
							} catch (Exception e) {
								par.value = null;
							}
						}
						cols[j] = par;
					}
					lin.cols = cols;
					output.add(lin);
					cntlin++;
				} while (cur.moveToNext());
				ok = true;
			}
		} catch (Exception e) {

		} finally {

		}

		if (!ok)
			throw new JarawakException("E", "CONNECTOR", "connector.e.notfound");
		return output;
	}

	/*
	 * #########################################################################
	 * ##### GET
	 * #########################################################################
	 * #####
	 */

	@Override
	public DBLin get(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return get(keys, uid, false);

	}

	@Override
	public DBLin get(ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor) throws JarawakException {
		if (keys == null || keys.size() <= 0)
			throw new JarawakException("E", "CONNECTOR", "connector.e.modinformkeys");
		return get(keys, uid, errorinterceptor, false);
	}

	@Override
	public DBLin get(ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor, boolean useconfig)
			throws JarawakException {
		if (keys == null || keys.size() <= 0)
			throw new JarawakException("E", "CONNECTOR", "connector.e.modinformkeys");

		SQLiteDatabase db = dbHelper.getReadableDatabase();

		String query = "SELECT * FROM " + getEndpoint() + " WHERE ";

		// ++++++++++++++++++++++++++++++++++++++
		// PREPARE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		boolean qlink = false;
		for (JarawakParam lin : keys) {
			String mityp = getMetaInfoType(lin.name);
			if (!checkFieldExcluded(lin.name)) {
				if (lin.value != null && lin.value.trim().length() > 0) {
					if (qlink)
						query = query + " AND ";
					query = query + " " + defineOper(lin, mityp);
					qlink = true;
				} else {
					throw new JarawakException("E", "CONNECTOR", "connector.e.modinvalidkey");
				}
			}
		}
		// ++++++++++++++++++++++++++++++++++++++
		// EXECUTE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		query = query + " LIMIT 1";
		boolean ok = false;
		DBLin output = null;
		try {
			Cursor cur = db.rawQuery(query, null);
			if (cur != null && cur.getCount() > 0) {
				cur.moveToFirst();
				output = new DBLin();
				output.cols = new DBParVal[cur.getColumnCount()];
				for (int j = 0; j < cur.getColumnCount(); j++) {
					DBParVal par = new DBParVal();
					par.param = cur.getColumnName(j);
					switch (cur.getType(j)) {
					case Cursor.FIELD_TYPE_STRING:
						par.value = cur.getString(j);
						break;
					case Cursor.FIELD_TYPE_INTEGER:
						par.value = String.valueOf(cur.getInt(j));
						break;
					case Cursor.FIELD_TYPE_FLOAT:
						par.value = String.valueOf(cur.getFloat(j));
						break;
					default:
						par.value = null;
					}
					output.cols[j] = par;
				}
				ok = true;
			} else {
				throw new JarawakException("E", "CONNECTOR", "connector.e.notfound");
			}

		} catch (Exception e) {

		} finally {

		}

		if (!ok)
			throw new JarawakException("E", "CONNECTOR", "connector.e.notfound");

		return output;
	}

	/*
	 * #########################################################################
	 * ##### DELETE
	 * #########################################################################
	 * #####
	 */

	@Override
	public String delete(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return delete(keys, uid, false);
	}

	@Override
	public String delete(ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor) throws JarawakException {
		return delete(keys, uid, errorinterceptor, null);
	}

	@Override
	public String delete(ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor, String sessionuser)
			throws JarawakException {
		return delete(keys, uid, errorinterceptor, sessionuser, false);
	}

	@Override
	public String delete(ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor, String sessionuser,
			boolean useconfig) throws JarawakException {

		if (keys == null || keys.size() <= 0)
			throw new JarawakException("E", "CONNECTOR", "connector.e.informkeys");

		SQLiteDatabase db = dbHelper.getWritableDatabase();
		String query = "DELETE FROM " + getEndpoint() + " WHERE ";

		// ++++++++++++++++++++++++++++++++++++++
		// PREPARE QUERY
		// ++++++++++++++++++++++++++++++++++++++

		boolean qlink = false;
		for (JarawakParam lin : keys) {
			if (checkFieldExcluded(lin.name))
				continue;
			String mityp = getMetaInfoType(lin.name);
			if (mityp != null && lin.value != null && lin.value.trim().length() > 0) {
				if (qlink)
					query = query + " AND ";
				query = query + " " + defineOper(lin, mityp);
				qlink = true;
			}
		}

		// ++++++++++++++++++++++++++++++++++++++
		// EXECUTE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		boolean ok = false;
		try {
			db.execSQL(query);
			ok = true;
		} catch (Exception e) {

		} finally {

		}

		if (!ok)
			throw new JarawakException("E", "CONNECTOR", "connector.e.notdeleted");

		return null;
	}

	/*
	 * #########################################################################
	 * ##### CREATE
	 * #########################################################################
	 * #####
	 */

	@Override
	public String create(String pk, JSONObject params) throws JarawakException {
		return create(pk, params, false);
	}

	@Override
	public String create(String pk, JSONObject params, boolean errorinterceptor) throws JarawakException {
		return create(pk, params, errorinterceptor, false);
	}

	@Override
	public String create(String pk, JSONObject params, boolean errorinterceptor, boolean encode)
			throws JarawakException {
		return create(pk, params, errorinterceptor, encode, false);
	}

	@Override
	public String create(String pk, JSONObject params, boolean errorinterceptor, boolean encode, boolean useconfig)
			throws JarawakException {

		if (params == null) {
			throw new JarawakException("E", "CONNECTOR", "connector.e.informparams");
		}

		SQLiteDatabase db = dbHelper.getWritableDatabase();
		String query = "INSERT INTO " + getEndpoint() + " ";
		String qfields = "";
		String qvalues = "";
		boolean qset = false;

		for (int i = 0; i < params.names().length(); i++) {
			String p = null;
			String v = null;
			try {
				p = params.names().getString(i);
				v = params.getString(p);
			} catch (Exception e) {

			}
			if (!checkFieldExcluded(p)) {
				if (getMetainfotype() != null && getMetainfotype().length > 0) {
					boolean metafound = false;
					for (JarawakParam lm : getMetainfotype()) {
						if (lm.name.trim().equals(p.trim())) {
							switch (lm.value) {
							case JarawakParam.TYPE_STRING:
								if (v != null) {
									if (qset) {
										qfields = qfields + ", ";
										qvalues = qvalues + ", ";
									}
									qfields = qfields + p;
									qvalues = qvalues + "'" + v + "'";
									qset = true;
								} else {
									if (qset) {
										qfields = qfields + ", ";
										qvalues = qvalues + ", ";
									}
									qfields = qfields + p;
									qvalues = qvalues + "NULL";
									qset = true;
								}
								break;
							case JarawakParam.TYPE_ENCODED_STRING:
								if (v != null)
									try {
										if (qset) {
											qfields = qfields + ", ";
											qvalues = qvalues + ", ";
										}
										qfields = qfields + p;
										qvalues = qvalues + "'" + URLEncoder.encode(v, encoding) + "'";
										qset = true;
									} catch (Exception e) {

									}
								else {
									if (qset) {
										qfields = qfields + ", ";
										qvalues = qvalues + ", ";
									}
									qfields = qfields + p;
									qvalues = qvalues + "NULL";
									qset = true;
								}
								break;
							case JarawakParam.TYPE_EXCLUDED:
								break;
							default:
								if (qset) {
									qfields = qfields + ", ";
									qvalues = qvalues + ", ";
								}
								qfields = qfields + p;
								qvalues = qvalues + v;
								qset = true;
								break;

							}
							metafound = true;
						}
					}
					if (!metafound) {
						if (qset) {
							qfields = qfields + ", ";
							qvalues = qvalues + ", ";
						}
						qfields = qfields + p;
						qvalues = qvalues + v;
						qset = true;
					}
				} else {
					if (qset) {
						qfields = qfields + ", ";
						qvalues = qvalues + ", ";
					}
					qfields = qfields + p;
					qvalues = qvalues + v;
					qset = true;
				}

			}
		}
		query = query + "(" + qfields + ") VALUES (" + qvalues + ");";

		// ++++++++++++++++++++++++++++++++++++++
		// EXECUTE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		boolean ok = false;
		String lastid = null;

		try {
			db.execSQL(query);
			if (pk != null) {
				query = " SELECT " + pk + " as lastid FROM " + getEndpoint() + " order by " + pk + " DESC LIMIT 1; ";
				try {
					Cursor cur = db.rawQuery(query, null);
					if (cur != null && cur.moveToFirst()) {
						if (pk != null) {
							lastid = cur.getString(cur.getColumnIndex("lastid"));
						}
						ok = true;
					}
				} catch (Exception e) {

				}
			}
			ok = true;
		} catch (Exception e) {

		} finally {

		}
		if (!ok)
			throw new JarawakException("E", "CONNECTOR", "connector.e.modnotcreated");

		return lastid;
	}

	/*
	 * #########################################################################
	 * ##### SAVE
	 * #########################################################################
	 * #####
	 */
	@Override
	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return save(params, keys, uid, false);
	}

	@Override
	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor)
			throws JarawakException {
		return save(params, keys, uid, errorinterceptor, false);
	}

	@Override
	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor,
			boolean encode) throws JarawakException {
		return save(params, keys, uid, errorinterceptor, encode, null);

	}

	@Override
	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor,
			boolean encode, String sessionuser) throws JarawakException {
		return save(params, keys, uid, errorinterceptor, encode, sessionuser, false);
	}

	@Override
	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid, boolean errorinterceptor,
			boolean encode, String sessionuser, boolean useconfig) throws JarawakException {

		if (keys == null || keys.size() <= 0)
			throw new JarawakException("E", "CONNECTOR", "connector.e.informkeys");

		SQLiteDatabase db = dbHelper.getWritableDatabase();

		String query = "UPDATE " + getEndpoint() + " SET ";
		String condition = " WHERE ";
		boolean qset = false;

		for (int i = 0; i < params.names().length(); i++) {
			String p = null;
			String v = null;
			try {
				p = params.names().getString(i);
				v = params.getString(p);
			} catch (Exception e) {

			}
			boolean iskey = false;
			for (JarawakParam k : keys)
				if (k.name.trim().toUpperCase().equals(p.trim().toUpperCase()))
					iskey = true;

			if (!iskey && !checkFieldExcluded(p)) {
				if (qset)
					query = query + ", ";
				qset = true;

				if (getMetainfotype() != null && getMetainfotype().length > 0) {
					boolean metafound = false;
					for (JarawakParam lm : getMetainfotype()) {
						if (lm.name.trim().equals(p.trim())) {
							switch (lm.value) {
							case JarawakParam.TYPE_STRING:
								if (v != null)
									query = query + p + " = " + "'" + v + "'";
								else
									query = query + p + " = " + "NULL";
								break;
							case JarawakParam.TYPE_ENCODED_STRING:
								if (v != null)
									try {
										query = query + p + " = " + "'" + URLEncoder.encode(v, encoding) + "'";
									} catch (Exception e) {

									}
								else
									query = query + p + " = " + "NULL";
								break;
							default:
								query = query + p + " = " + v;
								break;

							}
							metafound = true;
						}
					}
					if (!metafound)
						query = query + p + " = " + v;
				} else {
					query = query + p + " = " + v;
				}
			}
		}

		boolean qlink = false;
		for (JarawakParam lin : keys) {
			String mityp = getMetaInfoType(lin.name);
			if (qlink)
				condition = condition + " AND ";
			condition = condition + " " + defineOper(lin, mityp);
			qlink = true;
		}

		// ++++++++++++++++++++++++++++++++++++++
		// EXECUTE QUERY
		// ++++++++++++++++++++++++++++++++++++++
		boolean ok = false;
		String ret = null;
		try {
			db.beginTransaction();
			db.execSQL(query + condition);
			db.setTransactionSuccessful();
			ret = "S";
			ok = true;
		} catch (Exception e) {

		} finally {
			if (db != null) {
				if (db.inTransaction())
					db.endTransaction();

			}
		}
		if (!ok)
			throw new JarawakException("E", "CONNECTOR", "connector.e.modnotupdated");

		return ret;
	}

	/*
	 * #########################################################################
	 * ##### GET OPERATOR CLAUSE
	 * #########################################################################
	 * #####
	 */
	public String defineOper(JarawakParam p, String metatype) throws JarawakException {
		return defineOper(p, metatype, null);
	}

	public String defineOper(JarawakParam p, String metatype, String metamode) throws JarawakException {
		String oper = null;
		String tps = "";
		String val = null;

		if (p == null)
			throw new JarawakException("E", "CONNECTOR", "connector.e.informkeys");

		val = p.value;
		if (val == null)
			val = "NULL";

		String field = p.name.trim().toLowerCase();

		if (metatype != null) {
			if (metatype.trim().equals(JarawakParam.TYPE_STRING)
					|| metatype.trim().equals(JarawakParam.TYPE_ENCODED_STRING)) {
				tps = "'";
			}
		}

		if (p.mode != null && p.mode.trim().length() > 0) {
			switch (p.mode.trim()) {
			case JarawakParam.MODE_WSQUERY_EQUAL_U:
				oper = " UPPER(" + field + ")" + " <> " + tps + encodetrans(val.toUpperCase(), metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_NOTEQUAL:
				oper = field + " <> " + tps + encodetrans(val, metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_NOTEQUAL_U:
				oper = " UPPER(" + field + ")" + " <> " + tps + encodetrans(val.toUpperCase(), metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_GREATERTHAN:
				oper = field + " > " + tps + encodetrans(val, metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_GREATEROREQUAL:
				oper = field + " >= " + tps + encodetrans(val, metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_LOWERTHAN:
				oper = field + " < " + tps + encodetrans(val, metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_LOWEROREQUAL:
				oper = field + " <= " + tps + encodetrans(val, metatype) + tps;
				break;
			case JarawakParam.MODE_WSQUERY_ISNULL:
				oper = field + " IS NULL ";
				break;
			case JarawakParam.MODE_WSQUERY_NOTNULL:
				oper = field + " NOT IS NULL ";
				break;
			case JarawakParam.MODE_WSQUERY_IN:
				oper = field + " IN (" + encodetrans(val, metatype) + " ) ";
				break;
			case JarawakParam.MODE_WSQUERY_IN_U:
				oper = " UPPER(" + field + ") IN (" + encodetrans(val.toUpperCase(), metatype) + " ) ";
				break;
			case JarawakParam.MODE_WSQUERY_NOTIN:
				oper = field + " NOT IN (" + encodetrans(val, metatype) + " ) ";
				break;
			case JarawakParam.MODE_WSQUERY_NOTIN_U:
				oper = " UPPER(" + field + ") NOT IN (" + encodetrans(val.toUpperCase(), metatype) + " ) ";
				break;
			case JarawakParam.MODE_WSQUERY_STARTINGWITH:
				oper = field + " LIKE '" + encodetrans(val, metatype) + "%' ";
				break;
			case JarawakParam.MODE_WSQUERY_ENDINGWITH:
				oper = field + " LIKE '%" + encodetrans(val, metatype) + "' ";
				break;
			case JarawakParam.MODE_WSQUERY_HAVING:
				oper = field + " LIKE '%" + encodetrans(val, metatype) + "%' ";
				break;
			case JarawakParam.MODE_WSQUERY_STARTINGWITH_U:
				oper = " UPPER(" + field + ") LIKE '" + encodetrans(val.toUpperCase(), metatype) + "%' ";
				break;
			case JarawakParam.MODE_WSQUERY_ENDINGWITH_U:
				oper = " UPPER(" + field + ") LIKE '%" + encodetrans(val.toUpperCase(), metatype) + "' ";
				break;
			case JarawakParam.MODE_WSQUERY_HAVING_U:
				oper = " UPPER(" + field + ") LIKE '%" + encodetrans(val.toUpperCase(), metatype) + "%' ";
				break;
			default:
				oper = field + " = " + tps + encodetrans(val, metatype) + tps;
				break;
			}
		} else {
			if (p.value != null)
				oper = field + " = " + tps + encodetrans(val, metatype) + tps;
			else
				oper = field + " IS NULL ";
		}
		return oper;
	}

	private String encodetrans(String val, String metatype) {
		if (metatype == null)
			return val;
		if (val == null)
			return null;

		if (metatype.trim().equals(JarawakParam.TYPE_ENCODED_STRING)) {
			try {
				val = URLEncoder.encode(val, encoding);
			} catch (Exception e) {

			}
		}
		return val;
	}
}
