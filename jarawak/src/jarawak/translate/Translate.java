
package jarawak.translate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.time.ZonedDateTime;
import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;
import jarawak.config.DBConfig;
import jarawak.models.modelTranslateLine;
import jarawak.tools.Converters;

public class Translate {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	private String encoding = "UTF-8";

	private ArrayList<String[]> msgs = new ArrayList<String[]>();

	/*
	 * #########################################################################
	 * ##### CONSTRUCTOR
	 * #########################################################################
	 * #####
	 */
	public Translate(String filepath, String encoding) {
		this.encoding = encoding;
		loadfileT(filepath);
	}

	public Translate(String filepath) {
		loadfileT(filepath);
	}

	private void loadfileT(String filepath) {
		String[] mtz = null;
		try {
			File file = new File(filepath);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
			String line;
			while ((line = br.readLine()) != null) {
				if (line != null && line.indexOf('=') >= 0) {
					mtz = line.split("=");
					if (mtz != null && mtz.length > 1) {
						String[] l = new String[2];
						l[0] = mtz[0];
						l[1] = mtz[1];
						msgs.add(l);
					}
				}
			}
		} catch (Exception ex) {
			msgs = null;
		}
	}

	/*
	 * #########################################################################
	 * ##### GET TRANSLATION
	 * #########################################################################
	 * #####
	 */
	public String getTrans(String msgid) {
		if (msgs != null && msgs.size() > 0) {
			for (String[] s : msgs) {
				if (s[0] != null && s[0].trim().toUpperCase().equals(msgid.trim().toUpperCase())) {
					return s[1];
				}
			}
		}
		return null;
	}

	/*
	 * #########################################################################
	 * ##### GET MODEL TRANSLATIONS
	 * #########################################################################
	 * #####
	 */
	public static modelTranslateLine[] getModelTranslations(Object model, String translatetable, String lang,
			String tablename, String key1) throws JarawakException {
		return getModelTranslations(model, translatetable, lang, tablename, key1, "*");
	}

	public static modelTranslateLine[] getModelTranslations(Object model, String translatetable, String lang,
			String tablename, String key1, String key2) throws JarawakException {
		return getModelTranslations(model, translatetable, lang, tablename, key1, key2, "*");
	}

	public static modelTranslateLine[] getModelTranslations(Object model, String translatetable, String lang,
			String tablename, String key1, String key2, String key3) throws JarawakException {
		return getModelTranslations(model, translatetable, lang, tablename, key1, key2, key3, "*");
	}

	public static modelTranslateLine[] getModelTranslations(Object model, String translatetable, String lang,
			String tablename, String key1, String key2, String key3, String key4) throws JarawakException {
		return getModelTranslations(model, translatetable, lang, tablename, key1, key2, key3, key4, "*");
	}

	public static modelTranslateLine[] getModelTranslations(Object model, String translatetable, String lang,
			String tablename, String key1, String key2, String key3, String key4, String key5) throws JarawakException {

		if (model == null)
			throw new JarawakException("E", "Translation", "Inform target model");
		if (!model.getClass().getSuperclass().equals(JarawakModel.class))
			throw new JarawakException("E", "Translation", "Inform valid target model (Jarawak Model)");
		if (translatetable == null || translatetable.trim().length() <= 0)
			throw new JarawakException("E", "Translation", "Inform translate table name");
		if (tablename == null || tablename.trim().length() <= 0)
			throw new JarawakException("E", "Translation", "Inform valid target table");
		if (key1 == null || key1.trim().length() <= 0)
			throw new JarawakException("E", "Translation", "Inform valid key");

		// CAST MODEL
		JarawakModel jmodel = null;
		try {
			jmodel = (JarawakModel) model;
		} catch (Exception e) {
			throw new JarawakException("E", "Translation", "Invalid model object");
		}

		if (jmodel.getConnector() == null)
			throw new JarawakException("E", "Translation", "Invalid model connector");

		if (jmodel.getMetainfodb() == null || jmodel.getMetainfodb().length <= 0)
			throw new JarawakException("E", "Translation", "Invalid model meta info data");

		JarawakConnector con = (JarawakConnector) jmodel.getConnector();
		DBConfig conf = (DBConfig) con.getConf();
		String encoding = null;
		if (conf != null)
			encoding = conf.encoding;
		String origin_ep = con.getEndpoint();
		con.setEndpoint(translatetable);

		ArrayList<modelTranslateLine> result = new ArrayList<modelTranslateLine>();

		try {
			// Loop Through Meta Model Properties
			for (JarawakParam meta : jmodel.getMetainfodb()) {

				if (meta.value == null || meta.value.trim().equals(JarawakParam.TYPE_ARRAY_OBJECT)
						|| meta.value.trim().equals(JarawakParam.TYPE_OBJECT)
						|| meta.value.trim().equals(JarawakParam.TYPE_EXCLUDED))
					continue;

				modelTranslateLine transtab = new modelTranslateLine(con, encoding);

				ArrayList<JarawakParam> keys = new ArrayList<JarawakParam>();
				keys.add(new JarawakParam("lang", lang));
				keys.add(new JarawakParam("prop", meta.name));
				keys.add(new JarawakParam("target", tablename));
				keys.add(new JarawakParam("targetkey1", key1));
				if (key2 != null)
					keys.add(new JarawakParam("targetkey2", key2));
				else
					keys.add(new JarawakParam("targetkey2", "*"));
				if (key3 != null)
					keys.add(new JarawakParam("targetkey3", key3));
				else
					keys.add(new JarawakParam("targetkey3", "*"));
				if (key4 != null)
					keys.add(new JarawakParam("targetkey4", key4));
				else
					keys.add(new JarawakParam("targetkey4", "*"));
				if (key5 != null)
					keys.add(new JarawakParam("targetkey5", key5));
				else
					keys.add(new JarawakParam("targetkey5", "*"));

				try {
					transtab.getMe(keys, null, false, true);
					result.add(transtab);
				} catch (JarawakException | Exception e) {

				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			con.setEndpoint(origin_ep);
		}
		if (result.size() > 0) {
			return (modelTranslateLine[]) result.toArray(new modelTranslateLine[result.size()]);
		}

		return null;

	}

	/*
	 * #########################################################################
	 * ##### GET TRANSLATION TEXT
	 * #########################################################################
	 * #####
	 */

	public static String getMTText(modelTranslateLine[] translations, String par, String lang) {
		if (par == null || par.trim().length() <= 0)
			return null;

		if (lang == null || lang.trim().length() <= 0)
			return null;

		if (translations != null && translations.length > 0)
			for (modelTranslateLine tl : translations) {
				if (par.equals(tl.prop) && lang.equals(tl.lang))
					return tl.value;
			}

		return null;
	}

	/*
	 * #########################################################################
	 * ##### SET TRANSLATION TEXT
	 * #########################################################################
	 * #####
	 */

	public static boolean setMTText(modelTranslateLine transline, String userid) {

		if (transline == null)
			return false;
		if (transline.lang == null || transline.lang.trim().length() <= 0)
			return false;
		if (transline.prop == null || transline.prop.trim().length() <= 0)
			return false;
		if (transline.target == null || transline.target.trim().length() <= 0)
			return false;
		if (transline.targetkey1 == null || transline.targetkey1.trim().length() <= 0)
			return false;
		if (transline.targetkey2 == null || transline.targetkey2.trim().length() <= 0)
			transline.targetkey2 = "*";
		if (transline.targetkey3 == null || transline.targetkey3.trim().length() <= 0)
			transline.targetkey3 = "*";
		if (transline.targetkey4 == null || transline.targetkey4.trim().length() <= 0)
			transline.targetkey4 = "*";
		if (transline.targetkey5 == null || transline.targetkey5.trim().length() <= 0)
			transline.targetkey5 = "*";
		if (userid == null || userid.trim().length() <= 0)
			return false;

		ArrayList<JarawakParam> keys = new ArrayList<JarawakParam>();
		keys.add(new JarawakParam("lang", transline.lang));
		keys.add(new JarawakParam("prop", transline.prop));
		keys.add(new JarawakParam("target", transline.target));
		keys.add(new JarawakParam("targetkey1", transline.targetkey1));
		keys.add(new JarawakParam("targetkey2", transline.targetkey2));
		keys.add(new JarawakParam("targetkey3", transline.targetkey3));
		keys.add(new JarawakParam("targetkey4", transline.targetkey4));
		keys.add(new JarawakParam("targetkey5", transline.targetkey5));

		try {
			transline.dtupdated = ZonedDateTime.now().format(Converters.format_db);
			transline.updatedby = userid;
			transline.saveMe(keys, null, false, false, true, false, true);
			return true;
		} catch (JarawakException | Exception e) {
			try {
				transline.dtcreated = ZonedDateTime.now().format(Converters.format_db);
				transline.createdby = userid;
				transline.createMe(null, null, false, false, false, true);
				return true;
			} catch (JarawakException | Exception e2) {

			}
		}
		return false;
	}

}
