package jarawak;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleHttpRequests;
import org.apache.hc.client5.http.async.methods.SimpleHttpResponse;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.io.HttpClientConnectionManager;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.client5.http.ssl.NoopHostnameVerifier;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactoryBuilder;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.io.support.ClassicRequestBuilder;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.hc.core5.http.ssl.TLS;
import org.apache.hc.core5.ssl.SSLContexts;
import org.json.JSONObject;

import jarawak.config.WSConfig;

public class JarawakHttp implements Serializable {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	private WSConfig conf = null;

	public JarawakHttp() {

	}

	public JarawakHttp(WSConfig conf) {
		this.conf = conf;
	}

	private String doCall(int method, String endpoint, String[][] params, JSONObject jso, boolean encoded,
			String[][] custHeaders, boolean useconfig) throws JarawakException {
		String outp = null;
		int statusCode = 0;

		if (endpoint == null || endpoint.trim().length() <= 0)
			throw new JarawakException("E", "HTTPCALL", "Empty Host");

		// PREPARE URL PARAMS GET/DELthrow new JarawakException("E","HTTPCALL","Invalid
		// Method");
		String epparams = "";
		if (method == JarawakWS.METHOD_GET || method == JarawakWS.METHOD_DELETE) {
			if (params != null && params.length > 0) {
				epparams = "";
				for (String[] s : params) {
					if (s[0] != null && s[1] != null)
						if (encoded)
							try {
								epparams = epparams + "&" + s[0] + "=" + URLEncoder.encode(s[1], "UTF-8");
							} catch (UnsupportedEncodingException e) {
								throw new JarawakException("E", "HTTPCALL", "Encoding params");
							}
						else
							epparams = epparams + "&" + s[0] + "=" + s[1];
				}
			}
			if (endpoint.indexOf("?") < 0)
				epparams = epparams.replaceFirst("&", "?");
		}

		// GET HTTPCLIENT
		Object client = null;

		try {
			client = getHttpClient();
		} catch (Exception e) {
			throw new JarawakException("E", "HTTPCALL", "Obtaining client: " + e.getMessage());
		}
		if (client == null)
			throw new JarawakException("E", "HTTPCALL", "Null client");

		// SELECT SYNC or ASYNC TYPES
		if (client.getClass().getName().equals("org.apache.hc.client5.http.impl.async.InternalHttpAsyncClient")) {

			CloseableHttpAsyncClient httpclient = null;
			try {
				httpclient = (CloseableHttpAsyncClient) client;
			} catch (Exception e) {
				throw new JarawakException("E", "HTTPCALL", "Obtaining httpclient: " + e.getMessage());
			}

			try {
				httpclient.start();
				SimpleHttpRequest wschama = null;
				switch (method) {
				case JarawakWS.METHOD_GET:
					wschama = SimpleHttpRequests.get(endpoint + epparams);
					break;
				case JarawakWS.METHOD_POST:
					wschama = SimpleHttpRequests.post(endpoint + epparams);
					break;
				case JarawakWS.METHOD_PUT:
					wschama = SimpleHttpRequests.put(endpoint + epparams);
					break;
				case JarawakWS.METHOD_DELETE:
					wschama = SimpleHttpRequests.delete(endpoint + epparams);
					break;
				case JarawakWS.METHOD_PATCH:
					wschama = SimpleHttpRequests.patch(endpoint + epparams);
					break;
				default:
					throw new JarawakException("E", "HTTPCALL", "Invalid Method");
				}
				if (custHeaders != null && custHeaders.length > 0)
					for (String[] s : custHeaders) {
						if (wschama.getFirstHeader(s[0]) != null)
							wschama.setHeader(s[0], s[1]);
						else
							wschama.addHeader(s[0], s[1]);
					}

				// PREPARE BODY POST/PUT
				if (method == JarawakWS.METHOD_POST || method == JarawakWS.METHOD_PUT) {
					if (params != null && params.length > 0) {
						List<NameValuePair> nvps = new ArrayList<NameValuePair>();
						for (String[] s : params) {
							if (s[0] != null && s[1] != null)
								if (encoded)
									nvps.add(new BasicNameValuePair(s[0], URLEncoder.encode(s[1], "UTF-8")));
								else
									nvps.add(new BasicNameValuePair(s[0], s[1]));
						}
						wschama.setBody(nvps.toString(), ContentType.APPLICATION_FORM_URLENCODED);
					} else if (jso != null) {
						wschama.setBody(jso.toString(), ContentType.APPLICATION_JSON);
					}

				}
				// CALL
				Future<SimpleHttpResponse> future = httpclient.execute(wschama, null);
				SimpleHttpResponse res = future.get();
				outp = res.getBodyText();
			} catch (Exception e) {
				throw new JarawakException("E", "HTTPCALL", "Error on call:" + e.getMessage());
			} finally {
				if (httpclient != null)
					try {
						httpclient.close();
					} catch (Exception e) {

					}
			}

		} else if (client.getClass().getName().equals("org.apache.hc.client5.http.impl.classic.InternalHttpClient")
				|| client.getClass().getName().equals("org.apache.hc.client5.http.impl.classic.CloseableHttpClient")) {
			CloseableHttpClient httpclient = (CloseableHttpClient) client;
			try {
				ClassicHttpResponse resposta = null;

				if (useconfig && conf != null && conf.portrange_start < conf.portrange_end) {
					// USER CONFIG WS PATH AND PORT RANGE CONNECTIONS
					boolean conok = false;
					int p = conf.portrange_start;
					int retries = conf.connect_retries;

					while (!conok) {

						ClassicRequestBuilder wschama = null;
						switch (method) {
						case JarawakWS.METHOD_GET:
							wschama = ClassicRequestBuilder.get(conf.host + ":" + p + conf.path + endpoint + epparams);
							break;
						case JarawakWS.METHOD_POST:
							wschama = ClassicRequestBuilder.post(conf.host + ":" + p + conf.path + endpoint + epparams);
							break;
						case JarawakWS.METHOD_PUT:
							wschama = ClassicRequestBuilder.put(conf.host + ":" + p + conf.path + endpoint + epparams);
							break;
						case JarawakWS.METHOD_DELETE:
							wschama = ClassicRequestBuilder
									.delete(conf.host + ":" + p + conf.path + endpoint + epparams);
							break;
						case JarawakWS.METHOD_PATCH:
							wschama = ClassicRequestBuilder
									.patch(conf.host + ":" + p + conf.path + endpoint + epparams);
							break;
						default:
							throw new JarawakException("E", "HTTPCALL", "Invalid Method");
						}

						if (custHeaders != null && custHeaders.length > 0)
							for (String[] s : custHeaders) {
								if (wschama.getFirstHeader(s[0]) != null)
									wschama.setHeader(s[0], s[1]);
								else
									wschama.addHeader(s[0], s[1]);
							}

						// PREPARE BODY POST/PUT
						if (method == JarawakWS.METHOD_POST || method == JarawakWS.METHOD_PUT) {
							if (params != null && params.length > 0) {
								List<NameValuePair> nvps = new ArrayList<NameValuePair>();
								for (String[] s : params) {
									if (s[0] != null && s[1] != null)
										if (encoded)
											nvps.add(new BasicNameValuePair(s[0], URLEncoder.encode(s[1], "UTF-8")));
										else
											nvps.add(new BasicNameValuePair(s[0], s[1]));
								}
								wschama.setEntity(new UrlEncodedFormEntity(nvps, Charset.forName("UTF-8")));
							} else if (jso != null) {
								wschama.addHeader("Content-Type", "application/json");
								wschama.setEntity(new StringEntity(jso.toString(), Charset.forName("UTF-8")));
							}
						}
						try {
							JarawakHttpCallThread wsthread = new JarawakHttpCallThread(httpclient, wschama);
							Thread thread = new Thread(wsthread);
							thread.start();
							thread.join();
							outp = wsthread.getOut();
							statusCode = wsthread.statusCode;
							if (statusCode >= 200 || statusCode <= 299)
								conok = true;
							break;
						} catch (Exception e) {

						}
						if (retries <= 0) {
							conok = true;
							break;
						}
						if (p >= conf.portrange_end) {
							wait(500);
							p = conf.portrange_start;
							retries--;
						} else
							p++;
					}

				} else {

					if (useconfig)
						endpoint = conf.host + conf.path + endpoint;

					// SIMPLE CALL
					ClassicRequestBuilder wschama = null;
					switch (method) {
					case JarawakWS.METHOD_GET:
						wschama = ClassicRequestBuilder.get(endpoint + epparams);
						break;
					case JarawakWS.METHOD_POST:
						wschama = ClassicRequestBuilder.post(endpoint + epparams);
						break;
					case JarawakWS.METHOD_PUT:
						wschama = ClassicRequestBuilder.put(endpoint + epparams);
						break;
					case JarawakWS.METHOD_DELETE:
						wschama = ClassicRequestBuilder.delete(endpoint + epparams);
						break;
					case JarawakWS.METHOD_PATCH:
						wschama = ClassicRequestBuilder.patch(endpoint + epparams);
						break;
					}
					if (custHeaders != null && custHeaders.length > 0)
						for (String[] s : custHeaders) {
							if (wschama.getFirstHeader(s[0]) != null)
								wschama.setHeader(s[0], s[1]);
							else
								wschama.addHeader(s[0], s[1]);
						}

					// PREPARE BODY POST/PUT
					if (method == JarawakWS.METHOD_POST || method == JarawakWS.METHOD_PUT) {
						if (params != null && params.length > 0) {
							List<NameValuePair> nvps = new ArrayList<NameValuePair>();
							for (String[] s : params) {
								if (s[0] != null && s[1] != null)
									if (encoded)
										nvps.add(new BasicNameValuePair(s[0], URLEncoder.encode(s[1], "UTF-8")));
									else
										nvps.add(new BasicNameValuePair(s[0], s[1]));
							}
							wschama.setEntity(new UrlEncodedFormEntity(nvps, Charset.forName("UTF-8")));
						} else if (jso != null) {
							wschama.addHeader("Content-Type", "application/json");
							wschama.setEntity(new StringEntity(jso.toString(), Charset.forName("UTF-8")));
						}
					}

					resposta = httpclient.execute(wschama.build(), HttpClientContext.create());
					statusCode = resposta.getCode();
					try {
						outp = EntityUtils.toString(resposta.getEntity(), Charset.forName("UTF-8"));
					} catch (Exception e) {

					}
				}
			} catch (Exception e) {
				throw new JarawakException("E", "HTTPCALL", "Erro on call: " + e.getMessage());
			} finally {

			}

		} else {
			return null;
		}

		if (statusCode < 200 || statusCode > 299) {
			throw new JarawakException("E", "HTTPCALL Status: " + String.valueOf(statusCode), outp);
		}

		return outp;
	}

	// ####################################################################
	// GET
	// ####################################################################
	private static final long serialVersionUID = 1L;

	public String doGET(String endpoint, String[][] params, boolean encoded, String[][] custHeaders)
			throws JarawakException {
		return doGET(endpoint, params, encoded, custHeaders, false);
	}

	public String doGET(String endpoint, String[][] params, boolean encoded, String[][] custHeaders, boolean useconfig)
			throws JarawakException {
		return doCall(JarawakWS.METHOD_GET, endpoint, params, null, encoded, custHeaders, useconfig);
	}

	// ####################################################################
	// POST
	// ####################################################################
	public String doPOST(String endpoint, String[][] params, JSONObject jso, boolean encoded, String[][] custHeaders)
			throws JarawakException {
		return doPOST(endpoint, params, jso, encoded, custHeaders, false);
	}

	public String doPOST(String endpoint, String[][] params, JSONObject jso, boolean encoded, String[][] custHeaders,
			boolean useconfig) throws JarawakException {
		return doCall(JarawakWS.METHOD_POST, endpoint, params, jso, encoded, custHeaders, useconfig);
	}

	// ####################################################################
	// PUT
	// ####################################################################

	public String doPUT(String endpoint, String[][] params, JSONObject jso, boolean encoded, String[][] custHeaders)
			throws JarawakException {
		return doPUT(endpoint, params, jso, encoded, custHeaders, false);
	}

	public String doPUT(String endpoint, String[][] params, JSONObject jso, boolean encoded, String[][] custHeaders,
			boolean useconfig) throws JarawakException {
		return doCall(JarawakWS.METHOD_PUT, endpoint, params, jso, encoded, custHeaders, useconfig);
	}

	// ####################################################################
	// PACTH
	// ####################################################################

	public String doPATCH(String endpoint, String[][] params, JSONObject jso, boolean encoded, String[][] custHeaders)
			throws JarawakException {
		return doPATCH(endpoint, params, jso, encoded, custHeaders, false);
	}

	public String doPATCH(String endpoint, String[][] params, JSONObject jso, boolean encoded, String[][] custHeaders,
			boolean useconfig) throws JarawakException {
		return doCall(JarawakWS.METHOD_PATCH, endpoint, params, jso, encoded, custHeaders, useconfig);
	}

	// ####################################################################
	// DELETE
	// ####################################################################

	public String doDELETE(String endpoint, boolean encoded, String[][] custHeaders) throws JarawakException {
		return doDELETE(endpoint, null, null, encoded, custHeaders, false);
	}

	public String doDELETE(String endpoint, String[][] params, JSONObject jso, boolean encoded, String[][] custHeaders)
			throws JarawakException {
		return doPOST(endpoint, params, jso, encoded, custHeaders, false);
	}

	public String doDELETE(String endpoint, String[][] params, JSONObject jso, boolean encoded, String[][] custHeaders,
			boolean useconfig) throws JarawakException {
		return doCall(JarawakWS.METHOD_DELETE, endpoint, params, jso, encoded, custHeaders, useconfig);
	}

	// ####################################################################
	// GET HTTP CLIENT
	// ####################################################################

	private Object getHttpClient() {
		if (conf != null && conf.http_pool) {
			return conf.httpclient;
		} else {
			SSLContext sslContext = SSLContexts.createDefault();
			try {
				sslContext.init(null, new TrustManager[] { new X509TrustManager() {
					@Override
					public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
							throws CertificateException {

					}

					@Override
					public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
							throws CertificateException {

					}

					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}

				} }, new SecureRandom());
			} catch (KeyManagementException e) {

			}

			final SSLConnectionSocketFactory sslSocketFactory = SSLConnectionSocketFactoryBuilder.create()
					.setSslContext(sslContext).setTlsVersions(TLS.V_1_0, TLS.V_1_1, TLS.V_1_2, TLS.V_1_3)
					.setHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
			final HttpClientConnectionManager cm = PoolingHttpClientConnectionManagerBuilder.create()
					.setSSLSocketFactory(sslSocketFactory).build();
			CloseableHttpClient httpclient = HttpClients.custom().setConnectionManager(cm).build();

			return httpclient;
		}

	}

}
