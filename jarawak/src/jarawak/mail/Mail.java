package jarawak.mail;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import jarawak.config.MailConfig;

public class Mail {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	private MailConfig conf = null;

	public Mail(MailConfig conf) {
		this.conf = conf;
	}

	public boolean sendMail(String to, String subj, String msg) {
		return sendMail(to, subj, msg, null, null);
	}

	public boolean sendMail(String to, String subj, String msg, byte[] attach, String attachname) {
		Properties props = new Properties();
		props.put("mail.smtp.host", conf.mailHost);
		props.put("mail.smtp.auth", conf.mailAuth);
		props.put("mail.smtp.port", conf.mailPort);
		props.put("mail.smtp.socketFactory.port", conf.mailPort);
		props.put("mail.smtp.starttls.enable", conf.mailTLS);
		if (conf.mailSSL != null && conf.mailSSL.trim().toUpperCase().equals("TRUE")) {
			props.put("mail.smtp.ssl.enable", conf.mailSSL);
			props.put("mail.transport.protocol", "smtps");
			props.put("mail.smtps.ssl.checkserveridentity", "false");
			props.put("mail.smtps.ssl.trust", "*");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		} else {
			props.put("mail.transport.protocol", "smtp");
		}
		props.put("mail.smtp.user", conf.mailFrom);
		props.put("mail.smtp.socketFactory.fallback", "false");
		// props.put("mail.debug", "true");

		Session session = null;
		if (conf.mailAuth != null && conf.mailAuth.trim().toUpperCase().equals("TRUE")) {
			SimpleAuth auth = null;
			auth = new SimpleAuth(conf.mailUser, conf.mailPass);
			session = Session.getInstance(props, auth);
		} else {
			session = Session.getInstance(props);
		}
		try {
			Message message = new MimeMessage(session);
			InternetAddress fromAddress = null;

			try {
				if (conf.mailFromName != null && conf.mailFromName.trim().length() > 0)
					fromAddress = new InternetAddress(conf.mailFrom, conf.mailFromName);
				else
					fromAddress = new InternetAddress(conf.mailFrom, conf.mailFrom);
			} catch (UnsupportedEncodingException e) {

			}

			message.setFrom(fromAddress);

			Address[] toUser = InternetAddress.parse(to);
			message.setRecipients(Message.RecipientType.TO, toUser);
			message.setSubject(subj);
			if (attach != null && attach.length > 0) {
				MimeMultipart mpart = new MimeMultipart();
				// Create the message part
				BodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(msg, "text/html; charset=UTF-8");
				mpart.addBodyPart(messageBodyPart);
				MimeBodyPart bp = new MimeBodyPart();
				ByteArrayDataSource bds = new ByteArrayDataSource(attach, "application/*");
				bp.setDataHandler(new DataHandler(bds));
				bp.setFileName(attachname);
				mpart.addBodyPart(bp);
				message.setContent(mpart);
			} else {
				message.setText(msg);
				message.setContent(msg, "text/html; charset=UTF-8");
			}

			Transport.send(message);
			return true;
		} catch (MessagingException e) {
			return false;
		}
	}
}

class SimpleAuth extends Authenticator {
	public String username = null;
	public String password = null;

	public SimpleAuth(String user, String pwd) {
		username = user;
		password = pwd;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	}
}
