/*
 * Copyright 2020 Leonardo Germano Roese
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package jarawak.tools;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import jarawak.JarawakException;

public class Converters {
	public static final DateTimeFormatter format_db = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssZ");
	public static final DateTimeFormatter format_db_date = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	public static final DateTimeFormatter format_date_br = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	public static final DateTimeFormatter format_date_timehm_br = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
	public static final DateTimeFormatter format_date_time_br = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ssZ");
	public static final DateTimeFormatter format_date_procore = DateTimeFormatter.ISO_DATE_TIME;
	public static final DateTimeFormatter format_sapquery = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

	/*
	 * #########################################################################
	 * REMOVE ACCENTS
	 * #######################################################################
	 */

	public static String unaccent(String src) {
		return src == null ? null
				: Normalizer.normalize(src, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
	}

	/*
	 * #########################################################################
	 * CONVERT STRING TO ZONED DATE TIME
	 * #######################################################################
	 */

	public static ZonedDateTime strdate2ZonedDT(String inp_date) {
		return strdate2ZonedDTZ(inp_date, "");
	}

	public static ZonedDateTime strdate2ZonedDTZ(String inp_date, String zonediff) {
		if (inp_date == null || inp_date.trim().length() < 10)
			return null;
		if (zonediff == null || zonediff.trim().length() <= 0)
			zonediff = "-0000";
		try {
			if (inp_date.trim().length() == 10)
				if (inp_date.indexOf("-") > 0)
					return strdate2ZonedDT(inp_date, format_db_date);
				else
					return strdate2ZonedDT(inp_date, format_date_br);
			else if (inp_date.trim().length() == 16) {
				if (inp_date.trim().indexOf('/') > 0) {
					return strdate2ZonedDT(inp_date + ":00" + zonediff, format_date_time_br);
				} else {
					return strdate2ZonedDT(inp_date + ":00" + zonediff, format_db);
				}
			} else if (inp_date.trim().length() == 19)
				if (inp_date.trim().indexOf('/') > 0) {
					return strdate2ZonedDT(inp_date + zonediff, format_date_time_br);
				} else {
					return strdate2ZonedDT(inp_date + zonediff, format_db);
				}
			else if (inp_date.trim().length() > 21) {
				if (inp_date.indexOf('T') >= 0)
					inp_date = inp_date.replaceAll("T", " ");
				if (inp_date.trim().length() > 23)
					if (inp_date.trim().indexOf('/') > 0) {
						return strdate2ZonedDT(inp_date, format_date_time_br);
					} else {
						return strdate2ZonedDT(inp_date, format_db);
					}
				else if (inp_date.trim().indexOf('/') > 0) {
					return strdate2ZonedDT(inp_date + "00", format_date_time_br);
				} else {
					return strdate2ZonedDT(inp_date + "00", format_db);
				}
			}
		} catch (Exception e) {

		}
		return null;
	}

	/*
	 * #########################################################################
	 * CONVERT STRING TO ZONED DATE TIME BASED ON SPECIFIC FORMAT
	 * #######################################################################
	 */

	public static ZonedDateTime strdate2ZonedDT(String inp_date, DateTimeFormatter formatter) {
		if (inp_date == null || inp_date.trim().length() < 10)
			return null;
		try {
			if (inp_date.trim().length() == 10) {
				return LocalDate.parse(inp_date, formatter).atStartOfDay(ZoneId.systemDefault());
			} else {
				return ZonedDateTime.parse(inp_date, formatter);
			}

		} catch (Exception e) {
		}
		return null;
	}

	/*
	 * #########################################################################
	 * CONVERT DATE TO LITERAL
	 * #######################################################################
	 */

	public static String strdate2lit(String inp_date, String lang) {
		String[] months = new String[] { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto",
				"Setembro", "Outubro", "Novembro", "Dezembro" };
		ZonedDateTime zdt = strdate2ZonedDT(inp_date);
		if (zdt != null) {
			return zdt.getDayOfMonth() + " de " + months[zdt.getMonthValue() - 1] + " de " + zdt.getYear();
		} else {
			return inp_date;
		}
	}

	/*
	 * #########################################################################
	 * CONVERT BRAZILIAN CURRENCY TO FLOAT
	 * #######################################################################
	 */

	public static float BRCurrency2float(String value, String lang) throws JarawakException {
		if (value == null || value.trim().length() <= 0)
			throw new JarawakException("E", "CONVERSION", "Inform value");

		if (value.contains(",")) {
			if (value.contains("."))
				value = value.replaceAll("\\.", "");
			value = value.replaceAll(",", ".");
		}
		try {
			return Float.parseFloat(value);
		} catch (Exception e) {
			throw new JarawakException("E", "CONVERSION", "Conversion esception " + e.getMessage());
		}
	}

	/*
	 * #########################################################################
	 * CONVERT UTF-8 TO ISO CHARS
	 * #######################################################################
	 */

	public static String UTF8toISO(String str) {
		Charset utf8charset = Charset.forName("UTF-8");
		Charset iso88591charset = Charset.forName("ISO-8859-1");

		ByteBuffer inputBuffer = ByteBuffer.wrap(str.getBytes());

		// decode UTF-8
		CharBuffer data = utf8charset.decode(inputBuffer);

		// encode ISO-8559-1
		ByteBuffer outputBuffer = iso88591charset.encode(data);
		byte[] outputData = outputBuffer.array();

		return new String(outputData);
	}

	/*
	 * #########################################################################
	 * SCAPE CHARACTERS TO SQL DATABASE
	 * #######################################################################
	 */

	public static String sqlScapeString(String str) {
		String data = null;
		if (str != null && str.length() > 0) {
			str = str.replace("\\", "\\\\");
			str = str.replace("'", "\\'");
			str = str.replace("\0", "\\0");
			str = str.replace("\n", "\\n");
			str = str.replace("\r", "\\r");
			str = str.replace("\"", "\\\"");
			str = str.replace("\\x1a", "\\Z");
			data = str;
		}
		return data;
	}
}
