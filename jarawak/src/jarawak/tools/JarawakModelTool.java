/*
 * Copyright 2020 Leonardo Germano Roese
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package jarawak.tools;

import java.io.File;
import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.util.ArrayList;

import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;
import jarawak.config.DBConfig;
import jarawak.connectors.PGConnector;
import jarawak.models.modelModel;
import jarawak.models.modelModelProperty;

public class JarawakModelTool {

	public JarawakModelTool() {

	}

	/*
	 * #############################################################################
	 * ######### LIST MODELS
	 * #############################################################################
	 * ######
	 */
	public modelModel[] listModels(ClassLoader cl, String path) throws JarawakException {

		ArrayList<modelModel> models = scanModels(cl, null, path, path);

		if (models.size() > 0) {
			modelModel[] out = new modelModel[models.size()];
			for (int i = 0; i < out.length; i++) {
				out[i] = new modelModel(null, null);
			}
			return models.toArray(out);
		}

		return null;
	}

	private ArrayList<modelModel> scanModels(ClassLoader cl, ArrayList<modelModel> models, String path,
			String initialpath) {
		if (models == null)
			models = new ArrayList<modelModel>();
		File root = new File(path);
		File[] list = root.listFiles();
		if (list == null)
			return models;
		for (File f : list) {
			if (f.isDirectory()) {
				models = scanModels(cl, models, f.getAbsolutePath(), initialpath);
			} else {
				if (f.getAbsolutePath().toUpperCase().endsWith(".JAR")) {
					// DECOMPRESS
					try {
						final String decopath = Files.createTempDirectory("JARAWAKMONITOR").toAbsolutePath().toString();
						// EXTRACT
						UnzipUtility uzip = new UnzipUtility();
						uzip.unzip(f.getAbsolutePath(), decopath);
						models = scanModels(cl, models, decopath, decopath);
					} catch (Exception e) {

					}

				} else if (!f.getAbsolutePath().toUpperCase().endsWith(".CLASS")) {
					return models;
				}
				try {
					String fclass = f.getAbsolutePath().substring(initialpath.length());
					if (fclass.startsWith("/"))
						fclass = fclass.substring(1);
					fclass = fclass.replaceAll("/", "\\.");
					fclass = fclass.substring(0, fclass.length() - 6);
					Class<?> c = cl.loadClass(fclass);
					if (c.getSuperclass().getName().contentEquals("jarawak.JarawakModel")) {
						modelModel m = new modelModel(null, null);
						m.name = c.getSimpleName();
						m.cls = fclass;
						// PROPERTIES
						try {
							Constructor<?> constr = c.getConstructors()[0];
							JarawakModel mod = null;
							try {
								mod = (JarawakModel) constr.newInstance(
										new Object[] { new PGConnector(new DBConfig(), null, null), null });
							} catch (Exception w) {
								try {
									mod = (JarawakModel) constr.newInstance();
								} catch (Exception y) {

								}
							}
							JarawakParam[] metas = mod.getMetainfodb();
							if (mod.getTableName() != null)
								m.table_name = mod.getTableName();
							if (metas != null && metas.length > 0) {
								ArrayList<modelModelProperty> props = new ArrayList<modelModelProperty>();
								for (JarawakParam meta : metas) {
									if (meta.value != null && !meta.value.equals(JarawakParam.TYPE_EXCLUDED)
											&& !meta.value.equals(JarawakParam.TYPE_OBJECT)
											&& !meta.value.equals(JarawakParam.TYPE_ARRAY_OBJECT)) {
										modelModelProperty mp = new modelModelProperty(null, null);
										mp.id = "0";
										mp.name = meta.name;
										mp.datatype = meta.value;
										if (meta.mode != null) {
											mp.dbtype = meta.mode;
										}
										if (meta.primfore != null && meta.primfore.equals(JarawakParam.KEY_PRIMARY)) {
											mp.primary = meta.primfore;
										}
										props.add(mp);
									}
								}
								if (props.size() > 0) {
									modelModelProperty[] ps = new modelModelProperty[props.size()];
									for (int i = 0; i < props.size(); i++)
										ps[i] = new modelModelProperty(new PGConnector(new DBConfig(), null, null),
												null);
									m.properties = props.toArray(ps);
								}
							}

						} catch (Exception ee) {

						}
						models.add(m);
					}
				} catch (Exception | NoClassDefFoundError | IllegalAccessError e) {

				}

			}
		}
		return models;
	}

}
