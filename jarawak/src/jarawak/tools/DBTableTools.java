/*
 * Copyright 2020 Leonardo Germano Roese
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package jarawak.tools;

import java.util.ArrayList;

import javax.sql.DataSource;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakWS;
import jarawak.connectors.PGConnector;
import jarawak.db.ConBase;
import jarawak.db.DBLin;
import jarawak.models.modelTable;
import jarawak.models.modelTableColumn;

public class DBTableTools {

	/*
	 * #############################################################################
	 * ######### LIST TABLES
	 * #############################################################################
	 * ######
	 */
	public static modelTable[] listAllTables(DataSource ds, String schema, int dbtype, int page, int limit,
			boolean withcolumns) throws JarawakException {

		if (ds == null)
			throw new JarawakException("E", "DBTOOLS", "Inform DB DataSource");

		ConBase con = new ConBase(ds, dbtype, null);
		JarawakConnector connector = null;

		String query = null;

		switch (dbtype) {
		case ConBase.DBD_POSTGRESQL:
			query = "SELECT pg_catalog.pg_tables.schemaname AS schema, pg_catalog.pg_tables.tablename AS name, pg_catalog.pg_tables.tableowner AS owner,"
					+ " pg_catalog.pg_tables.hasindexes AS indexed, pg_catalog.pg_tables.hastriggers AS trigger "
					+ " FROM pg_catalog.pg_tables WHERE NOT schemaname IS NULL";
			if (schema != null && schema.trim().length() > 0)
				query = query + " AND UPPER(schemaname) = '" + schema.toUpperCase() + "' ";
			if (page != 0 && limit != 0)
				query = query + " LIMIT " + String.valueOf(limit) + " OFFSET " + String.valueOf(page * limit);
			query = query + " ORDER BY name ASC";
			connector = new PGConnector(ds, null, null);
			break;

		default:
			throw new JarawakException("E", "DBTOOLS", "Invalid DB Type");

		}

		// EXEC QUERY
		try {
			ArrayList<DBLin> lin = con.readDb(query);
			if (lin != null && lin.size() > 0) {
				modelTable[] tables = (modelTable[]) JarawakWS.convDBL2O(modelTable.class, null, lin, connector, null,
						true);
				if (withcolumns && tables != null & tables.length > 0) {
					int cnt = 0;
					for (modelTable t : tables) {
						tables[cnt].columns = listAllTableColumns(ds, t.schema, dbtype, t.name);
						cnt++;
					}
				}
				return tables;
			}
		} catch (JarawakException e) {

		}

		return null;
	}

	/*
	 * #############################################################################
	 * ######### LIST TABLE COLUMNS
	 * #############################################################################
	 * ######
	 */

	public static modelTableColumn[] listAllTableColumns(DataSource ds, String schema, int dbtype, String tablename)
			throws JarawakException {

		if (ds == null)
			throw new JarawakException("E", "DBTOOLS", "Inform DB Data Source");
		if (schema == null || schema.trim().length() <= 0)
			throw new JarawakException("E", "DBTOOLS", "Inform schema");
		if (tablename == null || tablename.trim().length() <= 0)
			throw new JarawakException("E", "DBTOOLS", "Inform table name");

		ConBase con = new ConBase(ds, dbtype, null);
		JarawakConnector connector = null;
		String query = null;

		switch (dbtype) {
		case ConBase.DBD_POSTGRESQL:
			query = "SELECT table_name AS tablename, table_schema as schemaname, column_name AS name, ordinal_position AS position,"
					+ " is_nullable AS nullable, data_type AS datatype, character_maximum_length AS datalength, numeric_precision_radix AS precision"
					+ " FROM information_schema.columns WHERE UPPER(table_schema) = '" + schema.toUpperCase()
					+ "' AND UPPER(table_name) = '" + tablename.toUpperCase() + "' ";
			connector = new PGConnector(ds, null, null);
			break;
		default:
			throw new JarawakException("E", "DBTOOLS", "Invalid DB Type");
		}

		// EXEC QUERY
		try {
			ArrayList<DBLin> lin = con.readDb(query);
			if (lin != null && lin.size() > 0) {
				modelTableColumn[] columns = (modelTableColumn[]) JarawakWS.convDBL2O(modelTableColumn.class, null, lin,
						connector, null, true);
				return columns;
			}
		} catch (JarawakException e) {

		}

		return null;
	}

}
