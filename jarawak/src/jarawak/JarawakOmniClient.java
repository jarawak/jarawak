/*
 * Copyright 2020 Leonardo Germano Roese
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package jarawak;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jarawak.config.WSConfig;
import jarawak.models.modelOmniResult;

public abstract class JarawakOmniClient {

	private JarawakConnector con = null;

	public JarawakOmniClient(JarawakConnector connector, String encoding) {
		con = connector;
	}

	public Object[] processCallA(Object toprocess, String methodname, Object... objs) throws JarawakException {
		JarawakWS ws = new JarawakWS();

		modelOmniResult ominres = execCall(toprocess, methodname, objs);

		if (ominres != null && ominres.returnresult != null && ominres.returnresult.trim().length() > 0) {
			if (ominres.returnresult.trim().startsWith("[")) {
				try {
					JSONArray rjsa = new JSONArray(ominres.returnresult);
					ClassLoader cl = con.getClass().getClassLoader();
					if (ominres.returntype.trim().indexOf("[]") > 0)
						ominres.returntype = ominres.returntype.substring(0, ominres.returntype.length() - 2);
					Class<?> retclz = cl.loadClass(ominres.returntype);
					Object[] oa = (Object[]) Array.newInstance(retclz, rjsa.length());
					int cnob = 0;
					for (Object xo : oa) {
						for (Constructor<?> constr : retclz.getConstructors()) {
							if (constr.getParameterTypes().length == 0)
								oa[cnob] = constr.newInstance();
							else if (constr.getParameterTypes().length == 2)
								oa[cnob] = constr.newInstance(new Object[] { null, null });
						}
						cnob++;
					}
					oa = ws.json2ObjectA(oa, rjsa, true);
					return oa;
				} catch (Exception e) {
					throw new JarawakException("E", "CALL", "Convert array. " + e.getMessage());
				}

			} else if (ominres.returnresult.indexOf("errortype") > 0) {
				String errortype = null;
				String errorcode = null;
				String errormessage = null;
				try {
					JSONObject rjso = new JSONObject(ominres.returnresult);
					if (rjso.has("errortype"))
						errortype = rjso.getString("errortype");
					if (rjso.has("errorcode"))
						errorcode = rjso.getString("errorcode");
					if (rjso.has("errormessage"))
						errormessage = rjso.getString("errormessage");
				} catch (Exception e) {
					throw new JarawakException("E", "CALL", "Try to get error object, msg: " + e.getMessage());
				}

				throw new JarawakException(errortype, errorcode, errormessage);
			}
		}

		return null;
	}

	public Object processCall(Object toprocess, String methodname, Object... objs) throws JarawakException {
		JarawakWS ws = new JarawakWS();

		modelOmniResult ominres = execCall(toprocess, methodname, objs);

		if (ominres != null && ominres.returnresult != null && ominres.returnresult.trim().length() > 0) {
			if (ominres.returntype.equals(String.class.getName())) {
				return ominres.returnresult;
			} else if (ominres.returnresult.trim().startsWith("{")) {
				if (ominres.returnresult.indexOf("errortype") > 0 && ominres.returnresult.indexOf("error") < 4) {
					String errortype = null;
					String errorcode = null;
					String errormessage = null;
					try {
						JSONObject rjso = new JSONObject(ominres.returnresult);
						if (rjso.has("errortype"))
							errortype = rjso.getString("errortype");
						if (rjso.has("errorcode"))
							errorcode = rjso.getString("errorcode");
						if (rjso.has("errormessage"))
							errormessage = rjso.getString("errormessage");
					} catch (Exception e) {
						throw new JarawakException("E", "CALL", "Try to get error object, msg: " + e.getMessage());
					}

					throw new JarawakException(errortype, errorcode, errormessage);
				} else {
					try {
						JSONObject rjso = new JSONObject(ominres.returnresult);
						ClassLoader cl = con.getClass().getClassLoader();
						Class<?> retclz = cl.loadClass(ominres.returntype);
						Constructor<?> constr = retclz.getConstructors()[0];
						Object o = null;
						if (constr.getParameterTypes().length == 0)
							o = constr.newInstance();
						else if (constr.getParameterTypes().length == 2)
							o = constr.newInstance(new Object[] { null, null });
						o = ws.json2Object(o, rjso, true);
						return o;
					} catch (JSONException | ClassNotFoundException | InstantiationException | IllegalAccessException
							| IllegalArgumentException | InvocationTargetException e) {
						throw new JarawakException("E", "CALL", "Get object. " + e.getMessage());
					}
				}
			} else {
				return ominres.returnresult;
			}
		}
		return null;
	}

	private modelOmniResult execCall(Object toprocess, String methodname, Object... objs) throws JarawakException {
		JarawakWS ws = new JarawakWS();
		Class<?> clz = toprocess.getClass();
		Method[] metz = clz.getDeclaredMethods();
		modelOmniResult ominres = new modelOmniResult(con, null);
		if (metz != null && metz.length > 0) {
			for (Method m : metz) {
				if (m.getName().equals(methodname)) {
					String clzname = clz.getName();
					if (clzname.indexOf('.') > 0) {
						String[] m_clzn = clzname.split("\\.");
						ominres.cls = clzname.substring(0, clzname.lastIndexOf('.')) + ".o_"
								+ m_clzn[m_clzn.length - 1];
					} else {
						ominres.cls = "o_" + clz.getName();
					}
					ominres.meth = methodname;
					ominres.returntype = m.getReturnType().getCanonicalName();
					// GET PARAMETERS
					JSONObject jparams = new JSONObject();
					JSONObject jparamvals = new JSONObject();
					Parameter[] ps = m.getParameters();
					if (ps != null && ps.length > 0) {
						int parcount = 0;
						for (Parameter p : ps) {
							if (p.getType().isArray()) {
								// --- ARRAY
								JSONArray jap = null;
								try {
									jparams.put(p.getName(), p.getType().getCanonicalName());
								} catch (JSONException e1) {

								}
								// OF String
								if (p.getType().getName().equals("[L" + String.class.getName() + ";")) {
									try {
										jap = new JSONArray(objs[parcount]);
										jparamvals.put(p.getName(), jap);
									} catch (Exception e) {
										throw new JarawakException("E", "OMNI",
												"Could not get parameter type: " + p.getName());
									}
								} else {
									try {
										jap = ws.lin2json((Object[]) objs[parcount], true);
										if (jap == null)
											jap = new JSONArray();
										jparamvals.put(p.getName(), jap);
									} catch (Exception e) {
										throw new JarawakException("E", "OMNI",
												"Could not get parameter type: " + p.getName());
									}
								}
							} else if (p.getType().isPrimitive()) {
								// --- PRIMITIVE
								try {
									jparams.put(p.getName(), p.getType().getCanonicalName());
									jparamvals.put(p.getName(), objs[parcount]);
								} catch (JSONException e) {
									throw new JarawakException("E", "OMNI",
											"Could not get parameter type: " + p.getName());
								}

							} else if (p.getType().getName().contentEquals(String.class.getName())) {
								// --- STRING OBJECT
								try {
									jparams.put(p.getName(), p.getType().getCanonicalName());
									String val = "";
									try {
										val = (String) objs[parcount];
										if (val == null)
											val = "";
									} catch (Exception e) {

									}
									try {
										jparamvals.put(p.getName(), URLEncoder.encode(val, "UTF-8"));
									} catch (UnsupportedEncodingException e) {

									}
								} catch (JSONException e) {
									throw new JarawakException("E", "OMNI",
											"Could not get parameter type: " + p.getName());
								}
							} else {
								// ---OBJECT
								JSONObject jop = null;
								try {
									jop = ws.lin2json(objs[parcount], true);
									jparams.put(p.getName(), p.getType().getCanonicalName());
									if (jop != null)
										jparamvals.put(p.getName(), jop);
									else
										jparamvals.put(p.getName(), new JSONObject());
								} catch (Exception e) {
									throw new JarawakException("E", "OMNI",
											"Could not get parameter type: " + p.getName());
								}

							}
							parcount++;
						}
					}

					ominres.parameters = jparams.toString();
					ominres.parametervalues = jparamvals.toString();
					try {
						ominres.returnresult = ominres.createMe(null, null, false, false, false,
								((WSConfig) con.getConf()).http_pool);
						return ominres;
					} catch (JarawakException e) {
						throw new JarawakException("E", "CALL", e.getMessage());
					}
				}
			}
		}
		return null;
	}
}
