package jarawak;

import java.io.Serializable;
import java.net.URLDecoder;

public class JarawakException extends Throwable implements Serializable {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	private static final long serialVersionUID = 1L;

	public String Message = null;
	public String Code = null;
	public String Type = null;

	public JarawakException() {
		this.Code = null;
		this.Type = null;
		this.Message = null;
	}

	public JarawakException(String type, String code, String message) {
		this.Code = code;
		this.Type = type;
		this.Message = message;
	}

	public String getMessage() {
		if (Message != null && Message.trim().length() > 0)
			try {
				Message = URLDecoder.decode(Message, "UTF-8");
			} catch (Exception e) {

			}
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

}
