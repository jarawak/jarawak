package jarawak;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONObject;

public class JarawakHttpAndroid {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	private static final String[] arrMeth = new String[] { "GET", "POST", "PUT", "DELETE" };
	private static final int CONN_TIMEOUT = 10000;

	public JarawakHttpAndroid() {
	}

	// ##############################################################################################
	// GET
	// ##############################################################################################
	public String doGET(String endpoint, String[][] params, boolean encoded) throws JarawakException {
		return execHttpCall(endpoint, params, null, JarawakWS.METHOD_GET, encoded);
	}

	public String doGET(String endpoint, String[][] params, boolean encoded, String[][] headerparams)
			throws JarawakException {
		return execHttpCall(endpoint, params, null, JarawakWS.METHOD_GET, encoded, headerparams);
	}
	// ##############################################################################################
	// POST
	// ##############################################################################################

	public String doPOST(String endpoint, String[][] params, JSONObject jobj, boolean encoded) throws JarawakException {
		return execHttpCall(endpoint, params, jobj, JarawakWS.METHOD_POST, encoded);
	}

	public String doPOST(String endpoint, String[][] params, JSONObject jobj, boolean encoded, String[][] headerparams)
			throws JarawakException {
		return execHttpCall(endpoint, params, jobj, JarawakWS.METHOD_POST, encoded, headerparams);
	}
	// ##############################################################################################
	// PUT
	// ##############################################################################################

	public String doPUT(String endpoint, String[][] params, JSONObject jobj, boolean encoded) throws JarawakException {
		return execHttpCall(endpoint, params, jobj, JarawakWS.METHOD_PUT, encoded);
	}

	public String doPUT(String endpoint, String[][] params, JSONObject jobj, boolean encoded, String[][] headerparams)
			throws JarawakException {
		return execHttpCall(endpoint, params, jobj, JarawakWS.METHOD_PUT, encoded, headerparams);
	}
	// ##############################################################################################
	// PATCH
	// ##############################################################################################

	public String doPATCH(String endpoint, String[][] params, JSONObject jobj, boolean encoded)
			throws JarawakException {
		return execHttpCall(endpoint, params, jobj, JarawakWS.METHOD_PATCH, encoded);
	}

	public String doPATCH(String endpoint, String[][] params, JSONObject jobj, boolean encoded, String[][] headerparams)
			throws JarawakException {
		return execHttpCall(endpoint, params, jobj, JarawakWS.METHOD_PATCH, encoded, headerparams);
	}

	// ##############################################################################################
	// DELETE
	// ##############################################################################################

	public String doDELETE(String endpoint, boolean encoded, String[][] headerparams) throws JarawakException {
		return execHttpCall(endpoint, null, null, JarawakWS.METHOD_DELETE, encoded, headerparams);
	}

	// ##############################################################################################
	// EXEC HTTP CALL
	// ##############################################################################################

	private String execHttpCall(String endpoint, String[][] params, JSONObject jobj, int method, boolean urlencoded)
			throws JarawakException {
		return execHttpCall(endpoint, params, jobj, method, urlencoded, null);
	}

	private String execHttpCall(String endpoint, String[][] params, JSONObject jobj, int method, boolean urlencoded,
			String[][] headerparams) throws JarawakException {
		String parcon = "";
		String linha = "";
		int timeout = CONN_TIMEOUT;

		if (params != null && params.length > 0) {
			for (int i = 0; i < params.length; i++) {
				if (params[i][0] != null && params[i][1] != null)
					try {
						if (urlencoded)
							parcon = parcon + "&" + params[i][0] + "=" + URLEncoder.encode(params[i][1], "UTF-8");
						else
							parcon = parcon + "&" + params[i][0] + "=" + params[i][1];
					} catch (UnsupportedEncodingException e) {

					}
			}
		}

		HttpURLConnection connection = null;
		try {

			// Concatenate parameters to endpoint
			if (parcon != null && parcon.length() > 0 && method == JarawakWS.METHOD_GET)
				endpoint = endpoint + "?" + parcon.substring(1);

			URL url = new URL(endpoint);
			connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(timeout);

			// SET HEADERS
			if (headerparams != null && headerparams.length > 0)
				for (String[] s : headerparams)
					connection.setRequestProperty(s[0], s[1]);

			// Pre-set for post
			if (method == JarawakWS.METHOD_POST || method == JarawakWS.METHOD_PUT) {
				connection.setDoOutput(true);
			} else {
				connection.setDoOutput(false);
			}
			connection.setRequestMethod(arrMeth[method - 1]);
			connection.setInstanceFollowRedirects(false);

			if (jobj != null) {
				connection.setRequestProperty("Content-Type", "application/json");
				if (method == JarawakWS.METHOD_GET)
					connection.setRequestProperty("Transfer-Encoding", "chunked");
				else
					connection.setRequestProperty("Content-Type", "application/json");
				OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
				osw.write(jobj.toString());
				osw.flush();
				osw.close();
			} else if (method != JarawakWS.METHOD_GET && parcon != null && parcon.trim().length() > 0) {
				connection.setDoOutput(true);
				byte[] postData = parcon.getBytes();
				connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				connection.setRequestProperty("charset", "utf-8");
				connection.setRequestProperty("Content-Length", String.valueOf(postData.length));
				connection.setUseCaches(false);
				OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
				osw.write(parcon);
				osw.flush();
				osw.close();
			}

			int stt = connection.getResponseCode();
			if (stt >= 400d) {
				try {
					BufferedInputStream is = new BufferedInputStream(connection.getErrorStream());
					if (is != null) {
						try {
							BufferedReader br = new BufferedReader(new InputStreamReader(is));
							String inputLine = "";
							while ((inputLine = br.readLine()) != null) {
								linha = linha + inputLine;
							}
						} catch (Exception e) {
							linha = "";
						} finally {
							if (is != null)
								is.close();
						}
					}
				} catch (Exception e) {
				}
				connection.disconnect();
				throw new JarawakException("E", String.valueOf(stt), linha);

			} else {
				try {
					BufferedInputStream is = new BufferedInputStream(connection.getInputStream());
					if (is != null) {
						try {
							BufferedReader br = new BufferedReader(new InputStreamReader(is));
							String inputLine = "";
							while ((inputLine = br.readLine()) != null) {
								linha = linha + inputLine;
							}
						} catch (Exception e) {
							linha = "";
						} finally {
							if (is != null)
								is.close();
						}
					}
				} catch (Exception e) {
				}
				connection.disconnect();
			}

		} catch (Exception e) {
			if (connection != null)
				connection.disconnect();

			throw new JarawakException("E", "WEBSERVICE", e.getMessage());

		} finally {
			if (connection != null)
				connection.disconnect();
		}
		return linha;

	}

}
