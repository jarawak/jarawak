package jarawak;

import org.json.JSONObject;

public class JarawakWSAndroid extends JarawakWS {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	@Override
	public String callWSAS(String host, String endpoint, String[][] params, JSONObject jso, boolean encoded, int method,
			String[][] headerparams, boolean useconfig) throws JarawakException {
		switch (method) {
		case METHOD_GET:
			return new JarawakHttpAndroid().doGET(host + endpoint, params, encoded, headerparams);
		case METHOD_POST:
			return new JarawakHttpAndroid().doPOST(host + endpoint, params, jso, encoded, headerparams);
		case METHOD_PUT:
			return new JarawakHttpAndroid().doPUT(host + endpoint, params, jso, encoded, headerparams);
		case METHOD_DELETE:
			return new JarawakHttpAndroid().doDELETE(host + endpoint, encoded, headerparams);
		}
		return null;
	}

}
