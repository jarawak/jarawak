package jarawak;

import java.util.ArrayList;

import javax.sql.DataSource;

import org.json.JSONObject;

import jarawak.config.DBConfig;
import jarawak.db.DBLin;

public abstract class JarawakConnector implements java.io.Serializable {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Object conf = null;
	private String endpoint = null;
	private JarawakParam[] metainfotype = null;
	public String totalrows = null;
	private String[][] headers = null;
	private DataSource ds = null;

	/*
	 * #########################################################################
	 * ##### CONSTRUCTOR
	 * #########################################################################
	 * #####
	 */

	public JarawakConnector(Object config, String endpoint, JarawakParam[] metainfotype) {
		this.conf = config;
		this.endpoint = endpoint;
		this.metainfotype = metainfotype;
	}

	public JarawakConnector(DataSource ds, String endpoint, JarawakParam[] metainfotype) {
		this.ds = ds;
		this.endpoint = endpoint;
		this.metainfotype = metainfotype;
	}

	public JarawakConnector(Object config, String endpoint, JarawakParam[] metainfotype, String[][] headerparams) {
		this.conf = config;
		this.endpoint = endpoint;
		this.metainfotype = metainfotype;
		this.headers = headerparams;
	}

	/*
	 * #########################################################################
	 * ##### GETTERS SETTERS
	 * #########################################################################
	 * #####
	 */
	public Object getConf() {
		return conf;
	}

	public void setConf(DBConfig conf) {
		this.conf = conf;
	}

	public String[][] getHeaders() {
		return this.headers;
	}

	public void setHeaders(String[][] headers) {
		this.headers = headers;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public JarawakParam[] getMetainfotype() {
		return metainfotype;
	}

	public void setMetainfotype(JarawakParam[] metainfotype) {
		this.metainfotype = metainfotype;
	}

	/*
	 * #########################################################################
	 * ##### METHODS
	 * #########################################################################
	 * #####
	 */

	public ArrayList<DBLin> list(ArrayList<JarawakParam> keyskeys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal) throws JarawakException {
		return null;
	}

	public ArrayList<DBLin> list(ArrayList<JarawakParam> keyskeys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal, boolean intercepterror) throws JarawakException {
		return null;
	}

	public ArrayList<DBLin> list(ArrayList<JarawakParam> keyskeys, JarawakParam[] ord, int page, int limit,
			boolean bringtotal, boolean intercepterror, boolean useconfig) throws JarawakException {
		return null;
	}

	public DBLin get(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return null;
	}

	public DBLin get(ArrayList<JarawakParam> keys, String uid, boolean intercepterror) throws JarawakException {
		return null;
	}

	public DBLin get(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, boolean useconfig)
			throws JarawakException {
		return null;
	}

	public String delete(ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return null;
	}

	public String delete(ArrayList<JarawakParam> keys, String uid, boolean intercepterror) throws JarawakException {
		return null;
	}

	public String delete(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, String sessionuser)
			throws JarawakException {
		return null;
	}

	public String delete(ArrayList<JarawakParam> keys, String uid, boolean intercepterror, String sessionuser,
			boolean useconfig) throws JarawakException {
		return null;
	}

	public String create(String pk, JSONObject params) throws JarawakException {
		return null;
	}

	public String create(String pk, JSONObject params, boolean intercepterror) throws JarawakException {
		return null;
	}

	public String create(String pk, JSONObject params, boolean intercepterror, boolean encode) throws JarawakException {
		return null;
	}

	public String create(String pk, JSONObject params, boolean intercepterror, boolean encode, boolean useconfig)
			throws JarawakException {
		return null;
	}

	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid) throws JarawakException {
		return null;
	}

	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid, boolean intercepterror)
			throws JarawakException {
		return null;
	}

	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid, boolean intercepterror,
			boolean encode) throws JarawakException {
		return null;
	}

	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid, boolean intercepterror,
			boolean encode, String sessionuser) throws JarawakException {
		return null;
	}

	public String save(JSONObject params, ArrayList<JarawakParam> keys, String uid, boolean intercepterror,
			boolean encode, String sessionuser, boolean useconfig) throws JarawakException {
		return null;
	}

	public boolean createTable(String name, ArrayList<JarawakParam> fields, boolean force) throws JarawakException {
		return false;
	}

	public boolean alterTable(String name, ArrayList<JarawakParam> fields) throws JarawakException {
		return false;
	}

	public void setsessionuser(String id) throws JarawakException {
	}

	/*
	 * #########################################################################
	 * ##### GET METINFOTYPE
	 * #########################################################################
	 * #####
	 */

	public String getMetaInfoType(String fname) {
		if (fname == null || metainfotype == null)
			return null;
		for (JarawakParam mi : metainfotype)
			if (mi.name.trim().toLowerCase().equals(fname.trim().toLowerCase()))
				return mi.value;
		return null;

	}

	public JarawakParam getMetaInfoTypeP(String fname) {
		if (fname == null || metainfotype == null)
			return null;
		for (JarawakParam mi : metainfotype)
			if (mi.name.trim().toLowerCase().equals(fname.trim().toLowerCase()))
				return mi;
		return null;

	}

	/*
	 * #########################################################################
	 * ##### GET METINFOTYPE MODE
	 * #########################################################################
	 * #####
	 */

	public String getMetaInfoTypeMode(String fname) {
		if (fname == null || metainfotype == null)
			return null;
		for (JarawakParam mi : metainfotype)
			if (mi.name.trim().toLowerCase().equals(fname.trim().toLowerCase()))
				return mi.mode;
		return null;

	}

	/*
	 * #########################################################################
	 * ##### CHECK FIELD EXCLUDED(true)
	 * #########################################################################
	 * #####
	 */

	public boolean checkFieldExcluded(String fname) {
		if (fname == null)
			return false;
		String[] fexcl = new String[] { "serialVersionUID", "targetDB", "sconf", "DBD", "metainfodb" };
		for (String s : fexcl)
			if (s.trim().equals(fname.trim()))
				return true;
		if (metainfotype != null && metainfotype.length > 0)
			for (JarawakParam ml : metainfotype)
				if (ml != null && ml.name.trim().equals(fname.trim()) && ml.value != null
						&& ml.value.trim().toUpperCase().equals(JarawakParam.TYPE_EXCLUDED))
					return true;

		return false;

	}

	/*
	 * #########################################################################
	 * ##### CHECK IF SERIAL
	 * #########################################################################
	 * #####
	 */

	public boolean checkFieldSerial(String fname) {
		if (fname == null)
			return false;
		if (metainfotype != null && metainfotype.length > 0)
			for (JarawakParam ml : metainfotype)
				if (ml != null && ml.name.trim().equals(fname.trim())
						&& ml.mode.trim().toUpperCase().equals(JarawakParam.DB_SERIAL))
					return true;

		return false;

	}

	/*
	 * #########################################################################
	 * ##### GET METHOD USED
	 * #########################################################################
	 * #####
	 */
	public int getMethod(String method) throws JarawakException {

		if (method == null || method.trim().length() <= 0)
			throw new JarawakException("E", "CONNECTOR", "connector.e.informmethod");

		if (method.trim().toUpperCase().equals("GET"))
			return JarawakWS.METHOD_GET;
		if (method.trim().toUpperCase().equals("POST"))
			return JarawakWS.METHOD_POST;
		if (method.trim().toUpperCase().equals("PATCH"))
			return JarawakWS.METHOD_PATCH;
		if (method.trim().toUpperCase().equals("PUT"))
			return JarawakWS.METHOD_PUT;
		if (method.trim().toUpperCase().equals("DELETE"))
			return JarawakWS.METHOD_DELETE;

		throw new JarawakException("E", "CONNECTOR", "connector.e.methodnotfound");
	}

	/*
	 * #########################################################################
	 * ##### GET PARAM MODE
	 * #########################################################################
	 * #####
	 */
	public String getParamsMode(String param) throws JarawakException {

		if (param == null || param.trim().length() <= 0)
			throw new JarawakException("E", "CONNECTOR", "connector.e.informparammode");
		param = param.trim().toUpperCase();

		if (param.equals(JarawakParam.PARAM_ON_REQUEST) || param.equals(JarawakParam.PARAM_AS_PARAMETER)
				|| param.equals(JarawakParam.PARAM_AS_JSON))
			return param;

		throw new JarawakException("E", "CONNECTOR", "connector.e.parammodenotfound");
	}
}
