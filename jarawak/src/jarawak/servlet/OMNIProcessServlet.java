/*
 * Copyright 2020 Leonardo Germano Roese
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package jarawak.servlet;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jarawak.JarawakException;
import jarawak.JarawakWS;
import jarawak.connectors.PGConnector;
import jarawak.models.modelOmniResult;
import jarawak.models.modelReturn;

public class OMNIProcessServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// ############# DECLARATIONS
		DataSource ds = null;
		PGConnector con = null;

		// ############# GATHER PARAMS

		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			ds = (DataSource) envCtx.lookup("dbconnection");
			con = new PGConnector(ds, "", null, null);
		} catch (Exception e) {
		}

		// ############# INITIALIZE

		JarawakRequest jr = new JarawakRequest(req, resp);
		JarawakWS ws = new JarawakWS();
		modelOmniResult oinput = new modelOmniResult(con, null);
		JSONObject jparameters = null;
		JSONObject jparametervalues = null;
		if (jr.jsonBody != null)
			oinput = (modelOmniResult) ws.json2Object(oinput, jr.jsonBody, true);
		else {
			jr.out.print(ws.lin2json(new modelReturn("", "", ""), true).toString());
			return;
		}

		// ############# VALIDATE OBLIGATORY
		if (oinput.cls == null || oinput.cls.trim().length() <= 0) {
			jr.out.print(ws.lin2json(new modelReturn("OMNI", "E", "Inform class"), true).toString());
			return;
		}
		if (oinput.meth == null || oinput.meth.trim().length() <= 0) {
			jr.out.print(ws.lin2json(new modelReturn("OMNI", "E", "Inform method"), true).toString());
			return;
		}
		/*
		 * if (oinput.returntype == null || oinput.returntype.trim().length() <= 0) {
		 * jr.out.print(ws.lin2json(new modelReturn("OMNI", "E", "Inform result type"),
		 * true).toString()); return; }
		 */

		if (oinput.parameters != null && oinput.parameters.trim().length() > 0) {
			try {
				jparameters = new JSONObject(oinput.parameters);
			} catch (JSONException e) {
			}
		}
		if (oinput.parametervalues != null && oinput.parametervalues.trim().length() > 0) {
			try {
				jparametervalues = new JSONObject(oinput.parametervalues);
			} catch (JSONException e) {
			}
		}
		if (jparameters != null) {
			if (jparametervalues == null || jparametervalues.length() != jparameters.length()) {
				jr.out.print(ws.lin2json(new modelReturn("OMNI", "E", "Inform parameter values"), true).toString());
				return;
			}
		}
		Object o = null;
		try {
			// ############# INSTANCE CLASS
			ClassLoader cl = req.getServletContext().getClassLoader();
			Class<?> invclz = cl.loadClass(oinput.cls);
			Constructor<?> constr = null;

			for (Constructor ctor : invclz.getConstructors()) {
				Class<?>[] pType = ctor.getParameterTypes();
				if (((Class) pType[0]).getCanonicalName().equals(DataSource.class.getCanonicalName())
						&& ctor.getParameterCount() == 1) {
					constr = ctor;
					break;
				}
			}
			if (constr != null) {
				o = constr.newInstance((DataSource) ds);
			} else {
				for (Constructor ctor : invclz.getConstructors()) {
					Class<?>[] pType = ctor.getParameterTypes();
					if (ctor.getParameterCount() == 0) {
						constr = ctor;
						break;
					}
				}
				if (constr != null) {
					o = constr.newInstance();
				} else {
					jr.out.print(
							ws.lin2json(new modelReturn("OMNI", "E", "Invalid OMNI Constructor"), true).toString());
					return;
				}
			}

			// ############# CHECK SUPER
			if (!o.getClass().getSuperclass().getName().equals("jarawak.servlet.OMNIWS")) {
				jr.out.print(ws.lin2json(new modelReturn("OMNI", "E", "Invalid OMNI WebService"), true).toString());
				return;
			}

			// ############# CHECK SECURITY
			Method mSec = null;
			boolean resultSec = false;
			try {
				mSec = o.getClass().getDeclaredMethod("wsSecure", HttpServletRequest.class, String.class);
				resultSec = (boolean) mSec.invoke(o, req, "");
			} catch (Exception e) {
				try {
					mSec = o.getClass().getDeclaredMethod("wsSecure", HttpServletRequest.class);
					resultSec = (boolean) mSec.invoke(o, req);
				} catch (Exception ex) {
					jr.out.print(
							ws.lin2json(new modelReturn("OMNISECURITY", "E", "No Authorization"), true).toString());
					return;
				}
			}

			if (!resultSec) {
				jr.out.print(ws.lin2json(new modelReturn("OMNISECURITY", "E", "No Authorization"), true).toString());
				return;
			}

			// ############# EXECUTE METHOD
			Method m = null;
			try {
				if (jparameters != null) {
					ArrayList<Class<?>> clsarr = new ArrayList<Class<?>>();
					ArrayList<Object> oarr = new ArrayList<Object>();

					for (int i = 0; i < jparameters.length(); i++) {
						String cn = null;
						try {
							cn = jparameters.getString("arg" + i);
						} catch (JSONException e1) {

						}
						if (cn != null && cn.trim().length() > 0) {
							if (cn.endsWith("[]")) {
								cn = cn.replaceAll("\\[", "").replaceAll("\\]", "");
								clsarr.add(Class.forName("[L" + cn + ";"));
								JSONArray ja = null;
								try {
									ja = jparametervalues.getJSONArray("arg" + i);
								} catch (Exception e) {

								}
								if (ja != null && ja.length() > 0) {
									// String
									if (cn.equals(String.class.getCanonicalName())) {
										String[] sa = new String[ja.length()];
										for (int ia = 0; ia < sa.length; ia++)
											try {
												sa[ia] = ja.getString(ia);
											} catch (JSONException e) {
											}
										oarr.add(sa);
									} else {
										Object[] oa = new Object[ja.length()];
										for (int a = 0; a < ja.length(); a++) {
											for (Constructor<?> cz : Class.forName(cn).getConstructors()) {
												if (cz.getParameterTypes().length == 0) {
													oa[a] = cz.newInstance();
													break;
												}
												if (cz.getParameterTypes().length == 2) {
													oa[a] = cz.newInstance(null, null);
													break;
												}
											}

										}
										oa = ws.json2ObjectA(oa, ja, true);
										Object unarr = Array.newInstance(Class.forName(cn), oa.length);
										for (int ix = 0; ix < oa.length; ix++) {
											Array.set(unarr, ix, oa[ix]);
										}
										oarr.add(unarr);
										// oarr.add(Arrays.copyOf((T[])Class.forName("[L" + cn + ";").cast(oa),
										// Array.getLength(oa)));
									}
								} else {
									oarr.add(null);
								}
							} else {
								// ############# PRIMITIVE PARAM
								if (cn.trim().equals("int")) {
									clsarr.add(int.class);
									try {
										oarr.add(jparametervalues.getInt("arg" + i));
									} catch (JSONException e) {
									}
								} else if (cn.trim().equals("long")) {
									clsarr.add(long.class);
									try {
										oarr.add(jparametervalues.getLong("arg" + i));
									} catch (JSONException e) {
									}
								} else if (cn.trim().equals("short")) {
									clsarr.add(short.class);
									try {
										oarr.add(jparametervalues.getInt("arg" + i));
									} catch (JSONException e) {
									}
								} else if (cn.trim().equals("float")) {
									clsarr.add(float.class);
									try {
										oarr.add(jparametervalues.getLong("arg" + i));
									} catch (JSONException e) {
									}
								} else if (cn.trim().equals("double")) {
									clsarr.add(double.class);
									try {
										oarr.add(jparametervalues.getDouble("arg" + i));
									} catch (JSONException e) {
									}
								} else if (cn.trim().equals("byte")) {
									clsarr.add(byte.class);
									try {
										oarr.add(jparametervalues.getString("arg" + i));
									} catch (JSONException e) {
									}
								} else if (cn.trim().equals("boolean")) {
									clsarr.add(boolean.class);
									try {
										oarr.add(jparametervalues.get("arg" + i));
									} catch (JSONException e) {
									}
								} else if (cn.equals(String.class.getCanonicalName())) {
									clsarr.add(String.class);
									try {
										String co = jparametervalues.getString("arg" + i);
										if (co != null)
											co = URLDecoder.decode(co, "UTF-8");
										oarr.add(co);
									} catch (JSONException e) {
										try {
											JSONObject cjo = jparametervalues.getJSONObject("arg" + i);
											if (cjo != null)
												oarr.add(URLDecoder.decode(cjo.toString(), "UTF-8"));
										} catch (JSONException e2) {
											try {
												JSONArray cja = jparametervalues.getJSONArray("arg" + i);
												if (cja != null)
													oarr.add(URLDecoder.decode(cja.toString(), "UTF-8"));
												oarr.add(cja.toString());
											} catch (JSONException e3) {
												try {
													oarr.add(String.valueOf(jparametervalues.getDouble("arg" + i)));
												} catch (JSONException e4) {
													try {
														oarr.add(String.valueOf(jparametervalues.getLong("arg" + i)));
													} catch (JSONException e5) {
														try {
															oarr.add(
																	String.valueOf(jparametervalues.getInt("arg" + i)));
														} catch (JSONException e6) {
														}
													}
												}
											}
										}
									}

								} else {
									// ############# OBJECT PARAM
									clsarr.add(Class.forName(cn));
									Object no = null;
									for (Constructor<?> cz : Class.forName(cn).getConstructors()) {
										if (cz.getParameterTypes().length == 0) {
											no = cz.newInstance();
											break;
										}
										if (cz.getParameterTypes().length == 2) {
											no = cz.newInstance(null, null);
											break;
										}
									}
									JSONObject val = null;
									try {
										val = jparametervalues.getJSONObject("arg" + i);
									} catch (Exception e) {

									}
									if (val != null)
										try {
											Object oo = ws.json2Object(no, val, true);
											oarr.add(oo);
										} catch (Exception e) {
											oarr.add(null);
										}
									else
										oarr.add(null);
								}
							}
						}
					}

					// ############# GET METHOD

					m = o.getClass().getDeclaredMethod(oinput.meth, clsarr.toArray(new Class<?>[clsarr.size()]));

					if (m.getReturnType() != null) {
						if (m.getReturnType().isArray()) {
							Object[] result = (Object[]) m.invoke(o, oarr.toArray());
							if (result == null)
								return;
							jr.out.print(ws.lin2json(result, true, true).toString());
							return;
						} else if (m.getReturnType().getName().equals(String.class.getName())) {
							Object result = m.invoke(o, oarr.toArray());
							if (result == null)
								return;
							if (result != null)
								jr.out.print(URLEncoder.encode((String) result, "UTF-8"));
							return;
						} else {

							// ############# INVOKE
							Object result = m.invoke(o, oarr.toArray());
							if (result == null)
								return;
							jr.out.print(ws.lin2json(result, true, true).toString());
							return;
						}
					} else {
						m.invoke(o, oarr.toArray());
						return;
					}
				} else {
					// ############# GET METHOD
					m = o.getClass().getDeclaredMethod(oinput.meth);
					if (m.getReturnType() != null) {
						if (m.getReturnType().isArray()) {
							Object[] result = (Object[]) m.invoke(o);
							if (result == null)
								return;
							jr.out.print(ws.lin2json(result, true, true).toString());
							return;
						} else if (m.getReturnType().getName().equals(String.class.getName())) {
							Object result = m.invoke(o);
							if (result != null)
								jr.out.print(URLEncoder.encode((String) result, "UTF-8"));
							return;
						} else {
							// ############# INVOKE
							Object result = m.invoke(o);
							if (result == null)
								return;
							jr.out.print(ws.lin2json(result, true, true).toString());
							return;
						}
					} else {
						m.invoke(o);
						return;
					}

				}
			} catch (InvocationTargetException | NoSuchMethodException | SecurityException ex) {
				if (ex.getClass().getName().equals(InvocationTargetException.class.getName())) {
					InvocationTargetException e = (InvocationTargetException) ex;
					if (e.getTargetException().getClass().equals(JarawakException.class)) {
						JarawakException je = (JarawakException) e.getTargetException();
						jr.out.print(ws.lin2json(new modelReturn(je.getCode(), je.getType(), je.getMessage()), true)
								.toString());
					} else {
						jr.out.print(ws.lin2json(new modelReturn("WEBSERVICE", "E", e.getMessage()), true).toString());
					}
				} else {
					jr.out.print(ws
							.lin2json(new modelReturn("OMNI", "E",
									"Error executing method " + oinput.meth + ": " + ex.getMessage()), true)
							.toString());
				}
				return;
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			jr.out.print(ws
					.lin2json(new modelReturn("OMNI", "E", "Error loading class " + oinput.cls + ": " + e.getMessage()),
							true)
					.toString());
			return;
		}
	}
}
