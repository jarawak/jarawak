/*
 * Copyright 2020 Leonardo Germano Roese
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package jarawak.servlet;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jarawak.JarawakException;
import jarawak.JarawakWS;
import jarawak.models.modelReturn;

public class OMNIProcessingClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private JarawakWS ws = new JarawakWS();

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JarawakRequest jr = new JarawakRequest(req, resp);

		try {
			jr.out.print(processRequest(req, resp, jr));
		} catch (JarawakException e) {
			jr.out.print(
					ws.lin2json(new modelReturn(e.getCode(), e.getType(), e.getMessage()), checkResponseEncoded(req))
							.toString());
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JarawakRequest jr = new JarawakRequest(req, resp);
		try {
			jr.out.print(processRequest(req, resp, jr));
		} catch (JarawakException e) {
			jr.out.print(
					ws.lin2json(new modelReturn(e.getCode(), e.getType(), e.getMessage()), checkResponseEncoded(req))
							.toString());
		}

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JarawakRequest jr = new JarawakRequest(req, resp);
		try {
			jr.out.print(processRequest(req, resp, jr));
		} catch (JarawakException e) {
			jr.out.print(
					ws.lin2json(new modelReturn(e.getCode(), e.getType(), e.getMessage()), checkResponseEncoded(req))
							.toString());
		}

	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JarawakRequest jr = new JarawakRequest(req, resp);

		try {
			jr.out.print(processRequest(req, resp, jr));
		} catch (JarawakException e) {
			jr.out.print(
					ws.lin2json(new modelReturn(e.getCode(), e.getType(), e.getMessage()), checkResponseEncoded(req))
							.toString());
		}

	}

	/*
	 * ############################################################# PROCESS REQUEST
	 * ############################################################## This method
	 * determine the flow direction for the request
	 */

	private String processRequest(HttpServletRequest req, HttpServletResponse resp, JarawakRequest jr)
			throws JarawakException {

		// ############# DECLARATIONS
		ClassLoader cl = null;
		String processingpkg = null;
		String viewspath = null;
		JSONObject jinput = null;
		String jsonparameters = null;
		JSONObject jparameters = null;
		JSONArray jparametersA = null;
		JarawakException edetected = null;
		String resultprocessing = null;
		String servicename = null;

		// ############# GATHER PARAMS
		cl = req.getServletContext().getClassLoader();
		processingpkg = getInitParameter("processingpkg");
		viewspath = getInitParameter("viewspath");

		if (jr.jsonPars != null && jr.jsonPars.length() > 0)
			jinput = jr.jsonPars;
		else if (jr.jsonBody != null && jr.jsonBody.length() > 0)
			jinput = jr.jsonBody;

		if (jinput != null && jinput.length() > 0) {
			if (jinput.has("servicename")) {
				try {
					servicename = jinput.getString("servicename");
				} catch (JSONException e1) {
				}
				if (servicename != null && servicename.trim().length() > 0)
					try {
						servicename = URLDecoder.decode(servicename, "UTF-8");
					} catch (Exception e) {

					}
			}
			if (jinput.has("jsonparameters"))
				try {
					jsonparameters = jinput.getString("jsonparameters");
				} catch (JSONException e) {
				}
			else
				jsonparameters = jinput.toString();
		}

		// ############# GET SERVICE NAME
		if (servicename == null) {
			if (req.getAttribute("servicename") != null
					&& req.getAttribute("servicename").toString().trim().length() > 0) {
				servicename = req.getAttribute("servicename").toString();
			} else if (jr.endpoint != null && jr.endpoint.trim().length() > 0) {
				String sp = req.getServletPath();
				if (sp.startsWith("/"))
					sp = sp.substring(1);
				servicename = jr.endpoint.replace(sp, "");
				if (servicename.startsWith("/"))
					servicename = servicename.substring(1);
			} else {
				servicename = "index";
			}
		}
		// ############# GET PARAMETERS
		try {
			if (jsonparameters != null && jsonparameters.trim().length() > 0) {
				if (jsonparameters.trim().startsWith("[")) {
					jparametersA = new JSONArray(jsonparameters);
				} else {
					jparameters = new JSONObject(jsonparameters);
				}
			}
		} catch (Exception e) {

		}
		/*
		 * ############################################################# PROCESSING
		 * ############################################################## This is the
		 * processing block, all requests may get an associated class to exec functions
		 * prior to output UI. If there is no Output UI(jsp) available for the
		 * corresponding function, the result string (probably containing some JSON
		 * object or even an HTML content is send to output stream
		 */

		Object instance = null;
		String processingitem = servicename;
		if (processingitem.indexOf('/') >= 0)
			processingitem = processingitem.replaceAll("/", "\\.");
		try {
			instance = cl.loadClass(processingpkg + "." + processingitem).newInstance();
			// GET Corresponding Method
			try {
				Method m = instance.getClass().getDeclaredMethod("execute", HttpServletRequest.class, JSONObject.class,
						JSONArray.class, String.class);
				resultprocessing = (String) m.invoke(instance, req, jparameters, jparametersA, jr.lastpoint);
			} catch (InvocationTargetException ex) {
				edetected = (JarawakException) ex.getTargetException();
			} catch (Exception e) {

			}
		} catch (Exception e) {

		}
		// In case of processing exception detected
		if (edetected != null)
			throw edetected;

		/*
		 * ############################################################# LOOK FOR JSP
		 * Referred ############################################################## If
		 * referred function (pagefile) has a JSP associated, request flow is redirected
		 * to the view!
		 */

		File pagefile;
		try {
			pagefile = new File(req.getServletContext().getRealPath(viewspath + "/" + servicename + ".jsp"));
			if (pagefile.exists()) {
				CharArrayWriterResponse customResponse = new CharArrayWriterResponse(resp);
				RequestDispatcher reqdest = req.getRequestDispatcher(viewspath + "/" + servicename + ".jsp");
				reqdest.forward(req, customResponse);
				resultprocessing = customResponse.getOutput();
				if (customResponse != null) {
					resultprocessing = customResponse.getOutput();
				}
				return resultprocessing;
			}
		} catch (Exception e) {
		}
		return resultprocessing;
	}

	/*
	 * ############################################################# CHECK RESPONSE
	 * ENCODED HEADER ##############################################################
	 */

	private boolean checkResponseEncoded(HttpServletRequest req) {
		if (req.getHeader("responseencoded") != null && req.getHeader("responseencoded").toUpperCase().equals("TRUE"))
			return true;
		else
			return false;
	}
}
