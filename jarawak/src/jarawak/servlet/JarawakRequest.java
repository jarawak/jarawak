/*
 * Copyright 2020 Leonardo Germano Roese
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package jarawak.servlet;

import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakParam;
import jarawak.JarawakWS;

public class JarawakRequest {

	private BodyRequest bodyreq = null;
	public JarawakWS webservice = null;
	public JSONObject jsonBody = null;
	public JSONArray jsonBodyA = null;
	public JSONObject jsonPars = null;
	public String address = null;
	public String endpoint = null;
	public String lastpoint = null;
	public PrintWriter out = null;
	public String encoding = "UTF-8";

	/*
	 * #########################################################################
	 * ##### CONSTRUCTOR
	 * #########################################################################
	 * #####
	 */
	public JarawakRequest(HttpServletRequest req, HttpServletResponse resp) {
		clear();

		webservice = new JarawakWS(encoding);

		try {
			out = resp.getWriter();
		} catch (Exception e) {

		}
		address = req.getRequestURI();
		bodyreq = new BodyRequest(req, encoding);

		if (bodyreq != null) {
			jsonPars = bodyreq.getParametersJSON();
			jsonBody = bodyreq.getBodyJSON();
			jsonBodyA = bodyreq.getBodyJSONA();
		}

		if (address.startsWith(req.getContextPath())) {
			address = address.substring(req.getContextPath().length());
		}

		endpoint = address;
		if (endpoint.indexOf("?") >= 0)
			endpoint = endpoint.substring(0, endpoint.indexOf("?"));
		if (endpoint != null && endpoint.startsWith("/"))
			endpoint = endpoint.substring(1);

		lastpoint = endpoint;
		if (lastpoint.indexOf("/") > 0)
			lastpoint = endpoint.substring(lastpoint.lastIndexOf("/"), lastpoint.length());

	}

	/*
	 * #########################################################################
	 * ##### CLEAR PARAMS
	 * #########################################################################
	 * #####
	 */

	private void clear() {
		bodyreq = null;
		webservice = null;
		out = null;
		jsonBody = null;
		jsonBodyA = null;
		jsonPars = null;
		address = null;
		lastpoint = null;
		endpoint = null;
	}

	/*
	 * #########################################################################
	 * ##### AUTO-DELETE PROCESS
	 * #########################################################################
	 * ##### This Method is experimental and implements AUTO-DELETE feature for DB
	 * Connectors Only delete register with specified keys Should be used on API
	 * Development Leo - 06/11/2018
	 */

	public void autodelete(Object model) throws JarawakException {
		JSONObject json = null;

		// GET DATA
		if (jsonBody != null)
			json = jsonBody;
		else if (jsonPars != null)
			json = jsonPars;
		autodelete(model, json);

	}

	public static void autodelete(Object model, JSONObject json) throws JarawakException {
		autodelete(model, json, null);
	}

	public static void autodelete(Object model, JSONObject json, String sessionuser) throws JarawakException {

		if (model == null)
			throw new JarawakException("E", "AUTODELETE", "jarawak.model.autodelete.e.informmodel");

		JarawakConnector con = null;
		String primary = null;
		// GET CONNECTOR
		try {
			con = (JarawakConnector) model.getClass().getDeclaredMethod("getConnector", null).invoke(model, null);
		} catch (Exception e) {
			try {
				con = (JarawakConnector) model.getClass().getSuperclass().getDeclaredMethod("getConnector", null)
						.invoke(model, null);
			} catch (Exception ex) {
				throw new JarawakException("E", "AUTODELETE", "jarawak.model.autodelete.e.noconnector");
			}
		}

		// DELETE
		ArrayList<JarawakParam> keys = new ArrayList<JarawakParam>();

		for (int i = 0; i < json.length(); i++) {
			String name = null;
			String value = null;
			try {
				name = json.names().getString(i);
				value = json.getString(name);
				keys.add(new JarawakParam(name, value));
			} catch (Exception e) {
			}
		}

		if (keys.size() <= 0)
			throw new JarawakException("E", "AUTODELETE", "jarawak.model.autodelete.e.informkeys");

		if (sessionuser != null && sessionuser.trim().length() > 0)
			con.setsessionuser(sessionuser);

		try {
			con.delete(keys, null);
		} catch (JarawakException e) {
			throw new JarawakException("E", "AUTODELETE", "jarawak.model.autodelete.e.notdeleted");
		}

	}

	/*
	 * #########################################################################
	 * ##### AUTO-CREATE PROCESS
	 * #########################################################################
	 * ##### This Method is experimental and implements AUTO-CREATE feature for DB
	 * Connectors Only and for 1st layer of declared public object Should be used on
	 * API Development Leo - 06/11/2018
	 */

	public String autocreate(Object model, JSONObject keys) throws JarawakException {
		JSONObject json = null;

		// GET DATA
		if (jsonBody != null)
			json = jsonBody;
		else if (jsonPars != null)
			json = jsonPars;
		return autocreate(model, json, null);

	}

	public static String autocreate(Object model, JSONObject json, String ai) throws JarawakException {

		if (model == null)
			throw new JarawakException("E", "AUTOCREATE", "jarawak.model.autocreate.e.informmodel");

		Field[] fields = model.getClass().getDeclaredFields();
		Method[] methods = model.getClass().getDeclaredMethods();
		Method validateMethod = null;
		JarawakConnector con = null;
		// GET CONNECTOR
		try {
			con = (JarawakConnector) model.getClass().getDeclaredMethod("getConnector", null).invoke(model, null);
		} catch (Exception e) {
			try {
				con = (JarawakConnector) model.getClass().getSuperclass().getDeclaredMethod("getConnector", null)
						.invoke(model, null);
			} catch (Exception ex) {
				throw new JarawakException("E", "AUTOCREATE", "jarawak.model.autocreate.e.noconnector");
			}
		}

		// CHECK FIELDS
		if (fields == null || fields.length <= 0)
			throw new JarawakException("E", "AUTOCREATE", "jarawak.model.autocreate.e.nopublicfields");

		// LOOP CHECK
		for (int i = 0; i < json.length(); i++) {
			String name = null;
			String value = null;
			boolean fieldfound = false;
			try {
				name = json.names().getString(i);
				value = json.getString(name);
			} catch (Exception e) {
			}
			// Check Field exist
			for (int j = 0; j < fields.length; j++)
				if (fields[j].getName().equals(name)) {
					fieldfound = true;
					break;
				}

			if (!fieldfound)
				throw new JarawakException("E", "AUTOCREATE", "jarawak.model.autocreate.e.fieldnotfound");

			// Check Validation method
			validateMethod = null;
			for (int j = 0; j < methods.length; j++)
				if (methods[j].getName().equals("_validate_" + name)) {
					validateMethod = methods[j];
					break;
				}

			// Validate
			if (validateMethod != null) {
				try {
					validateMethod.invoke(model, value);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					if (((InvocationTargetException) e).getTargetException().getClass().getName()
							.indexOf("JarawakException") >= 0)
						throw (JarawakException) ((InvocationTargetException) e).getTargetException();
					else
						throw new JarawakException("E", "AUTOCREATE",
								((InvocationTargetException) e).getCause().toString());
				}
			}
		} // check loop

		// SAVE
		try {
			return con.create(ai, json, false);
		} catch (JarawakException e) {
			throw new JarawakException("E", "AUTOCREATE", "jarawak.model.autocreate.e.notcreated");
		}

	}

	/*
	 * #########################################################################
	 * ##### AUTO-SAVE PROCESS
	 * #########################################################################
	 * ##### This Method is experimental and implements AUTO-SAVE feature for DB
	 * Connectors Only and for 1st layer of declared public object fields of type
	 * String Should be used on API Development Leo - 01/10/2018
	 */

	public void autosave(Object model) throws JarawakException {
		JSONObject json = null;

		// GET DATA
		if (jsonBody != null)
			json = jsonBody;
		else if (jsonPars != null)
			json = jsonPars;
		autosave(model, json);

	}

	public static void autosave(Object model, JSONObject json) throws JarawakException {

		if (model == null)
			throw new JarawakException("E", "AUTOSAVE", "jarawak.model.autosave.e.informmodel");

		Field[] fields = model.getClass().getDeclaredFields();
		Method[] methods = model.getClass().getDeclaredMethods();
		Method validateMethod = null;
		JarawakConnector con = null;
		ArrayList<JarawakParam> primary = new ArrayList<JarawakParam>();

		// GET CONNECTOR
		try {
			con = (JarawakConnector) model.getClass().getDeclaredMethod("getConnector", null).invoke(model, null);
		} catch (Exception e) {
			try {
				con = (JarawakConnector) model.getClass().getSuperclass().getDeclaredMethod("getConnector", null)
						.invoke(model, null);
			} catch (Exception ex) {
				throw new JarawakException("E", "AUTOSAVE", "jarawak.model.autosave.e.noconnector");
			}
		}

		// CHECK FIELDS
		if (fields == null || fields.length <= 0)
			throw new JarawakException("E", "AUTOSAVE", "jarawak.model.autosave.e.nopublicfields");

		// LOOP CHECK
		for (int i = 0; i < json.length(); i++) {
			String name = null;
			String value = null;

			boolean fieldfound = false;

			try {
				name = json.names().getString(i);
			} catch (Exception e) {
			}

			// Get Value
			try {
				value = json.getString(name);
			} catch (Exception e) {
				try {
					value = String.valueOf(json.getInt(name));
				} catch (Exception e2) {
					try {
						value = String.valueOf(json.getDouble(name));
					} catch (Exception e3) {
						try {
							value = String.valueOf(json.getLong(name));
						} catch (Exception e4) {
						}
					}
				}
			}
			// Check Field exist
			for (int j = 0; j < fields.length; j++)
				if (fields[j].getName().equals(name)) {
					fieldfound = true;
					break;
				}

			if (!fieldfound)
				throw new JarawakException("E", "AUTOSAVE", "jarawak.model.autosave.e.fieldnotfound");

			JarawakParam metai = con.getMetaInfoTypeP(name);
			if (metai.primfore != null && metai.primfore == JarawakParam.KEY_PRIMARY) {
				if (value == null || value.trim().length() <= 0)
					throw new JarawakException("E", "AUTOSAVE", "jarawak.model.autosave.e.informprimarykeyvalue");
				primary.add(new JarawakParam(name, value));
			}

			// Check Validation method
			validateMethod = null;
			for (int j = 0; j < methods.length; j++)
				if (methods[j].getName().equals("_validate_" + name)) {
					validateMethod = methods[j];
					break;
				}

			// Validate
			if (validateMethod != null) {
				try {
					validateMethod.invoke(model, value);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					if (((InvocationTargetException) e).getTargetException().getClass().getName()
							.indexOf("JarawakException") >= 0)
						throw (JarawakException) ((InvocationTargetException) e).getTargetException();
					else
						throw new JarawakException("E", "AUTOSAVE",
								((InvocationTargetException) e).getCause().toString());
				}
			}
		} // check loop

		// Check Primary key exist
		if (primary.size() <= 0)
			throw new JarawakException("E", "AUTOSAVE", "jarawak.model.autosave.e.informprimarykey");

		// Remove primaries
		for (JarawakParam p : primary) {
			if (json.has(p.name))
				json.remove(p.name);
		}

		// LOOP ACTION
		JSONObject jsave = new JSONObject();
		for (int i = 0; i < json.length(); i++) {
			String name = null;
			String value = null;
			try {
				name = json.names().getString(i);
			} catch (Exception e) {
			}
			// Get Value
			try {
				value = json.getString(name);
			} catch (Exception e) {
				try {
					value = String.valueOf(json.getInt(name));
				} catch (Exception e2) {
					try {
						value = String.valueOf(json.getDouble(name));
					} catch (Exception e3) {
						try {
							value = String.valueOf(json.getLong(name));
						} catch (Exception e4) {
						}
					}
				}
			}
			try {
				jsave.put(name, value);
			} catch (Exception e) {
			}
		} // for Action

		// SAVE
		try {
			con.save(jsave, primary, null);
		} catch (JarawakException e) {
			throw new JarawakException("E", "AUTOSAVE", "jarawak.model.autosave.e.notsaved");
		}
	}
}
