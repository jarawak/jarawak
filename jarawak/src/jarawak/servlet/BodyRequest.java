package jarawak.servlet;

import java.io.BufferedReader;
import java.net.URLDecoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BodyRequest {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */
	private String body = null;
	private HttpServletRequest request = null;
	private Map<String, String[]> Mappars = null;
	private String encoding = null;

	/*
	 * #########################################################################
	 * ##### CONSTRUCTOR
	 * #########################################################################
	 * #####
	 */

	public BodyRequest(HttpServletRequest req, String enc) {

		this.request = req;
		this.encoding = enc;
		if (enc == null)
			this.encoding = "UTF-8";

		if (request == null)
			return;
		try {
			Mappars = request.getParameterMap();
		} catch (Exception e) {

		}
		try {
			StringBuilder sb = new StringBuilder();
			BufferedReader br = request.getReader();
			String str;
			while ((str = br.readLine()) != null) {
				sb.append(str);
			}
			this.body = sb.toString();
		} catch (Exception e) {
		}
	}

	/*
	 * #########################################################################
	 * ##### GETTER
	 * #########################################################################
	 * #####
	 */

	public String getBody() {
		return this.body;
	}

	/*
	 * #########################################################################
	 * ##### GET JSON PARS IN REQUEST PARAMETERS
	 * #########################################################################
	 * #####
	 */

	public JSONObject getParametersJSON() {
		if (Mappars != null && Mappars.size() > 0) {
			JSONObject jsonObj = new JSONObject();
			try {
				for (Map.Entry<String, String[]> entry : Mappars.entrySet()) {
					String v[] = entry.getValue();
					Object o = (v.length == 1) ? v[0] : v;
					String s = null;
					if (o.getClass().isArray()) {
						int cnt = 0;
						s = "";
						for (String sm : ((String[]) o)) {
							if (cnt > 0)
								s = s + ",";
							try {
								sm = URLDecoder.decode(sm, encoding);
							} catch (Exception e) {
							}
							s = s + sm;
							cnt++;
						}
					} else {
						s = (String) o;
						try {
							s = URLDecoder.decode(s, encoding);
						} catch (Exception e) {

						}
					}
					// Check if is Array object
					if (s != null && s.trim().startsWith("[")) {
						try {
							JSONArray ja = new JSONArray(s);
							jsonObj.put(entry.getKey(), ja);
							continue;
						} catch (Exception e) {

						}
					}
					// Check if is object
					if (s != null && s.trim().startsWith("{")) {
						try {
							JSONObject jo = new JSONObject(s);
							jsonObj.put(entry.getKey(), jo);
							continue;
						} catch (Exception e) {

						}
					}
					jsonObj.put(entry.getKey(), s);
				}
				return jsonObj;
			} catch (Exception e) {
			}
		}
		return null;
	}

	/*
	 * #########################################################################
	 * ##### GET JSON PARS IN REQUEST BODY
	 * #########################################################################
	 * #####
	 */

	public JSONObject getBodyJSON() {
		if (this.body == null)
			return null;
		try {
			JSONObject jso = new JSONObject(this.body);
			if (jso != null) {
				jso = decodeObject(jso);
				return jso;
			}
		} catch (Exception e) {
		}
		return null;
	}

	/*
	 * #########################################################################
	 * ##### GET JSON ARRAY IN REQUEST BODY
	 * #########################################################################
	 * #####
	 */

	public JSONArray getBodyJSONA() {
		if (this.body == null)
			return null;
		try {
			JSONArray jsa = new JSONArray(this.body);
			if (jsa != null) {
				jsa = decodeArray(jsa);
				return jsa;
			}
		} catch (Exception e) {

		}
		return null;
	}

	/*
	 * #########################################################################
	 * ##### DECODE JSON OBJECT
	 * #########################################################################
	 * #####
	 */
	private JSONObject decodeObject(JSONObject jso) {
		for (int i = 0; i < jso.names().length(); i++) {
			String s = null;
			try {
				s = jso.names().getString(i);
			} catch (Exception e) {
			}
			// Check if Array
			try {
				JSONArray jsa = new JSONArray(jso.getString(s));
				jsa = decodeArray(jsa);
				jso.put(s, jsa);
				continue;
			} catch (Exception e) {
			}
			// Check if Another Object
			try {
				JSONObject ajso = new JSONObject(jso.getString(s));
				ajso = decodeObject(ajso);
				jso.put(s, ajso);
				continue;
			} catch (Exception e) {
			}
			try {
				String decoded = URLDecoder.decode(jso.getString(s), encoding);
				// Check if is Array object
				if (decoded != null && decoded.trim().startsWith("[")) {
					try {
						JSONArray ja = new JSONArray(decoded);
						jso.put(s, ja);
						continue;
					} catch (Exception e) {

					}
				}
				// Check if is object
				if (decoded != null && decoded.trim().startsWith("{")) {
					try {
						JSONObject jo = new JSONObject(decoded);
						jso.put(s, jo);
						continue;
					} catch (Exception e) {

					}
				}
				jso.put(s, URLDecoder.decode(jso.getString(s), encoding));
			} catch (Exception e) {
			}
		}
		return jso;
	}

	/*
	 * #########################################################################
	 * ##### DECODE JSON ARRAY
	 * #########################################################################
	 * #####
	 */

	private JSONArray decodeArray(JSONArray jsa) {
		if (jsa != null && jsa.length() > 0)
			for (int i = 0; i < jsa.length(); i++) {
				JSONObject jso;
				try {
					jso = (JSONObject) jsa.get(i);
					jso = decodeObject(jso);
					jsa.put(i, jso);
				} catch (JSONException e) {
				}
			}
		return jsa;
	}
}
