/*
 * Copyright 2020 Leonardo Germano Roese
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package jarawak.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.compress.utils.IOUtils;

import jarawak.JarawakException;

public class JarawakDriveUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/*
	 * ################################################################## POST
	 * ################################################################
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JarawakRequest jr = new JarawakRequest(req, resp);
		String privatepath = null;
		String publicpath = null;
		String keystorepath = null;
		String lookuppath = null;
		File privatefolder = null;
		File publicfolder = null;
		File keystore = null;

		/*********************************************
		 ** GET HEADERS
		 *********************************************/

		String apikey = req.getHeader("apikey");
		String privateaccess = req.getHeader("privateaccess"); // Y/N
		String base64object = req.getHeader("base64object"); // Y/N

		/*********************************************
		 ** GET CONFIG
		 *********************************************/
		try {
			// PRIVATE PATH
			try {
				privatepath = getServletContext().getInitParameter("privatepath");
			} catch (Exception e) {
				privatepath = getServletContext().getRealPath("/") + "/drive/private";
			}
			// PUBLIC PATH
			try {
				publicpath = getServletContext().getInitParameter("publicpath");
			} catch (Exception e) {
				publicpath = getServletContext().getRealPath("/") + "/drive/public";
			}
			// PUBLIC PATH
			try {
				publicpath = getServletContext().getInitParameter("publicpath");
			} catch (Exception e) {
				publicpath = getServletContext().getRealPath("/") + "/drive/public";
			}
			// PUBLIC PATH
			try {
				keystorepath = getServletContext().getInitParameter("keystorepath");
			} catch (Exception e) {
				keystorepath = getServletContext().getRealPath("/") + "/drive/keystore";
			}

			try {
				privatefolder = new File(privatepath);
			} catch (Exception e) {
				resp.setContentType("application/json");
				jr.out.print(jr.webservice.lin2json(
						new JarawakException("E", "DRIVE", "Check configuration! Private folder not accessible."), true)
						.toString());
				return;
			}
			try {
				publicfolder = new File(privatepath);
			} catch (Exception e) {
				resp.setContentType("application/json");
				jr.out.print(jr.webservice.lin2json(
						new JarawakException("E", "DRIVE", "Check configuration! Public folder not accessible."), true)
						.toString());
				return;
			}
			// CHECK KEYSTORE
			try {
				keystore = new File(keystorepath);
				if (!keystore.exists()) {
					resp.setContentType("application/json");
					jr.out.print(jr.webservice
							.lin2json(new JarawakException("E", "DRIVE", "Check configuration! KeyStore not found."),
									true)
							.toString());
					return;
				}
			} catch (Exception e) {
				resp.setContentType("application/json");
				jr.out.print(jr.webservice
						.lin2json(new JarawakException("E", "DRIVE", "Check configuration! KeyStore not accessible."),
								true)
						.toString());
				return;
			}
			// CHECK PRIVATE FOLDER
			if (!privatefolder.exists()) {
				try {
					privatefolder.mkdir();
				} catch (Exception ef) {
					resp.setContentType("application/json");
					jr.out.print(jr.webservice.lin2json(
							new JarawakException("E", "DRIVE", "Check configuration! Private folder not accessible."),
							true).toString());
					return;
				}

			}
			// CHECK PUBLIC FOLDER
			if (!publicfolder.exists()) {
				try {
					publicfolder.mkdir();
				} catch (Exception ef) {
					resp.setContentType("application/json");
					jr.out.print(jr.webservice.lin2json(
							new JarawakException("E", "DRIVE", "Check configuration! Public folder not accessible."),
							true).toString());
					return;
				}
			}

		} catch (Exception e) {
			resp.setContentType("application/json");
			jr.out.print(jr.webservice.lin2json(
					new JarawakException("E", "ENVIROMENT", "Error getting enviroment path. Contact administrator."),
					true).toString());
			return;
		}

		/*********************************************
		 ** CHECK PERMISSIONS
		 *********************************************/
		lookuppath = publicpath;
		if (privateaccess != null && privateaccess.toUpperCase().trim().equals("Y")) {
			lookuppath = privatepath;
		}

		// CHECK API KEY
		if (apikey == null || apikey.trim().length() <= 0) {
			resp.setContentType("application/json");
			jr.out.print(jr.webservice.lin2json(new JarawakException("E", "APIKEY", "Inform valid API key"), true)
					.toString());
			return;
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(keystore), "UTF-8"));
		String line;
		boolean validkey = false;
		while ((line = br.readLine()) != null) {
			if (line != null && line.indexOf('=') >= 0) {
				String[] mtz = line.split("=");
				if (mtz != null && mtz.length > 1) {
					String key = mtz[0];
					String expire = mtz[1];
					if (key != null && key.equals(apikey)) {
						// CHECK EXPIRE (!FUTURE)

						validkey = true;
						break;
					}

				}
			}
		}

		if (!validkey) {
			resp.setContentType("application/json");
			jr.out.print(jr.webservice.lin2json(new JarawakException("E", "APIKEY", "Inform valid API key"), true)
					.toString());
			return;
		}

		/*********************************************
		 ** GET FILE
		 *********************************************/
		byte[] bfile = null;

		try {
			for (Part p : req.getParts()) {
				if (p.getSubmittedFileName() != null) {
					InputStream in = p.getInputStream();
					bfile = IOUtils.toByteArray(in);
				} else {
					resp.setContentType("application/json");
					jr.out.print(jr.webservice
							.lin2json(new JarawakException("E", "UPLOAD", "Error accessing file"), true).toString());
				}
			}
		} catch (Exception e) {
			resp.setContentType("application/json");
			jr.out.print(jr.webservice.lin2json(new JarawakException("E", "UPLOAD", "Error accessing file"), true)
					.toString());
			return;
		}

		/*********************************************
		 ** SAVE FILE
		 *********************************************/
		FileOutputStream fos = null;
		String spath = req.getServletPath();
		if (spath == null)
			spath = "";
		else if (spath.startsWith("/"))
			spath = spath.substring(1);

		if (spath != null && spath.trim().length() > 0) {
			String ep = jr.endpoint.replaceAll(spath, "");
			if (ep.startsWith("/"))
				ep = ep.substring(1);
			spath = lookuppath + "/" + ep;
		} else
			spath = lookuppath + "/" + jr.endpoint;

		try {
			File fo = new File(spath);
			if (!fo.getParentFile().exists())
				fo.getParentFile().mkdirs();
			if (!fo.exists())
				fo.createNewFile();
			fos = new FileOutputStream(fo, false);
			fos.write(bfile);
		} catch (Exception e) {
			resp.setContentType("application/json");
			jr.out.print(
					jr.webservice.lin2json(new JarawakException("E", "UPLOAD", "File not Saved"), true).toString());
		} finally {
			if (fos != null)
				fos.close();
		}
		resp.setContentType("application/json");
		jr.out.print(jr.webservice.lin2json(new JarawakException("S", "UPLOAD", "File Saved"), true).toString());
	}

	/*
	 * ################################################################## DELETE
	 * ################################################################
	 */
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		notImplemented(req, resp);
	}

	/*
	 * ################################################################## GET
	 * ################################################################
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		notImplemented(req, resp);
	}

	/*
	 * ################################################################## PUT
	 * ################################################################
	 */
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		notImplemented(req, resp);
	}

	/*
	 * ################################################################## NOT
	 * IMPLEMENTED RESPONSE
	 * ################################################################
	 */
	private void notImplemented(HttpServletRequest req, HttpServletResponse resp) {
		JarawakRequest jr = new JarawakRequest(req, resp);
		jr.out.print(
				jr.webservice.lin2json(new JarawakException("E", "HTTP_METHOD", "Not Implemented"), true).toString());
	}
}
