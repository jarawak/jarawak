/*
 * Copyright 2020 Leonardo Germano Roese
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package jarawak.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jarawak.JarawakException;
import jarawak.tools.MIGBase64;

public class JarawakDriveDownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// ###################################################################
	// GET
	// ###################################################################

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JarawakRequest jr = new JarawakRequest(req, resp);
		String privatepath = null;
		String publicpath = null;
		String keystorepath = null;
		String lookuppath = null;
		File privatefolder = null;
		File publicfolder = null;
		File keystore = null;
		/*********************************************
		 ** GET HEADERS
		 *********************************************/

		String apikey = req.getHeader("apikey");
		String privateaccess = req.getHeader("privateaccess"); // Y/N
		String base64object = req.getHeader("base64object"); // Y/N

		/*********************************************
		 ** GET CONFIG
		 *********************************************/
		try {
			// PRIVATE PATH
			try {
				privatepath = getServletContext().getInitParameter("privatepath");
			} catch (Exception e) {
				privatepath = getServletContext().getRealPath("/") + "/drive/private";
			}
			// PUBLIC PATH
			try {
				publicpath = getServletContext().getInitParameter("publicpath");
			} catch (Exception e) {
				publicpath = getServletContext().getRealPath("/") + "/drive/public";
			}
			// PUBLIC PATH
			try {
				keystorepath = getServletContext().getInitParameter("keystorepath");
			} catch (Exception e) {
				keystorepath = getServletContext().getRealPath("/") + "/drive/keystore";
			}

			try {
				privatefolder = new File(privatepath);
			} catch (Exception e) {
				resp.setContentType("application/json");
				jr.out.print(jr.webservice.lin2json(
						new JarawakException("E", "DRIVE", "Check configuration! Private folder not accessible."), true)
						.toString());
				return;
			}
			try {
				publicfolder = new File(privatepath);
			} catch (Exception e) {
				resp.setContentType("application/json");
				jr.out.print(jr.webservice.lin2json(
						new JarawakException("E", "DRIVE", "Check configuration! Public folder not accessible."), true)
						.toString());
				return;
			}
			// CHECK KEYSTORE
			try {
				keystore = new File(keystorepath);
				if (!keystore.exists()) {
					resp.setContentType("application/json");
					jr.out.print(jr.webservice
							.lin2json(new JarawakException("E", "DRIVE", "Check configuration! KeyStore not found."),
									true)
							.toString());
					return;
				}
			} catch (Exception e) {
				resp.setContentType("application/json");
				jr.out.print(jr.webservice
						.lin2json(new JarawakException("E", "DRIVE", "Check configuration! KeyStore not accessible."),
								true)
						.toString());
				return;
			}
			// CHECK PRIVATE FOLDER
			if (!privatefolder.exists()) {
				try {
					privatefolder.mkdir();
				} catch (Exception ef) {
					resp.setContentType("application/json");
					jr.out.print(jr.webservice.lin2json(
							new JarawakException("E", "DRIVE", "Check configuration! Private folder not accessible."),
							true).toString());
					return;
				}

			}
			// CHECK PUBLIC FOLDER
			if (!publicfolder.exists()) {
				try {
					publicfolder.mkdir();
				} catch (Exception ef) {
					resp.setContentType("application/json");
					jr.out.print(jr.webservice.lin2json(
							new JarawakException("E", "DRIVE", "Check configuration! Public folder not accessible."),
							true).toString());
					return;
				}
			}

		} catch (Exception e) {
			resp.setContentType("application/json");
			jr.out.print(jr.webservice.lin2json(
					new JarawakException("E", "ENVIROMENT", "Error getting enviroment path. Contact administrator."),
					true).toString());
			return;
		}

		/*********************************************
		 ** CHECK PERMISSIONS
		 *********************************************/
		lookuppath = publicpath;
		if (privateaccess != null && privateaccess.toUpperCase().trim().equals("Y")) {
			// CHECK API KEY
			if (apikey == null || apikey.trim().length() <= 0) {
				resp.setContentType("application/json");
				jr.out.print(jr.webservice.lin2json(new JarawakException("E", "APIKEY", "Inform valid API key"), true)
						.toString());
				return;
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(keystore), "UTF-8"));
			String line;
			boolean validkey = false;
			while ((line = br.readLine()) != null) {
				if (line != null && line.indexOf('=') >= 0) {
					String[] mtz = line.split("=");
					if (mtz != null && mtz.length > 1) {
						String key = mtz[0];
						String expire = mtz[1];
						if (key != null && key.equals(apikey)) {
							// CHECK EXPIRE (!FUTURE)

							validkey = true;
							break;
						}

					}
				}
			}

			if (!validkey) {
				resp.setContentType("application/json");
				jr.out.print(jr.webservice.lin2json(new JarawakException("E", "APIKEY", "Inform valid API key"), true)
						.toString());
				return;
			}
			lookuppath = privatepath;

		}

		/*********************************************
		 ** GET FILE
		 *********************************************/
		File myfile = null;
		String spath = req.getServletPath();
		if (spath == null)
			spath = "";
		else if (spath.startsWith("/"))
			spath = spath.substring(1);

		if (spath != null && spath.trim().length() > 0) {
			String ep = jr.endpoint.replaceAll(spath, "");
			if (ep.startsWith("/"))
				ep = ep.substring(1);
			spath = lookuppath + "/" + ep;
		}else
			spath = lookuppath + "/" + jr.endpoint;
		
		try {
			myfile = new File(spath);
			if (!myfile.exists()) {
				resp.setContentType("application/json");
				jr.out.print(jr.webservice.lin2json(new JarawakException("E", "FILE", "Not found"), true).toString());
				return;
			}
			if (!myfile.canRead()) {
				resp.setContentType("application/json");
				jr.out.print(
						jr.webservice.lin2json(new JarawakException("E", "FILE", "Not acessible"), true).toString());
				return;
			}
		} catch (Exception e) {
			resp.setContentType("application/json");
			jr.out.print(jr.webservice.lin2json(new JarawakException("E", "FILE", "Not found"), true).toString());
			return;
		}

		/*********************************************
		 ** OUTPUTFILE
		 *********************************************/

		FileInputStream in = null;
		try {
			in = new FileInputStream(myfile);
			if (base64object != null && base64object.toUpperCase().trim().equals("Y")) {
				resp.setContentType("application/json");
				byte[] fileContent = Files.readAllBytes(myfile.toPath());
				String sfile = MIGBase64.encodeToString(fileContent, false);
				jr.out.print(
						jr.webservice.lin2json(new JarawakException("S", "file.getName()", sfile), true).toString());
			} else {
				MimetypesFileTypeMap mmap = new MimetypesFileTypeMap();
				mmap.addMimeTypes("video/mp4 mp4");
				mmap.addMimeTypes("video/mpg mpg");
				mmap.addMimeTypes("video/avi avi");
				mmap.addMimeTypes("audio/mp3 mp3");
				mmap.addMimeTypes("audio/ogg ogg");
				mmap.addMimeTypes("audio/wav wav");
				mmap.addMimeTypes("image/gif gif");
				mmap.addMimeTypes("image/png png");
				mmap.addMimeTypes("image/jpeg jpeg");
				mmap.addMimeTypes("image/jpg jpg");
				mmap.addMimeTypes("image/bmp bmp");

				String mmtype = mmap.getContentType(myfile);
				resp.setContentType(mmtype);
				resp.setContentLength((int) myfile.length());
				// resp.setHeader("Content-disposition", "attachment; filename=" +
				// myfile.getName());

				int i;
				while ((i = in.read()) != -1) {
					jr.out.write(i);
				}
			}

		} catch (Exception e) {
			resp.setContentType("application/json");
			jr.out.print(jr.webservice.lin2json(new JarawakException("E", "FILE", "Not acessible"), true).toString());
		} finally {
			if (in != null)
				in.close();
		}

	}

	// ###################################################################
	// DELETE
	// ###################################################################

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		notImplemented(req, resp);
	}

	// ###################################################################
	// POST
	// ###################################################################

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		notImplemented(req, resp);
	}

	// ###################################################################
	// PUT
	// ###################################################################

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		notImplemented(req, resp);
	}

	// ###################################################################
	// NOT IMPLEMENTED
	// ###################################################################

	private void notImplemented(HttpServletRequest req, HttpServletResponse resp) {
		JarawakRequest jr = new JarawakRequest(req, resp);
		jr.out.print(
				jr.webservice.lin2json(new JarawakException("E", "HTTP_METHOD", "Not Implemented"), true).toString());
	}
}
