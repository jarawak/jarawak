/*
 * Copyright 2020 Leonardo Germano Roese
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package jarawak;

import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.support.ClassicRequestBuilder;

public class JarawakHttpCallThread implements Runnable {

	private CloseableHttpClient httpclient = null;
	private ClassicRequestBuilder wschama = null;
	public int statusCode = 0;
	private volatile String outp = null;

	public JarawakHttpCallThread(CloseableHttpClient httpclient, ClassicRequestBuilder wschama) {
		this.httpclient = httpclient;
		this.wschama = wschama;
	}

	public void run() {
		try {
			ClassicHttpResponse resposta = httpclient.execute(wschama.build(), HttpClientContext.create());
			statusCode = resposta.getCode();
			HttpEntity entity = resposta.getEntity();
			outp = "";
			if (entity != null) {
				outp = EntityUtils.toString(entity);
			}
		} catch (Exception e) {

		}
	}

	public String getOut() {
		return outp;
	}
}
