package jarawak.db;

import java.io.Serializable;

public class DBLin implements Serializable {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */
	private static final long serialVersionUID = 1L;

	public int lin = 0;
	public DBParVal[] cols = null;

	public String getVal(String parname) {
		if (this.cols != null) {
			if (this.cols.length > 0) {
				for (DBParVal lin : this.cols) {
					if (lin.param.equals(parname)) {
						return lin.value;
					}
				}
			}
		}
		return null;
	}
}
