package jarawak.config;

import java.io.Serializable;

public class MailConfig implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	public String mailHost = null;
	public String mailFrom = null;
	public String mailFromName = null;
	public String mailAuth = null;
	public String mailPort = null;
	public String mailTLS = null;
	public String mailSSL = null;
	public String mailUser = null;
	public String mailPass = null;
	public String encoding = null;
}
