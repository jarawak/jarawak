package jarawak.config;

import java.io.Serializable;

import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;

public class WSConfig implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

	public String host = null;
	public int portrange_start = 80;
	public int portrange_end = 80;
	public int connect_retries = 3;
	public long connect_timeout = 30000;
	public String path = null;
	public boolean encoded = true;
	public String encoding = null;
	public int timeout = 0;
	// Methods (GET,POST,PUT,DELETE)
	public String method_create = null;
	public String method_save = null;
	public String method_get = null;
	public String method_list = null;
	public String method_delete = null;
	// Param mode (R - Request, P - Parameter, J - JSON)
	public String params_create = null;
	public String params_save = null;
	public String params_get = null;
	public String params_list = null;
	public String params_delete = null;

	public boolean http_pool = false;
	public CloseableHttpClient httpclient = null;

	public WSConfig() {
		init();
	}

	public WSConfig(CloseableHttpClient httpclient) {
		init();

		if (httpclient != null) {
			this.http_pool = true;
			this.httpclient = httpclient;
		}
	}

	private void init() {
		method_create = "POST";
		method_save = "PUT";
		method_get = "GET";
		method_list = "GET";
		method_delete = "DELETE";

		params_create = "J";
		params_save = "J";
		params_get = "R";
		params_list = "R";
		params_delete = "R";
	}
}
