package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelModelProperty extends JarawakModel {
	private static final long serialVersionUID = -953075943817991637L;

	public modelModelProperty(JarawakConnector connector, String encoding) {
		super(connector, encoding);

		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER));
		list.add(new JarawakParam("name", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("datatype", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("dbtype", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("primary", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		setMetainfodb(list.toArray(new JarawakParam[list.size()]));
	}

	public String id = null;
	public String name = null;
	public String datatype = null;
	public String dbtype = null;
	public String primary = null;
}
