package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class schemaPostalAddress extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public schemaPostalAddress(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public schemaPostalAddress(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[7];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("addressCountry", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[2] = new JarawakParam("addressLocality", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[3] = new JarawakParam("addressRegion", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[4] = new JarawakParam("postOfficeBoxNumber", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[5] = new JarawakParam("postalCode", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[6] = new JarawakParam("streetAddress", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		setMetainfodb(meta);
		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}
	}

	public String id = null;
	public String addressCountry = null;
	public String addressLocality = null;
	public String addressRegion = null;
	public String postOfficeBoxNumber = null;
	public String postalCode = null;
	public String streetAddress = null;
}
