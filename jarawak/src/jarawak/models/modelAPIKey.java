package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelAPIKey extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public modelAPIKey(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	private void init(boolean create) {
		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("appid", JarawakParam.TYPE_BIGINTEGER));
		list.add(new JarawakParam("keyvalue", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("valid", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME));

		setMetainfodb(list.toArray(new JarawakParam[list.size()]), false);
	}

	public String id = null;
	public String appid = null;
	public String keyvalue = null;
	public String valid = null;

}
