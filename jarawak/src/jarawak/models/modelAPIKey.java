package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelAPIKey extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public modelAPIKey(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public modelAPIKey(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[6];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("appid", JarawakParam.TYPE_BIGINTEGER);
		meta[2] = new JarawakParam("keyvalue", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[3] = new JarawakParam("valid", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[4] = new JarawakParam("dtreg", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[5] = new JarawakParam("stat", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);

		setMetainfodb(meta);

		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}
	}

	public String id = null;
	public String appid = null;
	public String keyvalue = null;
	public String valid = null;
	public String dtreg = null;
	public String stat = null;

}
