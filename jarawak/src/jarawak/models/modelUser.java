package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelUser extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public modelUser(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public modelUser(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[11];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("email", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[2] = new JarawakParam("pass", JarawakParam.TYPE_STRING, JarawakParam.DB_TEXT);
		meta[3] = new JarawakParam("name", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[4] = new JarawakParam("phone", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[5] = new JarawakParam("dtreg", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[6] = new JarawakParam("stat", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[7] = new JarawakParam("icon", JarawakParam.TYPE_STRING, JarawakParam.DB_TEXT);
		meta[8] = new JarawakParam("actcode", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[9] = new JarawakParam("retries", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[10] = new JarawakParam("dtupd", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		setMetainfodb(meta);

		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}
	}

	public String id = null;
	public String email = null;
	public String pass = null;
	public String name = null;
	public String phone = null;
	public String dtreg = null;
	public String stat = null;
	public String icon = null;
	public String actcode = null;
	public String retries = null;
	public String dtupd = null;
}
