package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class schemaJobPosting extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public schemaJobPosting(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public schemaJobPosting(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[27];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("baseSalary", JarawakParam.TYPE_ENCODED_STRING, "40");
		meta[2] = new JarawakParam("datePosted", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[3] = new JarawakParam("educationRequirements", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[4] = new JarawakParam("employmentType", JarawakParam.TYPE_ENCODED_STRING, "40");
		meta[5] = new JarawakParam("estimatedSalary", JarawakParam.TYPE_ENCODED_STRING, "40");
		meta[6] = new JarawakParam("experienceRequirements", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[7] = new JarawakParam("hiringOrganization", JarawakParam.TYPE_BIGINTEGER);
		meta[8] = new JarawakParam("incentiveCompensation", JarawakParam.TYPE_ENCODED_STRING, "100");
		meta[9] = new JarawakParam("industry", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[10] = new JarawakParam("jobBenefits", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[11] = new JarawakParam("jobLocation", JarawakParam.TYPE_BIGINTEGER);
		meta[12] = new JarawakParam("occupationalCategory", JarawakParam.TYPE_ENCODED_STRING, "40");
		meta[13] = new JarawakParam("qualifications", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[14] = new JarawakParam("relevantOccupation", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[15] = new JarawakParam("responsibilities", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[16] = new JarawakParam("salaryCurrency", JarawakParam.TYPE_ENCODED_STRING, "20");
		meta[17] = new JarawakParam("skills", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[18] = new JarawakParam("specialCommitments", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[19] = new JarawakParam("title", JarawakParam.TYPE_ENCODED_STRING, "80");
		meta[20] = new JarawakParam("validThrough", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[21] = new JarawakParam("workHours", JarawakParam.TYPE_ENCODED_STRING, "100");
		meta[22] = new JarawakParam("registered", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[23] = new JarawakParam("listimage", JarawakParam.TYPE_STRING, JarawakParam.DB_TEXT);
		meta[24] = new JarawakParam("image", JarawakParam.TYPE_STRING, JarawakParam.DB_TEXT);
		meta[25] = new JarawakParam("stats", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[26] = new JarawakParam("description", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		setMetainfodb(meta);

		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}
	}

	public String id = null;
	public String baseSalary = null;
	public String datePosted = null;
	public String educationRequirements = null;
	public String employmentType = null;
	public String estimatedSalary = null;
	public String experienceRequirements = null;
	public schemaOrganization hiringOrganization = null;
	public String incentiveCompensation = null;
	public String industry = null;
	public String jobBenefits = null;
	public schemaPlace jobLocation = null;
	public String occupationalCategory = null;
	public String qualifications = null;
	public String relevantOccupation = null;
	public String responsibilities = null;
	public String salaryCurrency = null;
	public String skills = null;
	public String specialCommitments = null;
	public String title = null;
	public String validThrough = null;
	public String workHours = null;
	public String registered = null;
	public String listimage = null;
	public String image = null;
	public String stats = null;
	public String description = null;
}
