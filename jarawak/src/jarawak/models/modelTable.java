package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelTable extends JarawakModel {

	public modelTable(JarawakConnector connector, String encoding) {
		super(connector, encoding);

		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER));
		list.add(new JarawakParam("name", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("schema", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("owner", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("space", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("indexed", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("dtcreated", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME));
		list.add(new JarawakParam("dtupdated", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME));
		list.add(new JarawakParam("columns", JarawakParam.TYPE_ARRAY_OBJECT));
		setMetainfodb(list.toArray(new JarawakParam[list.size()]));
	}

	private static final long serialVersionUID = 9135689444443227080L;

	public String id = null;
	public String name = null;
	public String schema = null;
	public String owner = null;
	public String space = null;
	public String indexed = null;
	public String trigger = null;
	public String dtcreated = null;
	public String dtupdated = null;

	public modelTableColumn[] columns = null;
}
