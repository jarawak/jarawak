package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class schemaGeoCoordinates extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public schemaGeoCoordinates(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public schemaGeoCoordinates(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[9];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("address", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[2] = new JarawakParam("addressCountry", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[3] = new JarawakParam("elevation", "E", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[4] = new JarawakParam("latitude", "E", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[5] = new JarawakParam("longitude", "E", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[6] = new JarawakParam("postalCode", "E", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[7] = new JarawakParam("registered", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[8] = new JarawakParam("description", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		setMetainfodb(meta);

		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}
	}

	public String id = null;
	public schemaPostalAddress address = null;
	public String addressCountry = null;
	public String elevation = null;
	public String latitude = null;
	public String longitude = null;
	public String postalCode = null;
	public String registered = null;
	public String description = null;
}
