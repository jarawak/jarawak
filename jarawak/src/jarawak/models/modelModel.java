package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelModel extends JarawakModel {
	private static final long serialVersionUID = -3078257330398518030L;

	public modelModel(JarawakConnector connector, String encoding) {
		super(connector, encoding);

		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER));
		list.add(new JarawakParam("name", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("cls", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("table_name", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("properties", JarawakParam.TYPE_ARRAY_OBJECT));
		setMetainfodb(list.toArray(new JarawakParam[list.size()]));
	}

	public String id = null;
	public String name = null;
	public String cls = null;
	public String table_name = null;
	public modelModelProperty[] properties = null;
}
