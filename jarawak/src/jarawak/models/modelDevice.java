package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelDevice extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public modelDevice(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public modelDevice(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[15];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("hash", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[2] = new JarawakParam("userid", JarawakParam.TYPE_BIGINTEGER);
		meta[3] = new JarawakParam("devtype", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[4] = new JarawakParam("devmodel", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[5] = new JarawakParam("devfab", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[6] = new JarawakParam("devso", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[7] = new JarawakParam("devsoversion", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[8] = new JarawakParam("appversion", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[9] = new JarawakParam("dtreg", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[10] = new JarawakParam("stat", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[11] = new JarawakParam("keyvalue", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[12] = new JarawakParam("sessionkey", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[13] = new JarawakParam("sessionvalid", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[14] = new JarawakParam("actcode", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		setMetainfodb(meta);

		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}
	}

	public String id = null;
	public String hash = null;
	public String userid = null;
	public String devtype = null;
	public String devmodel = null;
	public String devfab = null;
	public String devso = null;
	public String devsoversion = null;
	public String appversion = null;
	public String dtreg = null;
	public String stat = null;
	public String keyvalue = null;
	public String sessionkey = null;
	public String sessionvalid = null;
	public String actcode = null;
}
