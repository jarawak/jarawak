package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelDevice extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public modelDevice(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init();
	}


	private void init() {
		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("hash", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("userid", JarawakParam.TYPE_BIGINTEGER));
		list.add(new JarawakParam("devtype", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("devmodel", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("devfab", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("devso", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("devsoversion", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("appversion", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("keyvalue", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("sessionkey", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("sessionvalid", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME));
		list.add(new JarawakParam("actcode", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		setMetainfodb(list.toArray(new JarawakParam[list.size()]), false);
	}

	public String id = null;
	public String hash = null;
	public String userid = null;
	public String devtype = null;
	public String devmodel = null;
	public String devfab = null;
	public String devso = null;
	public String devsoversion = null;
	public String appversion = null;
	public String dtreg = null;
	public String stat = null;
	public String keyvalue = null;
	public String sessionkey = null;
	public String sessionvalid = null;
	public String actcode = null;
}
