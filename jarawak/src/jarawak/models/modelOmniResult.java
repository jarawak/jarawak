package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelOmniResult extends JarawakModel {

	public modelOmniResult(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("cls", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("meth", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("returntype", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("returnresult", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("parameters", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("parametervalues", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		setMetainfodb(list.toArray(new JarawakParam[list.size()]));
	}

	private static final long serialVersionUID = 1L;
	public String cls = null;
	public String meth = null;
	public String returntype = null;
	public String returnresult = null;
	public String parameters = null;
	public String parametervalues = null;
}
