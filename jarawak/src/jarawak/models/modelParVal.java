package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelParVal extends JarawakModel {

	public modelParVal(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("parameter", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("value", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("typ", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("ref", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("aux1", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("aux2", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("aux3", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		setMetainfodb(list.toArray(new JarawakParam[list.size()]));
	}

	private static final long serialVersionUID = 1L;
	public String parameter = null;
	public String value = null;
	public String typ = null;
	public String ref = null;
	public String aux1 = null;
	public String aux2 = null;
	public String aux3 = null;
}
