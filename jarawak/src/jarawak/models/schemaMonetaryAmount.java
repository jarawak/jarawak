package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class schemaMonetaryAmount extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public schemaMonetaryAmount(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public schemaMonetaryAmount(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[7];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("currency", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[2] = new JarawakParam("maxValue", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[3] = new JarawakParam("minValue", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[4] = new JarawakParam("validFrom", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[5] = new JarawakParam("validThrough", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[6] = new JarawakParam("value", JarawakParam.TYPE_BIGINTEGER);
		setMetainfodb(meta);
		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}
	}

	public String id = null;
	public String currency = null;
	public String maxValue = null;
	public String minValue = null;
	public String validFrom = null;
	public String validThrough = null;
	public schemaQuantitativeValue value = null;

}
