package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class schemaOrganization extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public schemaOrganization(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public schemaOrganization(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[50];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("actionableFeedbackPolicy", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[2] = new JarawakParam("address", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[3] = new JarawakParam("aggregateRating", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[4] = new JarawakParam("alumni", JarawakParam.TYPE_BIGINTEGER);
		meta[5] = new JarawakParam("areaServed", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[6] = new JarawakParam("award", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[7] = new JarawakParam("brand", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[8] = new JarawakParam("contactPoint", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[9] = new JarawakParam("correctionsPolicy", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[10] = new JarawakParam("department", JarawakParam.TYPE_BIGINTEGER);
		meta[11] = new JarawakParam("dissolutionDate", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[12] = new JarawakParam("diversityPolicy", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[13] = new JarawakParam("duns", JarawakParam.TYPE_ENCODED_STRING);
		meta[14] = new JarawakParam("email", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[15] = new JarawakParam("employee", JarawakParam.TYPE_BIGINTEGER);
		meta[16] = new JarawakParam("ethicsPolicy", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[17] = new JarawakParam("event", JarawakParam.TYPE_BIGINTEGER);
		meta[18] = new JarawakParam("faxNumber", JarawakParam.TYPE_ENCODED_STRING, "40");
		meta[19] = new JarawakParam("founder", JarawakParam.TYPE_BIGINTEGER);
		meta[20] = new JarawakParam("foundingDate", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[21] = new JarawakParam("foundingLocation", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[22] = new JarawakParam("funder", JarawakParam.TYPE_BIGINTEGER);
		meta[23] = new JarawakParam("globalLocationNumber", JarawakParam.TYPE_ENCODED_STRING, "40");
		meta[24] = new JarawakParam("hasOfferCatalog", JarawakParam.TYPE_ENCODED_STRING, "1");
		meta[25] = new JarawakParam("hasPOS", JarawakParam.TYPE_ENCODED_STRING, "1");
		meta[26] = new JarawakParam("isicV4", JarawakParam.TYPE_ENCODED_STRING, "1");
		meta[27] = new JarawakParam("legalName", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[28] = new JarawakParam("leiCode", JarawakParam.TYPE_ENCODED_STRING, "40");
		meta[29] = new JarawakParam("location", JarawakParam.TYPE_BIGINTEGER);
		meta[30] = new JarawakParam("logo", JarawakParam.TYPE_STRING, JarawakParam.DB_TEXT);
		meta[31] = new JarawakParam("makesOffer", JarawakParam.TYPE_ENCODED_STRING, "40");
		meta[32] = new JarawakParam("member", JarawakParam.TYPE_BIGINTEGER);
		meta[33] = new JarawakParam("memberOf", JarawakParam.TYPE_BIGINTEGER);
		meta[34] = new JarawakParam("naics", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[35] = new JarawakParam("numberOfEmployees", JarawakParam.TYPE_STRING);
		meta[36] = new JarawakParam("owns", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[37] = new JarawakParam("parentOrganization", JarawakParam.TYPE_BIGINTEGER);
		meta[38] = new JarawakParam("publishingPrinciples", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[39] = new JarawakParam("review", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[40] = new JarawakParam("seeks", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[41] = new JarawakParam("sponsor", JarawakParam.TYPE_BIGINTEGER);
		meta[42] = new JarawakParam("subOrganization", JarawakParam.TYPE_BIGINTEGER);
		meta[43] = new JarawakParam("taxID", JarawakParam.TYPE_ENCODED_STRING, "40");
		meta[44] = new JarawakParam("telephone", JarawakParam.TYPE_ENCODED_STRING, "40");
		meta[45] = new JarawakParam("unnamedSourcesPolicy", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[46] = new JarawakParam("vatID", JarawakParam.TYPE_ENCODED_STRING, "100");
		meta[47] = new JarawakParam("registered", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[48] = new JarawakParam("description", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[49] = new JarawakParam("sameAs", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		setMetainfodb(meta);

		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}

	}

	public String id = null;
	public String actionableFeedbackPolicy = null;
	public String address = null;
	public String aggregateRating = null;
	public schemaPerson alumni = null;
	public String areaServed = null;
	public String award = null;
	public String brand = null;
	public String contactPoint = null;
	public String correctionsPolicy = null;
	public String department = null;
	public String dissolutionDate = null;
	public String diversityPolicy = null;
	public String duns = null;
	public String email = null;
	public schemaPerson employee = null;
	public String ethicsPolicy = null;
	public schemaEvent event = null;
	public String faxNumber = null;
	public schemaPerson founder = null;
	public String foundingDate = null;
	public String foundingLocation = null;
	public schemaOrganization funder = null;
	public String globalLocationNumber = null;
	public String hasOfferCatalog = null;
	public String hasPOS = null;
	public String isicV4 = null;
	public String legalName = null;
	public String leiCode = null;
	public schemaPlace location = null;
	public String logo = null;
	public String makesOffer = null;
	public schemaOrganization member = null;
	public schemaOrganization memberOf = null;
	public String naics = null;
	public String numberOfEmployees = null;
	public String owns = null;
	public schemaOrganization parentOrganization = null;
	public String publishingPrinciples = null;
	public String review = null;
	public String seeks = null;
	public schemaOrganization sponsor = null;
	public schemaOrganization subOrganization = null;
	public String taxID = null;
	public String telephone = null;
	public String unnamedSourcesPolicy = null;
	public String vatID = null;
	public String registered = null;
	public String description = null;
	public String sameAs = null;
}
