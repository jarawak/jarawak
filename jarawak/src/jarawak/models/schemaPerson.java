package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class schemaPerson extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public schemaPerson(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public schemaPerson(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[55];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("additionalName", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[2] = new JarawakParam("address", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[3] = new JarawakParam("affiliation", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[4] = new JarawakParam("alumniOf", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[5] = new JarawakParam("award", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[6] = new JarawakParam("birthDate", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_DATETIME);
		meta[7] = new JarawakParam("birthPlace", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[8] = new JarawakParam("brand", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[9] = new JarawakParam("children", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[10] = new JarawakParam("colleague", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[11] = new JarawakParam("contactPoint", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[12] = new JarawakParam("deathDate", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_DATETIME);
		meta[13] = new JarawakParam("deathPlace", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[14] = new JarawakParam("duns", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[15] = new JarawakParam("email", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[16] = new JarawakParam("familyName", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[17] = new JarawakParam("faxNumber", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[18] = new JarawakParam("follows", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[19] = new JarawakParam("funder", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[20] = new JarawakParam("gender", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[21] = new JarawakParam("givenName", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[22] = new JarawakParam("globalLocationNumber", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[23] = new JarawakParam("hasOccupation", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[24] = new JarawakParam("hasOfferCatalog", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[25] = new JarawakParam("hasPOS", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[26] = new JarawakParam("height", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[27] = new JarawakParam("homeLocation", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[28] = new JarawakParam("honorificPrefix", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[29] = new JarawakParam("honorificSuffix", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[30] = new JarawakParam("isicV4", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[31] = new JarawakParam("jobTitle", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[32] = new JarawakParam("knows", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[33] = new JarawakParam("makesOffer", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[34] = new JarawakParam("memberOf", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[35] = new JarawakParam("naics", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[36] = new JarawakParam("nationality", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[37] = new JarawakParam("netWorth", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[38] = new JarawakParam("owns", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[39] = new JarawakParam("parent", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[40] = new JarawakParam("performerIn", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[41] = new JarawakParam("publishingPrinciples", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[42] = new JarawakParam("relatedTo", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[43] = new JarawakParam("seeks", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[44] = new JarawakParam("sibling", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[45] = new JarawakParam("sponsor", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[46] = new JarawakParam("spouse", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[47] = new JarawakParam("taxID", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[48] = new JarawakParam("telephone", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[49] = new JarawakParam("vatID", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[50] = new JarawakParam("weight", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[51] = new JarawakParam("workLocation", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[52] = new JarawakParam("worksFor", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[53] = new JarawakParam("registered", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[54] = new JarawakParam("description", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		setMetainfodb(meta);

		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}
	}

	public String id = null;
	public String additionalName = null;
	public schemaPostalAddress address = null;
	public schemaOrganization affiliation = null;
	public schemaOrganization alumniOf = null;
	public String award = null;
	public String birthDate = null;
	public String birthPlace = null;
	public String brand = null;
	public schemaPerson children = null;
	public schemaPerson colleague = null;
	public String contactPoint = null;
	public String deathDate = null;
	public String deathPlace = null;
	public String duns = null;
	public String email = null;
	public String familyName = null;
	public String faxNumber = null;
	public schemaPerson follows = null;
	public schemaOrganization funder = null;
	public String gender = null;
	public String givenName = null;
	public String globalLocationNumber = null;
	public String hasOccupation = null;
	public String hasOfferCatalog = null;
	public String hasPOS = null;
	public String height = null;
	public String homeLocation = null;
	public String honorificPrefix = null;
	public String honorificSuffix = null;
	public String isicV4 = null;
	public String jobTitle = null;
	public schemaPerson knows = null;
	public String makesOffer = null;
	public schemaOrganization memberOf = null;
	public String naics = null;
	public String nationality = null;
	public String netWorth = null;
	public schemaOrganization owns = null;
	public schemaPerson parent = null;
	public schemaEvent performerIn = null;
	public String publishingPrinciples = null;
	public schemaPerson relatedTo = null;
	public String seeks = null;
	public schemaPerson sibling = null;
	public schemaOrganization sponsor = null;
	public schemaPerson spouse = null;
	public String taxID = null;
	public String telephone = null;
	public String vatID = null;
	public String weight = null;
	public String workLocation = null;
	public schemaOrganization worksFor = null;
	public String registered = null;
	public String description = null;

}
