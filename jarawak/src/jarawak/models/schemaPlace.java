package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class schemaPlace extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public schemaPlace(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public schemaPlace(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[26];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("additionalProperty", JarawakParam.TYPE_ENCODED_STRING);
		meta[2] = new JarawakParam("address", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[3] = new JarawakParam("aggregateRating", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[4] = new JarawakParam("amenityFeature", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[5] = new JarawakParam("branchCode", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[6] = new JarawakParam("containedInPlace", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[7] = new JarawakParam("containsPlace", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[8] = new JarawakParam("event", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[9] = new JarawakParam("faxNumber", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[10] = new JarawakParam("geo", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_TEXT);
		meta[11] = new JarawakParam("globalLocationNumber", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[12] = new JarawakParam("hasMap", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[13] = new JarawakParam("isAccessibleForFree", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[14] = new JarawakParam("isicV4", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[15] = new JarawakParam("logo", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[16] = new JarawakParam("maximumAttendeeCapacity", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[17] = new JarawakParam("openingHoursSpecification", JarawakParam.TYPE_ENCODED_STRING,
				JarawakParam.DB_TEXT);
		meta[18] = new JarawakParam("photo", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[19] = new JarawakParam("publicAccess", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[20] = new JarawakParam("review", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[21] = new JarawakParam("smokingAllowed", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[22] = new JarawakParam("specialOpeningHoursSpecification", JarawakParam.TYPE_ENCODED_STRING,
				JarawakParam.DB_TEXT);
		meta[23] = new JarawakParam("telephone", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[24] = new JarawakParam("registered", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[25] = new JarawakParam("description", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);

		setMetainfodb(meta);

		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}
	}

	public String id = null;
	public String additionalProperty = null;
	public schemaPostalAddress address = null;
	public String aggregateRating = null;
	public String amenityFeature = null;
	public String branchCode = null;
	public schemaPlace containedInPlace = null;
	public schemaPlace containsPlace = null;
	public schemaEvent event = null;
	public String faxNumber = null;
	public schemaGeoCoordinates geo = null;
	public String globalLocationNumber = null;
	public String hasMap = null;
	public String isAccessibleForFree = null;
	public String isicV4 = null;
	public String logo = null;
	public String maximumAttendeeCapacity = null;
	public String openingHoursSpecification = null;
	public String photo = null;
	public String publicAccess = null;
	public String review = null;
	public String smokingAllowed = null;
	public String specialOpeningHoursSpecification = null;
	public String telephone = null;
	public String registered = null;
	public String description = null;
}
