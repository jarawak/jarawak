package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelTranslateLine extends JarawakModel {

	public modelTranslateLine(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("lang", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("prop", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("target", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("targetkey1", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("targetkey2", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("targetkey3", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("targetkey4", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("targetkey5", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("value", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("original", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		setMetainfodb(list.toArray(new JarawakParam[list.size()]), true);
	}

	private static final long serialVersionUID = 1L;
	public String lang = null;
	public String prop = null;
	public String target = null;
	public String targetkey1 = null;
	public String targetkey2 = null;
	public String targetkey3 = null;
	public String targetkey4 = null;
	public String targetkey5 = null;
	public String value = null;
	public String original = null;

}
