package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelTranslateLine extends JarawakModel {

	public modelTranslateLine(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("lang", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("prop", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("target", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("targetkey1", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("targetkey2", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("targetkey3", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("targetkey4", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("targetkey5", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("value", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("original", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("dtcreated", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME));
		list.add(new JarawakParam("createdby", JarawakParam.TYPE_BIGINTEGER));
		list.add(new JarawakParam("dtupdated", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME));
		list.add(new JarawakParam("updatedby", JarawakParam.TYPE_BIGINTEGER));
		list.add(new JarawakParam("stat", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		setMetainfodb(list.toArray(new JarawakParam[list.size()]));
	}

	private static final long serialVersionUID = 1L;
	public String lang = null;
	public String prop = null;
	public String target = null;
	public String targetkey1 = null;
	public String targetkey2 = null;
	public String targetkey3 = null;
	public String targetkey4 = null;
	public String targetkey5 = null;
	public String value = null;
	public String original = null;
	public String dtcreated = null;
	public String createdby = null;
	public String dtupdated = null;
	public String updatedby = null;
	public String stat = null;

}
