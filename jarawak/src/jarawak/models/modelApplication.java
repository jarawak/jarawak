package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelApplication extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public modelApplication(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}


	private void init(boolean create) {
		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY));
		list.add(new JarawakParam("name", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));

		setMetainfodb(list.toArray(new JarawakParam[list.size()]), false);
	}

	public String id = null;
	public String name = null;
}
