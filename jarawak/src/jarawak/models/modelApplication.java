package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakException;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelApplication extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public modelApplication(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init(false);
	}

	public modelApplication(JarawakConnector connector, String encoding, boolean create) {
		super(connector, encoding);
		init(create);
	}

	private void init(boolean create) {
		JarawakParam[] meta = new JarawakParam[4];
		meta[0] = new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER, JarawakParam.DB_SERIAL,
				JarawakParam.KEY_PRIMARY);
		meta[1] = new JarawakParam("name", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);
		meta[2] = new JarawakParam("dtreg", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME);
		meta[3] = new JarawakParam("stat", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT);

		setMetainfodb(meta);

		if (create)
			try {
				createTable(false);
			} catch (JarawakException e) {

			}
	}

	public String id = null;
	public String name = null;
	public String dtreg = null;
	public String stat = null;
}
