package jarawak.models;

public class modelReturn {

	public modelReturn(String etype, String message) {
		this.errortype = etype;
		this.errormessage = message;
		this.errorcode = null;
		this.refer = null;
	}

	public modelReturn(String ecode, String etype, String message) {
		this.errorcode = ecode;
		this.errortype = etype;
		this.errormessage = message;
		this.refer = null;
	}

	public modelReturn(String ecode, String etype, String message, String refer) {
		this.errorcode = ecode;
		this.errortype = etype;
		this.errormessage = message;
		this.refer = refer;
	}

	public String errorcode = null;
	public String errortype = null;
	public String errormessage = null;
	public String refer = null;
}
