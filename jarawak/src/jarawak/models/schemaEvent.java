package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class schemaEvent extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public schemaEvent(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init();
	}

	private void init() {
		JarawakParam[] meta = new JarawakParam[18];
		meta[0] = new JarawakParam("aggregateRating", "E");
		meta[1] = new JarawakParam("audience", "E");
		meta[2] = new JarawakParam("doorTime", "S");
		meta[3] = new JarawakParam("duration", "S");
		meta[4] = new JarawakParam("endDate", "S");
		meta[5] = new JarawakParam("eventStatus", "S");
		meta[6] = new JarawakParam("inLanguage", "E");
		meta[7] = new JarawakParam("isAccessibleForFree", "E");
		meta[8] = new JarawakParam("maximumAttendeeCapacity", "E");
		meta[9] = new JarawakParam("offers", "E");
		meta[10] = new JarawakParam("previousStartDate", "S");
		meta[11] = new JarawakParam("recordedIn", "E");
		meta[12] = new JarawakParam("remainingAttendeeCapacity", "E");
		meta[13] = new JarawakParam("review", "S");
		meta[14] = new JarawakParam("startDate", "S");
		meta[15] = new JarawakParam("typicalAgeRange", "E");
		meta[16] = new JarawakParam("workFeatured", "E");
		meta[17] = new JarawakParam("workPerformed", "E");
		setMetainfodb(meta);
	}

	public String id = null;
	public schemaThing about = null;
	public schemaPerson actor = null;
	public String aggregateRating = null;
	public schemaOrganization attendee = null;
	public String audience = null;
	public schemaOrganization composer = null;
	public schemaOrganization contributor = null;
	public schemaPerson director = null;
	public String doorTime = null;
	public String duration = null;
	public String endDate = null;
	public String eventStatus = null;
	public schemaOrganization funder = null;
	public String inLanguage = null;
	public String isAccessibleForFree = null;
	public schemaPlace location = null;
	public String maximumAttendeeCapacity = null;
	public String offers = null;
	public schemaOrganization organizer = null;
	public schemaOrganization performer = null;
	public String previousStartDate = null;
	public String recordedIn = null;
	public String remainingAttendeeCapacity = null;
	public String review = null;
	public schemaOrganization sponsor = null;
	public String startDate = null;
	public schemaEvent subEvent = null;
	public schemaEvent superEvent = null;
	public schemaOrganization translator = null;
	public String typicalAgeRange = null;
	public String workFeatured = null;
	public String workPerformed = null;
}
