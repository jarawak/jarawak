package jarawak.models;

import java.util.ArrayList;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class modelTableColumn extends JarawakModel {

	private static final long serialVersionUID = 5136714895165675183L;

	public modelTableColumn(JarawakConnector connector, String encoding) {
		super(connector, encoding);

		ArrayList<JarawakParam> list = new ArrayList<JarawakParam>();
		list.add(new JarawakParam("id", JarawakParam.TYPE_BIGINTEGER));
		list.add(new JarawakParam("schemaname", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("tablename", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("name", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("position", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("nullable", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("datatype", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("datalength", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("precision", JarawakParam.TYPE_ENCODED_STRING, JarawakParam.DB_TEXT));
		list.add(new JarawakParam("dtcreated", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME));
		list.add(new JarawakParam("dtupdated", JarawakParam.TYPE_STRING, JarawakParam.DB_DATETIME));
		setMetainfodb(list.toArray(new JarawakParam[list.size()]));
	}

	public String id = null;
	public String schemaname = null;
	public String tablename = null;
	public String name = null;
	public String position = null;
	public String nullable = null;
	public String datatype = null;
	public String datalength = null;
	public String precision = null;
	public String dtcreated = null;
	public String dtupdated = null;

	public modelTableColumn[] columns = null;

}
