package jarawak.models;

import jarawak.JarawakConnector;
import jarawak.JarawakModel;
import jarawak.JarawakParam;

public class schemaThing extends JarawakModel {
	private static final long serialVersionUID = 1L;

	public schemaThing(JarawakConnector connector, String encoding) {
		super(connector, encoding);
		init();
	}

	private void init() {
		JarawakParam[] meta = new JarawakParam[12];
		meta[0] = new JarawakParam("additionalType", "E");
		meta[1] = new JarawakParam("alternateName", "E");
		meta[2] = new JarawakParam("description", "E");
		meta[3] = new JarawakParam("disambiguatingDescription", "E");
		meta[4] = new JarawakParam("identifier", "E");
		meta[5] = new JarawakParam("image", "S");
		meta[6] = new JarawakParam("mainEntityOfPage", "E");
		meta[7] = new JarawakParam("name", "E");
		meta[8] = new JarawakParam("potentialAction", "E");
		meta[9] = new JarawakParam("sameAs", "E");
		meta[10] = new JarawakParam("subjectOf", "E");
		meta[11] = new JarawakParam("url", "E");

		setMetainfodb(meta);
	}

	public String additionalType = null;
	public String alternateName = null;
	public String description = null;
	public String disambiguatingDescription = null;
	public String identifier = null;
	public String image = null;
	public String mainEntityOfPage = null;
	public String name = null;
	public String potentialAction = null;
	public String sameAs = null;
	public String subjectOf = null;
	public String url = null;

}
