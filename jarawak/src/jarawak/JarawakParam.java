package jarawak;

import java.io.Serializable;

public class JarawakParam implements Serializable {
	/*
	 * Copyright 2017 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */
	private static final long serialVersionUID = 1L;

	public String name = null;
	public String value = null;
	public String mode = null;
	public String primfore = null;
	public String foretab = null;
	public String forecol = null;

	public static final String MODE_WSQUERY_GREATEROREQUAL = "GE";
	public static final String MODE_WSQUERY_GREATERTHAN = "GT";
	public static final String MODE_WSQUERY_LOWEROREQUAL = "LE";
	public static final String MODE_WSQUERY_LOWERTHAN = "LT";
	public static final String MODE_WSQUERY_EQUAL = "EQ";
	public static final String MODE_WSQUERY_EQUAL_U = "EQU";
	public static final String MODE_WSQUERY_NOTEQUAL = "NE";
	public static final String MODE_WSQUERY_NOTEQUAL_U = "NEU";
	public static final String MODE_WSQUERY_ISNULL = "IS";
	public static final String MODE_WSQUERY_NOTNULL = "NN";
	public static final String MODE_WSQUERY_IN = "IN";
	public static final String MODE_WSQUERY_NOTIN = "NIN";
	public static final String MODE_WSQUERY_IN_U = "INU";
	public static final String MODE_WSQUERY_NOTIN_U = "NOTINU";
	public static final String MODE_WSQUERY_STARTINGWITH = "SW";
	public static final String MODE_WSQUERY_ENDINGWITH = "EW";
	public static final String MODE_WSQUERY_HAVING = "HA";
	public static final String MODE_WSQUERY_STARTINGWITH_U = "SWU";
	public static final String MODE_WSQUERY_ENDINGWITH_U = "EWU";
	public static final String MODE_WSQUERY_HAVING_U = "HAU";
	public static final String MODE_WSQUERY_HAVING_UNA = "HAUNA";
	public static final String MODE_WSQUERY_STARTINGWITH_UNA_U = "SWUNAU";
	public static final String MODE_WSQUERY_ENDINGWITH_UNA_U = "EWUNAU";
	public static final String MODE_WSQUERY_HAVING_UNA_U = "HAUNAU";

	public static final String TYPE_STRING = "S";
	public static final String TYPE_ENCODED_STRING = "E";
	public static final String TYPE_INTEGER = "I";
	public static final String TYPE_BIGINTEGER = "B";
	public static final String TYPE_EXCLUDED = "X";
	public static final String TYPE_OBJECT = "O";
	public static final String TYPE_ARRAY_OBJECT = "A";
	public static final String TYPE_ARRAY_STRING = "SA";
	public static final String TYPE_BOOLEAN = "BO";
	public static final String TYPE_NUMERIC = "NUM";

	public static final String DB_SERIAL = "S";
	public static final String DB_TEXT = "T";
	public static final String DB_DATETIME = "D";

	public static final String PARAM_ON_REQUEST = "R";
	public static final String PARAM_AS_PARAMETER = "P";
	public static final String PARAM_AS_JSON = "J";

	public static final String KEY_PRIMARY = "P";
	public static final String KEY_FOREIGN = "F";

	public JarawakParam(String name, String value_type, String mode_len, String key_primfore, String fore_table,
			String fore_colunm) {
		this.name = name;
		this.value = value_type;
		this.mode = mode_len;
		this.primfore = key_primfore;
		this.foretab = fore_table;
		this.forecol = fore_colunm;
	}

	public JarawakParam(String name, String value_type, String mode_len, String key_primfore) {
		this.name = name;
		this.value = value_type;
		this.mode = mode_len;
		this.primfore = key_primfore;
	}

	public JarawakParam(String name, String value_type, String mode_len) {
		this.name = name;
		this.value = value_type;
		this.mode = mode_len;
	}

	public JarawakParam(String name, String value_type) {
		this.name = name;
		this.value = value_type;
		this.mode = null;
	}

	public JarawakParam() {

	}
}
